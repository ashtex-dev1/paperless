<?php

header( 'Access-Control-Allow-Origin: *' );

use \Config\Constants;
use \Config\Central;

class AppApi implements RocketSled\Runnable
{

    const LOGIN = 1;
    const ACCEPT_EVENTS = 2;
    const REJECT_EVENTS = 3;
    const PENDING_EVENTS = 4;
    const ALL_EVENTS = 5;
    const FORM = 6;
    const FORM_INSERTION = 7;
    const FORM_DETAILS = 8;
    const FORM_DATA_SEND = 9;
    const ACCEPT_EVENTS_DETAILS = 10;
    const ACCEPT_FORM_DETAILS = 11;
    const REJECT_FORM_DETAILS = 12;
    // Messages
    const ERROR_MESSAGE = 'Something went wrong. Please try again';
    const SUCCESS_MESSAGE = 'Success';

    private $profile = 'paperless';
    private $central = '';
    // Static
    private static $deleted = 'deleted <> 1';

    public function __construct()
    {
	try
	{
	    $this->central = Central::instance();
	    $this->central->set_alias_connection( $this->profile );
	}
	catch ( Exception $e )
	{
	    
	}
    }

    public function run()
    {
	try
	{
	    $response = array( 'success' => 0, 'error' => 1, 'message' => self::ERROR_MESSAGE );
	    $response = $this->update_main_contents();
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}

	die( json_encode( $response ) );
    }

    public function update_main_contents()
    {
	try
	{
	    $return = array( 'success' => 0, 'error' => 1, 'message' => self::ERROR_MESSAGE );
	    $corrupt = false;
	    $action = $this->central->getargs( 'action', $_GET, $corrupt );
	    if ( !$corrupt )
	    {
		switch ( $action )
		{
		    case self::LOGIN:
			$bcrypt = new \Config\Bcrypt();
			$return = $this->user( $bcrypt );
			break;
		    case self::ACCEPT_EVENTS:
			$return = $this->accept_event();
			break;
		    case self::REJECT_EVENTS:
			$return = $this->reject_event();
			break;
		    case self::PENDING_EVENTS:
			$return = $this->pending_event();
			break;
		    case self::ALL_EVENTS:
			$return = $this->all_event();
			break;
		    case self::FORM:
			$return = $this->form();
			break;
		    case self::FORM_INSERTION:
			$return = $this->form_insertion();
			break;
		    case self::FORM_DETAILS:
			$return = $this->form_details();
			break;
		    case self::FORM_DATA_SEND:
			$return = $this->form_data_send();
			break;
		    case self::ACCEPT_EVENTS_DETAILS:
			$return = $this->accept_event_details();
			break;
		    case self::ACCEPT_FORM_DETAILS:
			$return = $this->accept_form_details();
			break;
		    case self::REJECT_FORM_DETAILS:
			$return = $this->reject_form_details();
			break;
		    default:
			$return = array( 'success' => 0, 'error' => 1, 'message' => self::ERROR_MESSAGE );
			break;
		}
	    }
	}
	catch ( Exception $ex )
	{
	    
	}
	return $return;
    }

    private function all_event()
    {
	try
	{

	    $data = array();
	    $return = array( 'success' => 0, 'error' => 1, 'message' => self::ERROR_MESSAGE );
	    $all_events = PluSQL::from( $this->profile )->event->invitaion->select( '*' )->where( "event.deleted <> 1 AND invitaion.user_id='{$_GET[ "user_id" ]}' AND published <> 0" )->run()->event;
	    foreach ( $all_events as $all_event )
	    {
		$data[] = array(
		    'event_id' => "$all_event->event_id",
		    'user_id' => "$all_event->user_id",
		    'form_id' => "$all_event->form_id",
		    'name' => "$all_event->name",
		    'type' => "$all_event->type",
		    'start_date' => "$all_event->start_date",
		    'end_date' => "$all_event->end_date",
		    'start_time' => "$all_event->start_time",
		    'end_time' => "$all_event->end_time",
		    'image' => "$all_event->image",
		    'venue' => "$all_event->venue",
		    'description' => "$all_event->description",
		    'modified_at' => "$all_event->modified_at",
		    'published' => "$all_event->published",
		    'deleted' => "$all_event->deleted" );
	    }
	    $return = array( 'success' => 1, 'error' => 0, 'data' => $data, 'message' => self::SUCCESS_MESSAGE );
	}
	catch ( Exception $ex )
	{
	    
	}
	return $return;
    }

    private function accept_event()
    {
	try
	{
	    $data = array();
	    $return = array( 'success' => 0, 'error' => 1, 'message' => self::ERROR_MESSAGE );
	    $accept_events = PluSQL::from( $this->profile )->event->invitaion->select( '*' )->where( "invitaion.status = 1 AND invitaion.user_id='{$_GET[ "user_id" ]}' AND published = 1 AND deleted <> 1" )->run()->event;
	    foreach ( $accept_events as $accept_event )
	    {
		$data[] = array(
		    'event_id' => "$accept_event->event_id",
		    'user_id' => "$accept_event->user_id",
		    'form_id' => "$accept_event->form_id",
		    'name' => "$accept_event->name",
		    'type' => "$accept_event->type",
		    'start_date' => "$accept_event->start_date",
		    'end_date' => "$accept_event->end_date",
		    'start_time' => "$accept_event->start_time",
		    'end_time' => "$accept_event->end_time",
		    'image' => "$accept_event->image",
		    'venue' => "$accept_event->venue",
		    'description' => "$accept_event->description",
		    'modified_at' => "$accept_event->modified_at",
		    'published' => "$accept_event->published",
		    'deleted' => "$accept_event->deleted" );
	    }
	    $return = array( 'success' => 1, 'error' => 0, 'data' => $data, 'message' => self::SUCCESS_MESSAGE );
	}
	catch ( Exception $ex )
	{
	    
	}

	return $return;
    }

    private function accept_event_details()
    {
	try
	{
	    $data = array();
	    $return = array( 'success' => 0, 'error' => 1, 'message' => self::ERROR_MESSAGE );
	    $accept_events = PluSQL::from( $this->profile )->event->invitaion->select( '*' )->where( "invitaion.event_id = '{$_POST[ "event_id" ]}' AND invitaion.status = 1 AND invitaion.user_id='{$_GET[ "user_id" ]}' AND published = 1 AND deleted <> 1" )->run()->event;
	    foreach ( $accept_events as $accept_event )
	    {
		$data[] = array(
		    'event_id' => "$accept_event->event_id",
		    'user_id' => "$accept_event->user_id",
		    'form_id' => "$accept_event->form_id",
		    'name' => "$accept_event->name",
		    'type' => "$accept_event->type",
		    'start_date' => "$accept_event->start_date",
		    'end_date' => "$accept_event->end_date",
		    'start_time' => "$accept_event->start_time",
		    'end_time' => "$accept_event->end_time",
		    'image' => "$accept_event->image",
		    'venue' => "$accept_event->venue",
		    'description' => "$accept_event->description",
		    'modified_at' => "$accept_event->modified_at",
		    'published' => "$accept_event->published",
		    'deleted' => "$accept_event->deleted" );
	    }
	    $return = array( 'success' => 1, 'error' => 0, 'data' => $data, 'message' => self::SUCCESS_MESSAGE );
	}
	catch ( Exception $ex )
	{
	    
	}

	return $return;
    }

    private function reject_event()
    {
	try
	{
	    $data = array();
	    $return = array( 'success' => 0, 'error' => 1, 'message' => self::ERROR_MESSAGE );
	    $reject_events = PluSQL::from( $this->profile )->event->invitaion->select( '*' )->where( "invitaion.status = 2 AND invitaion.user_id='{$_GET[ "user_id" ]}'" )->run()->event;
	    foreach ( $reject_events as $reject_event )
	    {
		$data[] = array(
		    'event_id' => "$reject_event->event_id",
		    'user_id' => "$reject_event->user_id",
		    'form_id' => "$reject_event->form_id",
		    'name' => "$reject_event->name",
		    'type' => "$reject_event->type",
		    'start_date' => "$reject_event->start_date",
		    'end_date' => "$reject_event->end_date",
		    'start_time' => "$reject_event->start_time",
		    'end_time' => "$reject_event->end_time",
		    'image' => "$reject_event->image",
		    'venue' => "$reject_event->venue",
		    'description' => "$reject_event->description",
		    'modified_at' => "$reject_event->modified_at",
		    'published' => "$reject_event->published",
		    'deleted' => "$reject_event->deleted" );
	    }
	    $return = array( 'success' => 1, 'error' => 0, 'data' => $data, 'message' => self::SUCCESS_MESSAGE );
	}
	catch ( Exception $ex )
	{
	    
	}
	return $return;
    }

    private function pending_event()
    {
	try
	{
	    $data = array();
	    $return = array( 'success' => 0, 'error' => 1, 'message' => self::ERROR_MESSAGE );
	    $accept_events = PluSQL::from( $this->profile )->event->invitaion->select( '*' )->where( "invitaion.status = 0 AND invitaion.user_id='{$_GET[ "user_id" ]}'" )->run()->event;
	    foreach ( $accept_events as $accept_event )
	    {
		$data[] = array(
		    'event_id' => "$accept_event->event_id",
		    'user_id' => "$accept_event->user_id",
		    'form_id' => "$accept_event->form_id",
		    'name' => "$accept_event->name",
		    'type' => "$accept_event->type",
		    'start_date' => "$accept_event->start_date",
		    'end_date' => "$accept_event->end_date",
		    'start_time' => "$accept_event->start_time",
		    'end_time' => "$accept_event->end_time",
		    'image' => "$accept_event->image",
		    'venue' => "$accept_event->venue",
		    'description' => "$accept_event->description",
		    'modified_at' => "$accept_event->modified_at",
		    'published' => "$accept_event->published",
		    'deleted' => "$accept_event->deleted" );
	    }
	    $return = array( 'success' => 1, 'error' => 0, 'data' => $data, 'message' => self::SUCCESS_MESSAGE );
	}
	catch ( Exception $ex )
	{
	    
	}

	return $return;
    }

    private function form()
    {
	try
	{
	    $data = array();
	    $return = array( 'success' => 0, 'error' => 1, 'message' => self::ERROR_MESSAGE );
	    $forms = PluSQL::from( $this->profile )->form->invitaion->select( '*' )->where( "deleted <> '1' AND form.form_id='{$_POST[ 'form_id' ]}' AND invitaion.event_id='{$_POST[ "event_id" ]}'" )->limit( '0, 1' )->run()->form;
	    foreach ( $forms as $form )
		$return = array( 'form_data' => unserialize( $form->form_fields ), 'data' => $this->form_data_send()[ 'data' ], 'success' => self::SUCCESS_MESSAGE );
	}
	catch ( Exception $ex )
	{
	    
	}
	return $return;
    }

    private function form_data_send()
    {
	try
	{
	    $data = array();
	    $return = array( 'success' => 0, 'error' => 1, 'message' => self::ERROR_MESSAGE );
	    $user_id = $_GET[ 'user_id' ];
	    $query = "SELECT * FROM form_data WHERE user_id={$user_id}";
	    $form_details = PluSQL::against( $this->profile )->run( $query );
	    while ( $row = $form_details->nextRow() )
	    {
		$data[] = array(
		    'form_data_id' => $row[ 'form_data_id' ],
		    'user_id' => $row[ 'user_id' ],
		    'data_key' => $row[ 'data_key' ],
		    'value' => $row[ 'value' ] );
	    }
	    $return = array( 'success' => 1, 'error' => 0, 'data' => $data, 'message' => self::SUCCESS_MESSAGE );
	}
	catch ( Exception $ex )
	{
	    echo $ex->getMessage();
	}
	return $return;
    }

    private function form_details()
    {
	try
	{
	    $data = array();
	    $return = array( 'success' => 0, 'error' => 1, 'message' => self::ERROR_MESSAGE );
	    $form_details = PluSQL::from( $this->profile )->invitaion->form->select( '*' )->where( "invitaion.user_id='{$_GET[ "user_id" ]}' AND form.deleted <> '1' AND invitaion.event_id='{$_POST[ "event_id" ]}' AND invitaion.status = 0" )->run()->invitaion;
	    foreach ( $form_details as $form_detail )
	    {
		$data[] = array(
		    'form_id' => "$form_detail->form_id",
		    'user_id' => "$form_detail->user_id",
		    'name' => "$form_detail->name",
		    'form_fields' => "$form_detail->form_fields" );
	    }
	    $return = array( 'success' => 1, 'error' => 0, 'data' => $data, 'message' => self::SUCCESS_MESSAGE );
	}
	catch ( Exception $ex )
	{
	    
	}
	return $return;
    }

    private function accept_form_details()
    {
	try
	{
	    $data = array();
	    $return = array( 'success' => 0, 'error' => 1, 'message' => self::ERROR_MESSAGE );
	    $form_details = PluSQL::from( $this->profile )->invitaion->form->select( '*' )->where( "invitaion.user_id='{$_GET[ "user_id" ]}' AND form.deleted <> '1' AND invitaion.event_id='{$_POST[ "event_id" ]}' AND invitaion.status = 1" )->run()->invitaion;
	    foreach ( $form_details as $form_detail )
	    {
		$data[] = array(
		    'form_id' => "$form_detail->form_id",
		    'user_id' => "$form_detail->user_id",
		    'name' => "$form_detail->name",
		    'form_fields' => "$form_detail->form_fields" );
	    }
	    $return = array( 'success' => 1, 'error' => 0, 'data' => $data, 'message' => self::SUCCESS_MESSAGE );
	}
	catch ( Exception $ex )
	{
	    
	}
	return $return;
    }

    private function reject_form_details()
    {
	try
	{
	    $data = array();
	    $return = array( 'success' => 0, 'error' => 1, 'message' => self::ERROR_MESSAGE );
	    $form_details = PluSQL::from( $this->profile )->invitaion->form->select( '*' )->where( "invitaion.user_id='{$_GET[ "user_id" ]}' AND form.deleted <> '1' AND invitaion.event_id='{$_POST[ "event_id" ]}' AND invitaion.status = '2'" )->run()->invitaion;
	    foreach ( $form_details as $form_detail )
	    {
		$data[] = array(
		    'form_id' => "$form_detail->form_id",
		    'user_id' => "$form_detail->user_id",
		    'name' => "$form_detail->name",
		    'form_fields' => "$form_detail->form_fields" );
	    }
	    $return = array( 'success' => 1, 'error' => 0, 'data' => $data, 'message' => self::SUCCESS_MESSAGE );
	}
	catch ( Exception $ex )
	{
	    
	}
	return $return;
    }

    private function form_insertion()
    {
	try
	{
	    $data = array();
	    $return = array( 'success' => 0, 'error' => 1, 'message' => self::ERROR_MESSAGE );
	    $user_id = $_GET[ "user_id" ];
	    $form_id = $_POST[ 'form_id' ];
	    $status = $_POST[ 'status' ];
	    $event_id = $_POST[ 'event_id' ];
	    //---------- Form Data -------------
	    foreach ( $_POST[ 'form_data' ] as $key => $val )
	    {
		$form_data = array(
		    'user_id' => $user_id,
		    'data_key' => $key,
		    'value' => $val
		);
		PluSQL::into( $this->profile )->form_data( $form_data )->insert();
	    }
	    $data = array( 'user_id' => $user_id, 'form_id' => $form_id, 'event_id' => $event_id, 'status' => $status );
	    if ( $_POST[ 'status' ] )
	    {
		Plusql::on( $this->profile )->invitaion( $data )->where( "form_id=$form_id AND user_id=$user_id AND event_id = '$event_id'" )->update();
		$return = array( 'success' => 1, 'error' => 0, 'data' => $invites, 'message' => self::SUCCESS_MESSAGE );
	    }
	    else
	    {
		$return = array( 'success' => 0, 'error' => 1, 'data' => $invites, 'message' => self::ERROR_MESSAGE );
	    }
	}
	catch ( Exception $ex )
	{
	    
	}
	return $return;
    }

    private function user( $bcrypt )
    {
	try
	{            
	    $corrupt = false;
	    //$email = $this->central->getargs( 'email', $_POST[ 'email' ], $corrupt );
	    //$password = $this->central->getargs( 'password', $_POST[ 'password' ], $corrupt );
	    $email = $_POST[ 'email' ];
	    $password = $_POST[ 'password' ];
	    $data = array();
	    $return = array( 'success' => 0, 'error' => 1, 'message' => self::ERROR_MESSAGE );
	    if ( !$corrupt )
	    {
		$users = PluSQL::from( $this->profile )->user->select( '*' )->where( "email='$email' AND status='active'" )->run()->user;
		foreach ( $users as $user )
		{
		    $user_password = $bcrypt->verify( $password, $user->password );
		    if ( $user_password )
		    {
			$data[] = array(
			    'user_id' => "$user->user_id",
			    'user_name' => "$user->user_name",
			    'email' => "$user->email",
			    'password' => "$user_password",
			    'school' => "$user->school",
			    'profile_pic' => "$user->profile_pic",
			    'is_admin' => "$user->is_admin",
			    'status' => "$user->status",
			);
			$return = array( 'success' => 1, 'error' => 0, 'data' => $data, 'message' => self::SUCCESS_MESSAGE );
		    }
		    else
		    {
			$return = array( 'success' => 0, 'error' => 1, 'data' => $data, 'message' => self::ERROR_MESSAGE );
		    }
		}
	    }
	}
	catch ( Exception $ex )
	{
	    
	}
	return $return;
    }

}

?>
