<?php

namespace Config;

use Config\Central;
use Config\Constants;
use \SalimApi;

abstract class RSBase implements \Rocketsled\Runnable {

    //--private members
    protected $template;
    protected $central;
    protected $profile = 'dm';
    protected $esc;
    private $info = array();
    private $redirect;
    private $test_mode = 1;

    //--public members
    //--constructor
    public function __construct() {
        try {
            // date_default_timezone_set("America/Chicago");
            @session_start();
            // Load DB credentials and supported classes
            $this->central = Central::instance();
            $this->central->set_alias_connection($this->profile);
        } catch (Exception $e) {
            throw $e;
        }
    }

    //render function
    public function render($display = 1) {
        try {
            if ($this->central->check_user_login_status($this->profile)) {
                try {
                    $this->prepare_template();
                    $this->update_main_contents();
                    if ($display && !is_null($this->template)) {
//                        $this->template->remove('.static');
                        $this->central->render($this->template);
                    }
                } catch (Exception $e) {
                    @header('location: ?r=DMErrorPage');
                }
            } else {
                @header('location: ?r=Login');
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    //prepare the template
    public function prepare_template() {
        try {
            if (is_null($this->template))
                return;
            $this->template->setValue('#singout@href', '?r=Login&' . Central::base64url_encode('logout') . '=' . Central::base64url_encode('yes'));
            $this->template->setValue('#user_profile@href', '?r=Profile');
            $this->template->setValue('.logo@href', '?r=Dashboard');

            if ($_SESSION['user']['role'] == 'admin') {
                $this->update_menu_for_admin();
            } else {
                $this->update_menu_for_member();
            }
        } catch (Exception $e) {
            //do nothing
        }
    }

    private function update_menu_for_admin() {
        $this->template->setValue('#dashboard/a@href', '?r=Dashboard');
        $this->template->setValue('#events/a@href', '?r=Events');
        $this->template->setValue('#new_event/a@href', '?r=Events&NewEvent=MQ');
        $this->template->setValue('#manage_users/a@href', '?r=Users');
        $this->template->setValue('#forms/a@href', '?r=Forms');
        $this->template->setValue('#new_form/a@href', '?r=Forms&NewForm=MQ');
    }

    private function update_menu_for_member() {
        
    }

    //get template function
    public function get_template() {
        return $this->template;
    }

    public function run($verbose = true) {
        try {
            if ($verbose) {
                $this->render(true);
            }
        } catch (Exception $e) {
            if ($verbose) {
                //echo $e->getMessage();
            } else {
                throw $e; //for the Murphy
            }
        }
    }

    //get user information and store it into the session


    protected function __redirect($url) {
        ?><html><body><script type="text/javascript">top.location.href = '<?php echo $url; ?>';</script></body></html><?php
        die();
    }

    //abstract function
    abstract protected function update_main_contents();
}
?>
