<?php

@session_start();

use \Config\Constants;
use \Config\Central;

class AuthAction implements RocketSled\Runnable {

    //--private members
    private $file_name = "login.html";
    private $profile = "login";
    private $template;
    private $central;

    //--constructor
    public function __construct() {
        try {
            $this->central = Central::instance();
            $this->central->set_alias_connection($this->profile);
            $this->template = $this->central->load_normal($this->file_name);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function run() {
        try {
            $corrupt = false;
            $this->update_main_contents();
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function update_main_contents() {
        try {
            $bcrypt = new \Config\Bcrypt();
            if ($_POST && isset($_POST['singin'])) {
                $this->LoginAction($bcrypt);
            } elseif ($_POST && isset($_POST['register'])) {
                $this->RegisterAction($bcrypt);
            } elseif ($_POST && isset($_POST['reset_pas'])) {
                $this->ResetPassAction($bcrypt);
            }
        } catch (Exception $ex) {
            
        }
    }

    private function LoginAction($bcrypt) {
        try {
            $corrupt = false;
            $email = $this->central->getargs('email', $_POST, $corrupt);
            $password = $this->central->getargs('password', $_POST, $corrupt);
            if (!$corrupt) {
                try {
                    $is_admin = PluSQL::from($this->profile)->user->select('*')->where("email='{$email}' AND status = 'active'")->limit('0,1')->run()->user;
                    foreach ($is_admin as $admin) {
                        if ($admin->is_admin == 1) {
                            if ($bcrypt->verify($password, $admin->password)) {
                                $_SESSION['user']['user_id'] = $admin->user_id;
                                $_SESSION['user']['role'] = 'admin';
                                unset($_SESSION['login_erros']);
                                @header('location: ?r=Dashboard');
                            } else {
                                $_SESSION['login_email'] = $email;
                                $_SESSION['login_pass'] = $password;
                                $_SESSION['login_erros']['password'] = 1;
                                @header('location: ?r=Login');
                            }
                        } else {
                            $_SESSION['login_email'] = $email;
                            $_SESSION['login_pass'] = $password;
                            $_SESSION['login_erros']['email'] = 1;
                            @header('location: ?r=Login');
                        }
                    }
                } catch (Exception $ex) {
                    $_SESSION['login_email'] = $email;
                    $_SESSION['login_pass'] = $password;
                    $_SESSION['login_erros']['email'] = 1;
                    @header('location: ?r=Login');
                }
            } else {
                $_SESSION['login_erros']['smthng'] = 1;
                @header('location: ?r=Login');
            }
        } catch (Exception $ex) {
            $_SESSION['login_erros']['smthng'] = 1;
            @header('location: ?r=Login');
        }
    }

    private function RegisterAction($bcrypt) {
        try {
            $profilepic = 'default.png';
            $email = $this->central->getargs('email', $_POST, $corrupt);
            if ($_POST && $_POST['ajax']) {
                $this->CheckDuplicate($email);
            }
            $name = $this->central->getargs('name', $_POST, $corrupt);
            $password = $this->central->getargs('pas', $_POST, $corrupt);
            $school = $this->central->getargs('schol', $_POST, $corrupt);
            if (!$corrupt) {
                $is_already = $this->CheckDuplicate($email, 1);
                if ($is_already['error']) {
                    $_SESSION['DuplicateInRegister'] = 'Already there.';
                    $this->SetSessionValue($name, $email, $password, $school);
                    header('Location:?r=Signup');
                }
//                if (isset($_FILES['profile_pic']['tmp_name'])) {
//                    $image_extension = strtolower(end(explode(".", $_FILES["profile_pic"]["name"])));
//                    if ($image_extension == "jpg" || $image_extension == "png" || $image_extension == "jpeg") {
//                        $image_name = sha1(time()) . "." . $image_extension;
//                        $target_file = 'packages/profilepics/' . $image_name;
//                        move_uploaded_file($_FILES["profile_pic"]["tmp_name"], $target_file);
//                        $profilepic = $image_name;
//                    } else {
//                        $this->SetSessionValue($name, $email, $password, $school);
//                        $_SESSION['ProfilePicEror'] = 1;
//                        header('Location:?r=Signup');
//                    }
//                }
                $password = $bcrypt->hash($password);
                $args = array('user_name' => $name,
                    'email' => $email,
                    'password' => $password,
                    'school' => $school,
                    'profile_pic' => $profilepic,
                    'is_admin' => 1,
                    'status' => 'active');
                Plusql::into($this->profile)->user($args)->insert();
                header('Location:?r=Login');
            } else {
                header('Location:?r=Signup');
            }
        } catch (Exception $ex) {
            header('Location:?r=Signup');
        }
    }

    private function SetSessionValue($name, $email, $password, $school) {
        try {
            $_SESSION['NameInRegister'] = $name;
            $_SESSION['EmailInRegister'] = $email;
            $_SESSION['PassInRegister'] = $password;
            $_SESSION['SchoolInRegister'] = $school;
        } catch (Exception $ex) {
            
        }
    }

    private function CheckDuplicate($email, $flag = 0) {
        try {
            $response = '';
            $isthere = PluSQL::from($this->profile)->user->select('*')->where("email='$email'")->run()->user;
            if ($isthere)
                $response = array('success' => 0, 'error' => 1);
        } catch (Exception $ex) {
            $response = array('success' => 1, 'error' => 0);
        }
        if ($flag) {
            return $response;
        } else {
            die(json_encode($response));
        }
    }

    private function ResetPassAction($bcrypt) {
        try {
            if (isset($_POST['tmplink'])) {
                $this->UpdatePass($bcrypt);
            } else {
                $this->SendEmailForResetPass();
            }
        } catch (Exception $ex) {
            
        }
    }

    private function UpdatePass($bcrypt) {
        try {
            $corrupt = false;
            $password = $this->central->getargs('pas', $_POST, $corrupt);
            $againpas = $this->central->getargs('again_pas', $_POST, $corrupt);
            $tmplink = $this->central->getargs('tmplink', $_POST, $corrupt);
            if (!$corrupt) {
                if (strcmp($password, $againpas) == 0) {
                    $user = Plusql::from($this->profile)->user->select("*")->where("temp_link = '{$tmplink}'")->limit('0,1')->run()->user;
                    try {
                        $reset_pwd_data = array(
                            "password" => $bcrypt->hash($password),
                            "temp_link" => ''
                        );
                        Plusql::on($this->profile)->user($reset_pwd_data)->where("user_id=" . $user->user_id)->update();
                        $_SESSION['ResetScs'] = 1;
                        unset($_SESSION['pass']);
                        unset($_SESSION['again_pass']);
                        unset($_SESSION['reset_pass_div']);
                        @header('location: ?r=Login');
                    } catch (EmptySetException $ex) {
                        $_SESSION['UpdatePasSmthng'] = 1;
                        @header('location: ?r=ForgotPassword&' . Central::base64url_encode('temp_link') . "=$tmplink");
                    }
                } else {
                    $_SESSION['UpdateAgainPass'] = 1;
                    @header('location: ?r=ForgotPassword&' . Central::base64url_encode('temp_link') . "=$tmplink");
                }
            } else {
                $_SESSION['UpdatePasSmthng'] = 1;
                @header('location: ?r=ForgotPassword&' . Central::base64url_encode('temp_link') . "=$tmplink");
            }
        } catch (Exception $ex) {
            $_SESSION['UpdatePasSmthng'] = 1;
            @header('location: ?r=ForgotPassword&' . Central::base64url_encode('temp_link') . "=$tmplink");
        }
    }

    private function SendEmailForResetPass() {
        try {
            $corrupt = false;
            $email = $this->central->getargs('email', $_POST, $corrupt);
            if (!$corrupt) {
                $user = Plusql::from($this->profile)->user->select("*")->where("email = '{$email}' AND status <> '" . Constants::STATUS_DELETED . "'")->limit('0,1')->run()->user;
                if ($user) {
                    try {
                        $temporary_link = md5($user->user_id . $this->central->create_password_randomly());
                        $reset_pwd_data = array(
                            "email" => $_POST["email"],
                            "temp_link" => $temporary_link
                        );
                        Plusql::on($this->profile)->user($reset_pwd_data)->where("email='" . $user->email . "'")->update();
                    } catch (EmptySetException $ex) {
                        
                    }

                    $email_body = "<p style='font:normal 18px Arial,serif'>"
                            . "Change Password Request:</p>
			   You are receiving this email because a request has been made for a reset 
			   password link for {$user->user_name}.<br>
			   <a href='" . $this->central->get_server_url() . "?r=ForgotPassword&" . Central::base64url_encode('temp_link') . "={$temporary_link}'>Change my password</a> <br>
			   If you didn't request this, please ignore this email. 
			   Your password won't change until you access the link above and create a new one."
                            . "<p style='color:#757679'>Sincerely, \n Customer Service</p>";
                    $email_data = array('email' => $user->email,
                        'subject' => 'Password Reset',
                        'text_body' => $email_body,
                        'html_body' => str_replace('\n', '<br />', $email_body)
                    );
                    try {
                        if ($this->central->send_email($email_data)) {
                            unset($_SESSION['ForgotPassEmail']);
                            $_SESSION['email_success'] = 1;
                        } else {
                            $_SESSION['Smthng'] = 1;
                        }
                    } catch (Exception $ex) {
                        $_SESSION['Smthng'] = 1;
                    }
                } else {
                    $_SESSION['ForgotPassEmailAdress'] = $email;
                    $_SESSION['ForgotPassEmail'] = 1;
                }
            }
        } catch (Exception $e) {
            $_SESSION['ForgotPassEmailAdress'] = $email;
            $_SESSION['ForgotPassEmail'] = 1;
        }
        @header("location: ?r=ForgotPassword");
    }

}

?>