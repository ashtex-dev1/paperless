<?php

use \Config\Constants;
use \Config\Central;

class UserAction implements RocketSled\Runnable {

    //--private members
    private $profile = 'db';
    private $central;

    public function __construct() {
        try {
            @session_start();
            $this->central = Central::instance();
            $this->central->set_alias_connection($this->profile);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function run() {
        try {
            $this->update_main_contents();
        } catch (Exception $ex) {
            
        }
    }

    private function update_main_contents() {
        try {
            
        } catch (Exception $ex) {
            
        }
    }

}
