<?php

use \Config\Constants;
use \Config\Central;

class GetUsers implements RocketSled\Runnable {

    //--private members
    private $profile = 'db';
    private $central;

    public function __construct() {
        try {
            @session_start();
            $this->central = Central::instance();
            $this->central->set_alias_connection($this->profile);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function run() {
        try {
            $this->GetUsers();
        } catch (Exception $ex) {
            
        }
    }

    private function GetUsers() {
        try {
            $userdata = array();
            $users = PluSQL::from($this->profile)->user->select('*')->where("is_admin <> '1' AND status='active'")->run()->user;
            if ($users) {
                foreach ($users as $user) {
                    $userdata[] = array('name' => $user->email, 'email' => $user->email);
                }
            } else {
                $userdata[] = array('name' => ' ', 'email' => ' ');
            }
        } catch (Exception $ex) {
            $userdata[] = array('name' => ' ', 'email' => ' ');
        }
        echo json_encode($userdata);
    }

}
