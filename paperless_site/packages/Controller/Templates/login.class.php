<?php

@session_start();

use \Config\Constants;
use \Config\Central;

class Login implements RocketSled\Runnable {

    //--private members
    private $file_name = "login.html";
    private $profile = "login";
    private $template;
    private $central;

    //--constructor
    public function __construct() {
        try {
            $this->central = Central::instance();
            $this->central->set_alias_connection($this->profile);
            $this->template = $this->central->load_normal($this->file_name);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function run() {
        try {
            $this->check_logout_request();
            if (!$this->central->check_user_login_status($this->profile)) {
                $this->render();
            } else {
                @header('location: ?r=Dashboard');
            }
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    private function render($display = TRUE) {
        try {
            $this->update_main_contents();
            if ($display)
                $this->central->render($this->template);
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function update_main_contents() {
        try {
            $this->CheckEror();
            $this->template->setValue("#login_form@action", "?r=AuthAction");
            $this->template->setValue("#signup@href", "?r=Signup");
            $this->template->setValue("#forget_pass@href", "?r=ForgotPassword");
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    private function CheckEror() {
        try {
            if (isset($_SESSION['ResetScs'])) {
                $this->ShowPassResetScs();
            } else if (isset($_SESSION['login_erros']['password'])) {
                $this->ShowPassEror();
            } else if (isset($_SESSION['login_erros']['email'])) {
                $this->ShowEmailEror();
            } else if (isset($_SESSION['login_erros']['smthng'])) {
                $this->ShowSmthngEror();
            }
        } catch (Exception $ex) {
            
        }
    }

    private function ShowPassEror() {
        try {
            $this->template->setValue('.alert-success@style', 'display:none');
            $this->template->setValue('.alert-danger@style', 'display:block');
            $this->template->setValue('.danger_eror', '<i class="fa fa-warning"> </i> Please enter valid passowrd', 1);
            $this->template->setValue('#email@value', $_SESSION['login_email']);
            $this->template->setValue('#pas@value', $_SESSION['login_pass']);
            unset($_SESSION['login_erros']['password']);
            unset($_SESSION['login_email']);
            unset($_SESSION['login_pass']);
        } catch (Exception $ex) {
            
        }
    }

    private function ShowPassResetScs() {
        try {
            $this->template->setValue('.alert-danger@style', 'display:none');
            $this->template->setValue('.alert-success@style', 'display:block');
            unset($_SESSION['ResetScs']);
        } catch (Exception $ex) {
            
        }
    }

    private function ShowEmailEror() {
        try {
            $this->template->setValue('.alert-success@style', 'display:none');
            $this->template->setValue('.alert-danger@style', 'display:block');
            $this->template->setValue('.danger_eror', '<i class="fa fa-warning"> </i> Email does not exist in our record.', 1);
            $this->template->setValue('#email@value', $_SESSION['login_email']);
            $this->template->setValue('#pas@value', $_SESSION['login_pass']);
            unset($_SESSION['login_erros']['email']);
            unset($_SESSION['login_email']);
            unset($_SESSION['login_pass']);
        } catch (Exception $ex) {
            
        }
    }

    Private function ShowSmthngEror() {
        try {
            $this->template->setValue('.alert-success@style', 'display:none');
            $this->template->setValue('.alert-danger@style', 'display:block');
            $this->template->setValue('.danger_eror', '<i class="fa fa-warning"> </i> Something went wrong, Try again later.', 1);
            unset($_SESSION['login_erros']['smthng']);
        } catch (Exception $ex) {
            
        }
    }

    private function check_logout_request() {
        try {
            $corrupt = false;
            $logout = $this->central->getargs(Central::base64url_encode('logout'), $_GET, $corrupt);
            if (!$corrupt) {
                if (Central::base64url_decode($logout) == 'yes')
                    unset($_SESSION['user']);
            }
        } catch (Exception $ex) {
            // do nothing
        }
    }

}

?>