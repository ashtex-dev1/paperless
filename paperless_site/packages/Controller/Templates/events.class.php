<?php

use \Config\Constants;
use \Config\Central;

class Events extends Config\RSBase {

    public function __construct() {
        try {
            parent::__construct();
            if (isset($_GET['EditEvent'])) {
                $this->LoadTemplate('new_event');
                $this->LoadEditEvent();
            } else if (isset($_GET['ViewEvent'])) {
                $this->LoadTemplate('event_detail');
                $this->LoadViewEvents();
            } else if (isset($_GET['NewEvent'])) {
                $this->LoadTemplate('new_event');
                $this->LoadNewEvent();
            } else {
                $this->LoadTemplate('events');
                $this->LoadEvents();
            }
        } catch (Exception $e) {
            
        }
    }

    public function update_main_contents() {
        try {
//	    $this->template->setValue( '#view@href', '?r=EditEvent=MQ' );
        } catch (Exception $ex) {
            
        }
    }

    private function LoadEvents() {
        try {
            $this->MarkActive('events');
            $this->SetEventPageOthers();
            $this->ShowEventPageMessages();
            $this->PopulateEvents();
        } catch (Exception $ex) {
            
        }
    }

    private function LoadEditEvent() {
        try {
            $this->MarkActive('events');
            $this->SetEditEventOthers();
            $this->ShowEditEventMessages();
            $this->PopulateEvent();
        } catch (Exception $ex) {
            
        }
    }

    private function LoadNewEvent() {
        try {
            $this->MarkActive('new_event');
            $this->SetNewEventOthers();
            $this->PopulateForms();
        } catch (Exception $ex) {
            
        }
    }

    private function SetNewEventOthers() {
        try {
            $this->template->setValue('#new_event_form@action', '?r=EventsAction');
            $this->template->setValue('#home@href', '?r=Dashboard');
            $this->template->setValue('#events@href', '?r=Events');
        } catch (Exception $ex) {
            
        }
    }

    private function PopulateEvent() {
        try {
            $event_id = $this->GetIdFromUrl();
            if ($event_id) {
                try {
                    $event = PluSQL::from($this->profile)->event->select('*')->where("user_id={$_SESSION['user']['user_id']} AND deleted <> '1' AND event_id='$event_id'")->limit('0, 1')->run()->event;
                    $this->template->setValue('#event_name@value', $event->name);
                    $this->template->setValue('#hidden_type@value', $event->type);
                    $this->template->setValue('#hidden_venue@value', $event->venue);
                    $this->template->setValue('#hidden_id@value', $event_id);
//		    $this->template->setValue( '#send_form_id@value', $form_id );
                    $this->template->setValue('#send_form_id@value', $event->form_id);
                    $this->template->setValue('#event_start_date@value', date("m/d/Y", strtotime($event->start_date)));
                    $this->template->setValue('#event_end_date@value', date("m/d/Y", strtotime($event->end_date)));
                    $this->template->setValue('#start_time@value', $event->start_time);
                    $this->template->setValue('#end_time@value', $event->end_time);
                    $this->template->setValue('#event_des', $event->description);
//		    $this->template->remove( '#submit_new_event' );
//		    $this->template->remove( '#new_users' );
//                    die("$event->image");
                    if ($event->image != NULL) {
                        $this->ShowEditEventImage($event->image);
                    }
                    $this->PopulateForms($event->form_id);
                } catch (Exception $ex) {
                    $this->central->NotFound($this->template);
                }
            } else {
                $this->central->NotFound($this->template);
            }
        } catch (Exception $ex) {
            
        }
    }

    private function LoadViewEvents() {
        try {
            $event_id = $this->GetIdFromUrl();
            if ($event_id) {
                try {
                    $this->LoadInvitedUserList($event_id);

                    $event = PluSQL::from($this->profile)->event->select('*')->where("user_id={$_SESSION['user']['user_id']} AND deleted <> '1' AND event_id='$event_id'")->limit('0, 1')->run()->event;
                    $this->template->setValue('#event_name', $event->name);
                    $this->template->setValue('#hidden_type', $event->type);
                    $this->template->setValue('#event_type', $event->type);
                    $this->template->setValue('#hidden_venue', $event->venue);
                    $this->template->setValue('#hidden_id@value', $event_id);
                    $this->template->setValue('#send_form_id@value', $event->form_id);
                    $this->template->setValue('#event_start_date', date("m/d/Y", strtotime($event->start_date)));
                    $this->template->setValue('#event_end_date', date("m/d/Y", strtotime($event->end_date)));
                    $this->template->setValue('#start_time', $event->start_time);
                    $this->template->setValue('#end_time', $event->end_time);
                    $this->template->setValue('#event_des', $event->description);

                    $this->template->remove('#submit_new_event');
                    $this->template->remove('#new_users');
                    $this->template->remove('#send_new_event');
                    if ($event->image != 0)
                        $this->ShowEditEventImage($event->image);
                    $this->PopulateForms($event->form_id);
                } catch (Exception $ex) {
                    $this->central->NotFound($this->template);
                }
            } else {
                $this->central->NotFound($this->template);
            }
        } catch (Exception $ex) {
            
        }
    }

    private function LoadInvitedUserList($event_id) {
        try {

            $invited_users = PluSQL::from($this->profile)->user->invitaion->select('user.user_id,user.user_name, user.email')->where("invitaion.event_id=$event_id")->run()->user;
            $counter = 0;
           // die(PluSQL::from($this->profile)->user->invitaion->select('user.user_id,user.user_name, user.email')->where("invitaion.event_id=$event_id"));
           $repeat = $this->template->repeat('#invited_users_detail');
            foreach ($invited_users as $users) {
                $repeat->setValue('#counter',  ++$counter);                
                $repeat->setValue('#user_email_detail', $users->email);  
                $repeat->next();
            }
        } catch (Exception $ex) {
            print_r($ex);
        }
    }

    private function PopulateForms($default = 0) {
        try {
            $html = '';
            $selected_forms = 0;
            $form_ids = array();
            if ($default) {
                $selected_forms = explode(',', $default);
                $this->template->setValue('#form_ids@value', $default);
                $this->template->setValue('.badge', count($selected_forms));
                foreach ($selected_forms as $form_id) {
                    array_push($form_ids, $form_id);
                }
            }
            $rows = Plusql::from($this->profile)->form->select('*')->where("user_id = {$_SESSION['user']['user_id']} AND deleted <> 1")->run()->form;
            foreach ($rows as $row)
                if (in_array($row->form_id, $form_ids))
                    $html .= "<div class='form-border' title='$row->name'>
                            <input type='hidden' id='form_id' value='$row->form_id'/>
                            <img src='packages/Views/dist/img/form.png'  />
                            <input type='checkbox' checked='checked' class='check-form'/>
                            <p class='form-name-noted' title='$row->name'> $row->name</p>
                        </div>";
                else {
                    $html .= "<div class='form-border' title='$row->name'>
                            <input type='hidden' id='form_id' value='$row->form_id'/>
                            <img src='packages/Views/dist/img/form.png'  />
                            <input type='checkbox' class='check-form'/>
                            <p class='form-name-noted' title='$row->name'> $row->name</p>
                        </div>";
                }
        } catch (Exception $ex) {
            $html = "<span> Sorry! You have not created any form yet . </span>"
                    . "<a href='?r=Forms&amp;NewForm=MQ'> Click here </a> for creating new form.";
            $this->template->setValue('.from_availbility', $html, 1);
        }
        $this->template->setValue('.all-form-view', $html, 1);
    }

    private function GetIdFromUrl() {
        try {
            $corrupt = false;
            $isget = $this->central->getargs(Central::base64url_encode('event_id'), $_GET, $corrupt);
            if (!$corrupt) {
                $event_id = Central::base64url_decode($isget);
            }
        } catch (Exception $ex) {
            $event_id = 0;
        }
        return $event_id;
    }

    private function ShowEditEventImage($img) {
        try {
            $this->template->addClass("#imgdiv", "img-main");
            $this->template->addClass("#img-label", "img-label");
            $this->template->addClass("#event_image", "file-type");
            $this->template->addClass("#evntimg", "img-size");
            $this->template->setValue("#evntimg@style", "display:block");
            $this->template->setValue('#evntimg@src', "packages/eventimg/$img");
        } catch (Exception $ex) {
            
        }
    }

    private function ShowEditEventMessages() {
        try {
            if (isset($_SESSION['event']['updated'])) {
                $this->template->setValue('.eventscs@style', 'display:block');
                $this->template->setValue('#eventmsg', 'Event has been successfully updated.');
                unset($_SESSION['event']['updated']);
            }
        } catch (Exception $ex) {
            
        }
    }

    private function SetEditEventOthers() {
        try {
            $this->template->setValue('#new_event_form@action', '?r=EventsAction');
            $this->template->setValue('#events@href', '?r=Events');
            $this->template->setValue('#home@href', '?r=Dashboard');
            $this->template->setValue('#submit_new_event@name', 'update_event');
//	    $this->template->setValue( '#submit_new_event', 'Update' );
            $this->template->setValue('#heading1', 'Edit Event');
            $this->template->setValue('.heading2', 'Edit Event');
            $this->template->setValue('.box-title', 'Edit Event');
        } catch (Exception $ex) {
            
        }
    }

    private function SetEventPageOthers() {
        try {
            $this->template->setValue('#home@href', '?r=Dashboard');
            $this->template->setValue('#delete_event@action', '?r=EventsAction');
            $this->template->setValue('#all_users@action', '?r=EventsAction');
        } catch (Exception $ex) {
            
        }
    }

    private function PopulateEvents() {
        try {
            $this->ShowEvents('.rep_event', $this->GetAllEvents(), 1);
        } catch (Exception $ex) {
            $this->NoRecord('.rep_event', '#stop_events', 1);
        }
        try {
            $this->ShowEvents('.rep_pb_events', $this->GetAllEvents(1), 0);
        } catch (Exception $ex) {
            $this->NoRecord('.rep_pb_events', '#stop_pb_events', 0);
        }
    }

    private function NoRecord($rep, $stop, $drafted) {
        try {
            if ($drafted) {
                $this->template->setValue($rep, "<td></td><td>No matching records found</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>", 1);
                $this->template->remove($stop);
            } else {
                $this->template->setValue($rep, "<td></td><td>No matching records found</td><td></td><td></td><td></td><td></td><td></td><td></td>", 1);
                $this->template->remove($stop);
            }
        } catch (Exception $ex) {
            
        }
    }

    private function GetAllEvents($flag = 0) {
        try {
            $user_id = $_SESSION['user']['user_id'];
            $events = PluSQL::from($this->profile)->event->select('*')->where("user_id='$user_id' AND published = '$flag' AND deleted <> '1'")->orderBy('name ASC')->run()->event;
            return $events;
        } catch (Exception $ex) {
            
        }
    }

    private function ShowEvents($class, $events, $drafted) {
        try {
            $item = $this->template->repeat($class);
            if ($drafted) {
                $this->Populate($item, $events, $drafted);
            } else {
                $this->Populate($item, $events);
            }
        } catch (Exception $ex) {
            
        }
    }

    private function Populate($item, $events, $drafted = 0) {
        try {
            $cnt = 0;
            if ($drafted) {
                foreach ($events as $event) {
                    $cnt++;
                    $item->setValue('#name', $event->name);
                    $item->setValue('#set_form_id@value', $event->form_id);
                    $item->setValue('#type', str_replace('_', ' ', $event->type));
                    $item->setValue('#start_date', $event->start_date);
                    $item->setValue('#end_date', $event->end_date);
                    $item->setValue('#venue', str_replace('_', ' ', $event->venue));
                    $item->setValue('#venue@title', str_replace('_', ' ', $event->venue));
                    $item->setValue('#des', $event->description);
                    $item->setValue('#des@title', $event->description);
                    $item->setValue('#edit_event@href', '?r=Events&EditEvent=MQ&' . Central::base64url_encode('event_id') . '=' . Central::base64url_encode($event->event_id));
                    $item->setValue('#event_id@value', Central::base64url_encode($event->event_id));
                    $item->setValue('#cnt', $cnt);
                    $item->next();
                }
                Central::remove_last_repeating_element($this->template, '#stop_events', 1, 2, 0);
                $this->template->remove('#stop_events');
            } else {
                foreach ($events as $event) {
                    $cnt++;
                    $item->setValue('#pb_name', $event->name);
                    $item->setValue('#pb_type', str_replace('_', ' ', $event->type));
                    $item->setValue('#pb_start_date', $event->start_date);
                    $item->setValue('#pb_end_date', $event->end_date);
                    $item->setValue('#pb_venue', str_replace('_', ' ', $event->venue));
                    $item->setValue('#pb_venue@title', str_replace('_', ' ', $event->venue));
                    $item->setValue('#pb_des', $event->description);
                    $item->setValue('#pb_des@title', $event->description);
                    $item->setValue('#pb_date', $event->modified_at);
                    $item->setValue('#cnt', $cnt);
                    $item->setValue('#view_event@href', '?r=Events&ViewEvent=MQ&' . Central::base64url_encode('event_id') . '=' . Central::base64url_encode($event->event_id));
                    $item->setValue('#event_id@value', Central::base64url_encode($event->event_id));
                    $item->next();
                }
                Central::remove_last_repeating_element($this->template, '#stop_pb_events', 1, 2, 0);
                $this->template->remove('#stop_pb_events');
            }
        } catch (Exception $ex) {
            
        }
    }

    private function MarkActive($id) {
        try {
            $this->template->query('#events_menu')->item(0)->setAttribute('class', 'treeview active');
            $this->template->query("#events_menu/ul/li#$id")->item(0)->setAttribute('class', 'active');
        } catch (Exception $ex) {
            
        }
    }

    private function LoadTemplate($file) {
        try {
            $this->template = $this->central->load_normal("$file.html");
            $this->central->populate_user_contents($this->template);
        } catch (Exception $ex) {
            
        }
    }

    private function ShowEventPageMessages() {
        try {
            if (isset($_SESSION['event']['inserted'])) {
                $this->template->setValue('.eventscs@style', 'display:block');
                $this->template->setValue('#eventmsg', 'New event has been successfully created.');
                unset($_SESSION['event']['inserted']);
            } else if (isset($_SESSION['event']['EventSent'])) {
                $this->template->setValue('.eventscs@style', 'display:block');
                $this->template->setValue('#eventmsg', 'Event Invitation has been successfully sent to parents.');
                unset($_SESSION['event']['EventSent']);
            } else if (isset($_SESSION['event']['MailEror'])) {
                $this->template->setValue('.alert-danger@style', 'display:block');
                $this->template->setValue('#mailmsg', 'Something went wrong, Please try again later.');
                unset($_SESSION['event']['MailEror']);
            } else if (isset($_SESSION['event']['EventSentEror'])) {
                $this->template->setValue('.alert-danger@style', 'display:block');
                $this->template->setValue('#mailmsg', 'Something went wrong, Please send event later.');
                unset($_SESSION['event']['EventSentEror']);
            }
        } catch (Exception $ex) {
            
        }
    }

}

?>