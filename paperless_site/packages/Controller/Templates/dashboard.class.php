<?php

use \Config\Constants;
use \Config\Central;

class Dashboard extends Config\RSBase {

    private $file_name = "dashboard.html";

    public function __construct() {
        try {
            parent::__construct();
            $this->template = $this->central->load_normal($this->file_name);
            $this->central->populate_user_contents($this->template);
            $this->template->query('#dashboard')->item(0)->setAttribute('class', 'treeview active');
        } catch (Exception $e) {
            
        }
    }

    public function update_main_contents() {
        try {
            if (isset($_POST['get_count']))
                $this->GetCount(1);
            $this->PopulateStat();
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    private function PopulateStat() {
        try {
            $count = $this->GetCount();
            $this->template->setValue('.events-count', $count['events']);
            $this->template->setValue('.forms-count', $count['forms']);
            $this->template->setValue('.users-count', $count['users']);
        } catch (Exception $ex) {
            
        }
    }

    private function GetCount($ajax = 0) {
        try {
            $events = PluSQL::from($this->profile)->event->select("count(*) as cnt,event_id")->where("user_id={$_SESSION['user']['user_id']} AND deleted <> 1")->run()->event->cnt;
        } catch (Exception $ex) {
            $events = 0;
        }
        try {
            $forms = PluSQL::from($this->profile)->form->select("count(*) as cnt,form_id")->where("user_id={$_SESSION['user']['user_id']} AND deleted <> 1")->run()->form->cnt;
        } catch (Exception $ex) {
            $forms = 0;
        }
        try {
            $users = PluSQL::from($this->profile)->user->select("count(*) as cnt,user_id")->where("created_by={$_SESSION['user']['user_id']} AND status='active' AND is_admin <> '1'")->run()->user->cnt;
        } catch (Exception $ex) {
            $users = 0;
        }
        $array = array('events' => $events, 'forms' => $forms, 'users' => $users);
        if ($ajax)
            die(json_encode($array));
        else
            return $array;
    }

}

?>