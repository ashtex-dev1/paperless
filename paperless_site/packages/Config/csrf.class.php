<?php
namespace  Config;
class Csrf
{
    //the constant for the CSRF attack
    const CSRF_ATTACK = 'csrf_attack';
    //add csrf token
    public function add_csrf_token($profile =  self::CSRF_ATTACK)
    {
         $token= md5(uniqid());
         $_SESSION[profile] = $token;
         return $_SESSION[profile];
    }
    //validate csrf token
    public function validate_csrf_token($token, $profile =  self::CSRF_ATTACK)
    {
         if($token == $_SESSION[profile])
         {
             return true;
         }
         return false;
    }
}
?>
