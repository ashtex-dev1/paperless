$(document).ready(function () {
    $(".datatables").DataTable();
    var url = window.location.href;
    if (url.indexOf('?r=Signup') !== -1) {
        Registerjs();
    } else if (url.indexOf('?r=Login') !== -1) {
        Loginjs();
    } else if (url.indexOf('?r=ForgotPassword') !== -1) {
        ForgotPassjs();
    } else if (url.indexOf('?r=Profile') !== -1) {
        Profilejs();
    }

    function ForgotPassjs() {
        $('body').delegate('#forgot_pas_form', 'submit', function () {
            var all_ok = true;
            if (!check_error('email'))
                all_ok = false;
            if (!validateEmail('email')) {
                $('.email_eror').html('<i class="fa fa-warning"> </i> Please enter valid email address.').show();
                return false;
            }

            if (!all_ok)
                return false;
        });
        $('body').delegate('#forgot_pas_field_form', 'submit', function () {
            var all_ok = true;
            if (!check_error('pas'))
                all_ok = false;
            if (!check_error('again_pas'))
                all_ok = false;
            if (($('#pas').val().length && $('#again_pas').val().length) && ($('#pas').val() !== $('#again_pas').val())) {
                $('.again_pas_eror').html('<i class="fa fa-warning"> </i> Passowrd does not match.');
                $('.again_pas_eror').show();
                all_ok = false;
            }
            if (!all_ok)
                return false;
        });
    }

    function Loginjs() {
        $('body').delegate('#login_form', 'submit', function () {
            var all_ok = true;
            if (!check_error('email'))
                all_ok = false;
            if (!check_error('pas'))
                all_ok = false;

            if (!validateEmail('email')) {
                $('.email_eror').html('<i class="fa fa-warning"> </i> Please enter valid email address.').show();
                return false;
            }

            if (!all_ok)
                return false;
        });
    }

    function Profilejs() {
        $('body').delegate('.change-profile-pic', 'click', function () {
            $('.file').click();
        });

        $('.file').change(function () {
            $('#update_pic_form').submit();
        });

        $('body').delegate('#profile_form', 'submit', function () {
            var all_ok = true;
            if (!check_error('name'))
                all_ok = false;
            if (!check_error('email'))
                all_ok = false;
            if (!check_error('schol'))
                all_ok = false;

            if ($('#email').val().length && !validateEmail('email')) {
                $('.email_eror').html('<i class="fa fa-warning"> </i> Please enter valid email address.').show();
                return false;
            }
            if (!all_ok) {
                return false;
            }
        });
    }

    function Registerjs() {
        $("#email").focusout(function () {
            var email = $('#email').val();
            if (email.length > 6) {
                if (validateEmail('email')) {
                    var result = send_ajax({register: 1, ajax: 1, email: email}, '?r=AuthAction', false);
                    if (result.error) {
                        $('.email_eror').html('<i class="fa fa-warning"> </i> Sorry! email already exist.').show();
                        $('#ok').val('no');
                    } else {
                        $('.email_eror').hide();
                        $('#ok').val('yes');
                    }
                }

            }
        });

        $('#file').change(function () {
            if ($('#file').val().length)
                $('.selected').removeClass('glyphicon glyphicon-file').addClass('glyphicon glyphicon-remove').css({color: '#e63144', cursor: 'pointer', 'pointer-events': 'auto'}).attr('title', 'Remove Selected Files');
        });

        var file = $("#file");
        $(".selected").on("click", function () {
            file.replaceWith(file = file.clone(true));
            $('.selected').removeClass('glyphicon glyphicon-remove').addClass('glyphicon glyphicon-file').css({color: 'rgb(119, 119, 119)'});
        });


        $('body').delegate('#register_form', 'submit', function () {
            var all_ok = true;
            var is_ok = $('#ok').val();
            if (!check_error('name'))
                all_ok = false;
            if (!check_error('email'))
                all_ok = false;
            if (!check_error('pas'))
                all_ok = false;
            if (!check_error('again_pas'))
                all_ok = false;
            if (!check_error('schol'))
                all_ok = false;
            if ($('#email').val().length > 1 && !validateEmail('email')) {
                $('.email_eror').html('<i class="fa fa-warning"> </i> Please enter valid email address.').show();
                return false;
            }
            if (($('#pas').val().length && $('#again_pas').val().length) && ($('#pas').val() !== $('#again_pas').val())) {
                $('.again_pas_eror').html('<i class="fa fa-warning"> </i> Passowrd does not match.');
                $('.again_pas_eror').show();
                all_ok = false;
            }
            if (is_ok === 'no') {
                $('.email_eror').html('<i class="fa fa-warning"> </i> Sorry! email already exist.').show();
                all_ok = false;
            }

            if (!all_ok)
                return false;
        });
    }

});

