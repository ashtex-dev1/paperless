$(document).ready(function () {
    var result = send_ajax({ajax: 1, get_count: 1}, '?r=Dashboard', false);
    if (result.events === '0' && result.forms === '0' && result.users === '0') {
        $('#bx_title').html('Sorry! No Record Found');
        $('.fa-minus').trigger('click');
    } else {
        var myChart = new Chart($('#salesChart'), {
            type: 'bar',
            data: {
                labels: ["Events", "Forms", "Users"],
                datasets: [{
                        label: 'Stats',
                        data: [result.events, result.forms, result.users],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)'
                        ],
                        borderWidth: 1
                    }]
            },
            options: {
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                }
            }
        });
    }

});