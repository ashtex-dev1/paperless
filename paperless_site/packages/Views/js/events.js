$( document ).ready( function () {
    var url = window.location.href;
    $( ".datatables" ).DataTable();
    if ( url.indexOf( 'NewEvent=MQ' ) !== -1 ) {
	NewEventjs();
    } else if ( url.indexOf( 'EditEvent=MQ' ) !== -1 ) {
	EditEventjs();
    } else if ( url.indexOf( '?r=Events' ) !== -1 ) {
	Eventsjs();
    }

    function Eventsjs() {
        
	$( 'body' ).delegate( '#send_event', 'click', function () {
	    $( '#hidden_id' ).val( $( this ).parent().parent().find( '#event_id' ).val() );
	    $( '#send_form_id' ).val( $( this ).parent().parent().find( '#set_form_id' ).val() );
	    $( '#usersmodal' ).modal( 'show' );
	} );

	$( 'body' ).delegate( '#delete_event_btn', 'click', function () {
	    var tr = $( this ).parent().parent();
	    var event_id = $( this ).parent().parent().find( '#event_id' ).val();
	    if ( event_id ) {
		var result = send_ajax( { ajax: 1, delete_event: event_id }, '?r=EventsAction', false );
		if ( result ) {
		    $( tr ).fadeOut( 1000 );
		}
	    }
	} );
	GetUsers();
	ValidateUsers( false );
    }

    function ValidateUsers( is_new ) {
	$( 'body' ).delegate( '#validate_users', 'click', function () {

	    setTimeout( function () {
		var is_ok = false;
		var emails = '';
		if ( $( '.ms-sel-ctn .ms-sel-item ' ).length ) {
		    $( '.ms-sel-ctn .ms-sel-item ' ).each( function () {
			if ( $( this ).html() && isEmail( $( this ).html().replace( /<\/?span[^>]*>/g, "" ) ) ) {
			    if ( is_new )
				emails += $( this ).html().replace( /<\/?span[^>]*>/g, "" ) + ',';
			    $( '.allusers_eror' ).hide();
			    is_ok = true;
			} else {
			    $( '.allusers_eror' ).show();
			}
		    } );
		} else {
		    $( '.allusers_eror' ).show();
		}
		if ( is_ok && !is_new )
		    $( '#all_users' ).submit();
		else if ( is_ok && is_new ) {
		    $( '#new_users' ).val( emails );
		    $( '#new_event_form' ).submit();
		}
	    }, 300 );
	} );
    }

    function GetUsers() {
	$( '#users' ).magicSuggest( {
	    data: '?r=GetUsers',
	    valueField: 'name',
	    displayField: 'email'
	} );
    }

    function isEmail( email ) {
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return regex.test( email );
    }

    function NewEventjs() {
	$( ".timepicker" ).timepicker( {
	    showInputs: false,
	    autoclose: true,
	} );
	$( ".timepicker" ).focusout( function () {
//            $(this).timepicker('hideWidget');
	} );
	$( '.date' ).datepicker( {
	    autoclose: true
	} );
	CheckUncheck( false );
	GetUsers();
	$( 'body' ).delegate( '#getforms', 'click', function () {
	    $( '#all_forms' ).modal( 'show' );
	} );
	$( 'body' ).delegate( '#submit_new_event', 'click', function () {
	    ValidateNewForm( false );
	} );
	$( 'body' ).delegate( '#send_new_event', 'click', function () {
	    var validated = ValidateNewForm( true );
	    if ( $( '#hidden_id' ).val() !== '' ) {
		if ( validated )
		    $( '#usersmodal' ).modal( 'show' );
	    } else if ( $( '#hidden_id' ).val() == '' ) {
		var action = "?r=EventsAction";
		var data = $( '#new_event_form' ).serializeObject();
		$.ajax( {
		    url: action + "&action=1",
		    type: 'POST',
		    data: data,
		    crossDomain: true,
		    cache: false,
		    dataType: "json",
		    success: function ( data ) {
			console.log( data );
			$( '#hidden_id' ).val( data.data );
			setTimeout( function ( ) {
			    var validated = ValidateNewForm( true );
			    if ( validated )
				$( '#usersmodal' ).modal( 'show' );
			}, 3000 );
//			callback( data );
		    }
		} );
	    }
	}
	);
	ValidateUsers( true );
    }

    function ValidateNewForm( send ) {
	var all_ok = true;
	if ( !check_error( 'event_name' ) )
	    all_ok = false;
	if ( !check_error( 'event_type' ) )
	    all_ok = false;
	if ( !check_error( 'event_venue' ) )
	    all_ok = false;
	if ( !check_error( 'event_start_date' ) )
	    all_ok = false;
	if ( !check_error( 'event_end_date' ) )
	    all_ok = false;
	if ( !check_error( 'start_time' ) )
	    all_ok = false;
	if ( !check_error( 'end_time' ) )
	    all_ok = false;
	if ( !check_error( 'event_des' ) )
	    all_ok = false;
	if ( $( '#form_ids' ).val() === '0' ) {
	    $( '.form_ids_eror' ).show();
	    all_ok = false;
	} else {
	    $( '.form_ids_eror' ).hide();
	}
	ValidateTime( false );
	var result = ValidateDate();
	if ( !result )
	    all_ok = false;
	if ( all_ok )
	    if ( !send ) {
		if ( $( '#submit_new_event' ).attr( "name" ) === "update_event" ) {
		    $( '#new_users' ).attr( 'name', 'update_event' ).val( '1' );
		} else {
		    $( '#new_users' ).attr( 'name', 'new_event' ).val( '1' );
		}
		$( '#new_event_form' ).submit();
	    } else if ( send )
		return true;
    }

    function ValidateDate() {
	var start = $( '#event_start_date' ).val();
	var end = $( '#event_end_date' ).val();
	if ( end.length && start.length ) {
	    if ( end > start ) {
		HideDateEror();
		return ValidateTime( true );
	    } else if ( end === start ) {
		HideDateEror();
		return ValidateTime( false );
	    } else {
		$( '.event_start_date_eror' ).css( 'display', 'block' ).html( '<i class="fa fa-warning"> </i> Start date must less than End date.' );
		$( '.event_end_date_eror' ).css( 'display', 'block' ).html( '<i class="fa fa-warning"> </i> End date must greater than Start date.' );
		return false;
	    }
	}
    }

    function ValidateTime( date_not_equal ) {
	var start = $( '#start_time' ).val();
	var end = $( '#end_time' ).val();
	var startdate = $( '#event_start_date' ).val();
	var enddate = $( '#event_end_date' ).val();
	if ( end.length && start.length ) {
	    if ( date_not_equal ) {
		HideTimeEror();
		var hours = TimeDiff( start, end, startdate, enddate );
	    } else if ( !date_not_equal ) {
		HideTimeEror();
		hours = TimeDiff( start, end, startdate, startdate );
	    }
	    if ( hours > 0 ) {
		$( '.start_time_eror' ).css( 'display', 'none' );
		return true;
	    } else {
		$( '.start_time_eror' ).css( 'display', 'block' ).html( '<i class="fa fa-warning"> </i> Please enter a valid start time and end time.' );
		return false;
	    }
	}
    }


    function TimeDiff( starttime, endtime, startdate, enddate ) {
	var timeStart = new Date( '"' + startdate + '"' + '"' + starttime + '"' ),
		timeEnd = new Date( '"' + enddate + '"' + '"' + endtime + '"' ),
		diff = ( timeEnd - timeStart ) / 60000,
		minutes = diff % 60,
		hours = ( diff - minutes ) / 60;
	return hours;
    }

    function HideDateEror() {
	$( '.event_start_date_eror' ).css( 'display', 'none' );
	$( '.event_start_date_eror' ).css( 'display', 'none' );
    }

    function HideTimeEror() {
	$( '.start_time_eror' ).css( 'display', 'none' );
	$( '.end_time_eror' ).css( 'display', 'none' );
    }

    $( 'body' ).delegate( '#event_image', 'change', function () {
	var img = $( this ).val();
	var extension = ( img.split( '.' ) ).slice( -1 )[0];
	if ( extension !== 'jpg' && extension !== 'png' && extension !== 'jpeg' ) {
	    $( this ).parent().find( '.eventimgeror' ).css( 'display', 'block' );
	    $( '#submit_new_event' ).prop( 'disabled', true );
	} else {
	    $( this ).parent().find( '.eventimgeror' ).css( 'display', 'none' );
	    $( '#submit_new_event' ).prop( 'disabled', false );
	}
    } );

    function EditEventjs() {
	NewEventjs();
	var form_ids = [ ];
	$( '#event_type' ).val( $( '#hidden_type' ).val() );
	$( '#event_venue' ).val( $( '#hidden_venue' ).val() );
	$( '.check-form' ).each( function () {
	    if ( $( this ).is( ":checked" ) ) {
		form_ids.push( $( this ).parent().find( '#form_id' ).val() );
	    }
	} );
	CheckUncheck( form_ids );
    }

    function CheckUncheck( formids ) {
	var form_ids = [ ];
	if ( formids.length )
	    form_ids = formids;

	$( 'body' ).delegate( '.check-form', 'change', function () {
	    var formid = $( this ).parent().find( '#form_id' ).val();
	    if ( $( this ).prop( 'checked' ) ) {
		form_ids.push( formid );
	    } else {
		form_ids.splice( $.inArray( formid, form_ids ), 1 );
	    }
	    $( '.badge' ).html( form_ids.length );
	    $( '#form_ids' ).val( form_ids );
	} );
    }
} );