-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 27, 2017 at 08:21 AM
-- Server version: 5.5.50-0ubuntu0.14.04.1-log
-- PHP Version: 5.5.9-1ubuntu4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `paperless`
--

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `form_id` varchar(11) NOT NULL,
  `name` text NOT NULL,
  `type` text NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `image` varchar(300) NOT NULL,
  `venue` text NOT NULL,
  `description` text NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `published` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=103 ;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`event_id`, `user_id`, `form_id`, `name`, `type`, `start_date`, `end_date`, `start_time`, `end_time`, `image`, `venue`, `description`, `created_at`, `modified_at`, `published`, `deleted`) VALUES
(94, 47, '55', 'test', 'comedy', '2017-03-01', '2017-03-30', '12:45:00', '12:45:00', '', 'tacoma_dome', 'test', '2017-03-13 03:58:40', '2017-03-13 04:02:10', 1, 0),
(95, 47, '55,56', 'test2', 'film', '2017-03-02', '2017-03-29', '12:45:00', '12:45:00', '', 'bbT_center', 'test2', '2017-03-13 03:59:59', '2017-03-24 09:53:37', 1, 0),
(96, 47, '56', 'test3', 'film', '2017-03-01', '2017-03-21', '01:00:00', '01:00:00', '', 'united_center', 'test3', '2017-03-13 04:02:40', '0000-00-00 00:00:00', 1, 0),
(97, 47, '56,55', 'Museum exhibition', 'exhibition', '2017-03-13', '2017-03-14', '02:45:00', '02:45:00', '', 'united_center', 'Going to Museum exhibition that is to be held in xyz\r\nApprove if you want to allow the children to go to trip.', '2017-03-13 05:48:42', '0000-00-00 00:00:00', 1, 0),
(98, 47, '57', 'all', 'learning', '2017-03-08', '2017-03-22', '04:00:00', '04:00:00', '', 'the_palace_of_auburn_hills', 'all', '2017-03-15 07:01:58', '0000-00-00 00:00:00', 1, 0),
(99, 47, '58,55', 'test3', 'learning', '2017-03-17', '2017-03-17', '01:15:00', '01:15:00', '', 'tacoma_dome', 'This event is created for testing purpose.\r\nEvent Creation', '2017-03-17 04:27:56', '0000-00-00 00:00:00', 1, 0),
(100, 47, '59,60', 'tesing', 'film', '2017-03-17', '2017-03-20', '01:45:00', '07:45:00', '', 'scottrade_center', 'hgshgdhsjfkebuicw kjc hfucid', '2017-03-17 04:51:54', '0000-00-00 00:00:00', 1, 0),
(101, 47, '57,58', '123', 'film', '2017-03-01', '2017-03-31', '08:15:00', '08:15:00', '', 'united_center', '123', '2017-03-24 11:18:12', '2017-03-27 07:21:14', 1, 0),
(102, 47, '56', 'New testing event', 'club', '2017-03-01', '2017-03-03', '11:45:00', '11:45:00', '', 'united_center', 'event description', '2017-03-27 02:55:01', '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `event_form_data`
--

CREATE TABLE IF NOT EXISTS `event_form_data` (
  `event_form_data_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `type` text NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `image` varchar(500) NOT NULL,
  `venue` text NOT NULL,
  `description` text NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `published` int(50) NOT NULL,
  `deleted` int(50) NOT NULL,
  PRIMARY KEY (`event_form_data_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=82 ;

--
-- Dumping data for table `event_form_data`
--

INSERT INTO `event_form_data` (`event_form_data_id`, `user_id`, `event_id`, `form_id`, `name`, `type`, `start_date`, `end_date`, `start_time`, `end_time`, `image`, `venue`, `description`, `created_at`, `modified_at`, `published`, `deleted`) VALUES
(68, 47, 94, 55, 'test', 'comedy', '2017-03-01', '2017-03-30', '12:45:00', '12:45:00', '', 'tacoma_dome', 'test', '2017-03-13 03:58:40', '2017-03-13 04:02:10', 1, 0),
(69, 47, 95, 55, 'test2', 'film', '2017-03-02', '2017-03-29', '12:45:00', '12:45:00', '', 'bbT_center', 'test2', '2017-03-13 03:59:59', '2017-03-24 09:53:37', 1, 0),
(70, 47, 95, 56, 'test2', 'film', '2017-03-02', '2017-03-29', '12:45:00', '12:45:00', '', 'bbT_center', 'test2', '2017-03-13 03:59:59', '2017-03-24 09:53:37', 1, 0),
(71, 47, 96, 56, 'test3', 'film', '2017-03-01', '2017-03-21', '01:00:00', '01:00:00', '', 'united_center', 'test3', '2017-03-13 04:02:40', '0000-00-00 00:00:00', 1, 0),
(72, 47, 97, 56, 'Museum exhibition', 'exhibition', '2017-03-13', '2017-03-14', '02:45:00', '02:45:00', '', 'united_center', 'Going to Museum exhibition that is to be held in xyz\r\nApprove if you want to allow the children to go to trip.', '2017-03-13 05:48:42', '0000-00-00 00:00:00', 1, 0),
(73, 47, 97, 55, 'Museum exhibition', 'exhibition', '2017-03-13', '2017-03-14', '02:45:00', '02:45:00', '', 'united_center', 'Going to Museum exhibition that is to be held in xyz\r\nApprove if you want to allow the children to go to trip.', '2017-03-13 05:48:42', '0000-00-00 00:00:00', 1, 0),
(74, 47, 98, 57, 'all', 'learning', '2017-03-08', '2017-03-22', '04:00:00', '04:00:00', '', 'the_palace_of_auburn_hills', 'all', '2017-03-15 07:01:58', '0000-00-00 00:00:00', 1, 0),
(75, 47, 99, 58, 'test3', 'learning', '2017-03-17', '2017-03-17', '01:15:00', '01:15:00', '', 'tacoma_dome', 'This event is created for testing purpose.\r\nEvent Creation', '2017-03-17 04:27:56', '0000-00-00 00:00:00', 1, 0),
(76, 47, 99, 55, 'test3', 'learning', '2017-03-17', '2017-03-17', '01:15:00', '01:15:00', '', 'tacoma_dome', 'This event is created for testing purpose.\r\nEvent Creation', '2017-03-17 04:27:56', '0000-00-00 00:00:00', 1, 0),
(77, 47, 100, 59, 'tesing', 'film', '2017-03-17', '2017-03-20', '01:45:00', '07:45:00', '', 'scottrade_center', 'hgshgdhsjfkebuicw kjc hfucid', '2017-03-17 04:51:54', '0000-00-00 00:00:00', 1, 0),
(78, 47, 100, 60, 'tesing', 'film', '2017-03-17', '2017-03-20', '01:45:00', '07:45:00', '', 'scottrade_center', 'hgshgdhsjfkebuicw kjc hfucid', '2017-03-17 04:51:54', '0000-00-00 00:00:00', 1, 0),
(79, 47, 101, 57, '123', 'film', '2017-03-01', '2017-03-31', '08:15:00', '08:15:00', '', 'united_center', '123', '2017-03-24 11:18:12', '2017-03-27 07:21:14', 1, 0),
(80, 47, 101, 58, '123', 'film', '2017-03-01', '2017-03-31', '08:15:00', '08:15:00', '', 'united_center', '123', '2017-03-24 11:18:12', '2017-03-27 07:21:14', 1, 0),
(81, 47, 102, 56, 'New testing event', 'club', '2017-03-01', '2017-03-03', '11:45:00', '11:45:00', '', 'united_center', 'event description', '2017-03-27 02:55:01', '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `form`
--

CREATE TABLE IF NOT EXISTS `form` (
  `form_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `created_at` date NOT NULL,
  `form_fields` text NOT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`form_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `form`
--

INSERT INTO `form` (`form_id`, `user_id`, `name`, `created_at`, `form_fields`, `deleted`) VALUES
(55, 47, 'form', '2017-03-13', 'a:5:{i:0;O:8:"stdClass":4:{s:4:"type";s:12:"autocomplete";s:5:"label";s:12:"Autocomplete";s:9:"className";s:12:"autocomplete";s:4:"name";s:12:"autocomplete";}i:1;O:8:"stdClass":4:{s:4:"type";s:12:"autocomplete";s:5:"label";s:12:"Autocomplete";s:9:"className";s:12:"autocomplete";s:4:"name";s:12:"autocomplete";}i:2;O:8:"stdClass":4:{s:4:"type";s:12:"autocomplete";s:5:"label";s:12:"Autocomplete";s:9:"className";s:12:"autocomplete";s:4:"name";s:12:"autocomplete";}i:3;O:8:"stdClass":4:{s:4:"type";s:12:"autocomplete";s:5:"label";s:12:"Autocomplete";s:9:"className";s:12:"autocomplete";s:4:"name";s:12:"autocomplete";}i:4;O:8:"stdClass":4:{s:4:"type";s:12:"autocomplete";s:5:"label";s:12:"Autocomplete";s:9:"className";s:12:"autocomplete";s:4:"name";s:12:"autocomplete";}}', 0),
(56, 47, 'form2', '2017-03-13', 'a:3:{i:0;O:8:"stdClass":4:{s:4:"type";s:12:"autocomplete";s:5:"label";s:12:"Autocomplete";s:9:"className";s:12:"autocomplete";s:4:"name";s:12:"autocomplete";}i:1;O:8:"stdClass":4:{s:4:"type";s:4:"date";s:5:"label";s:10:"Date Field";s:9:"className";s:8:"calendar";s:4:"name";s:4:"date";}i:2;O:8:"stdClass":4:{s:4:"type";s:6:"number";s:5:"label";s:6:"Number";s:9:"className";s:12:"form-control";s:4:"name";s:6:"number";}}', 0),
(57, 47, 'all', '2017-03-15', 'a:13:{i:0;O:8:"stdClass":4:{s:4:"type";s:12:"autocomplete";s:5:"label";s:12:"Autocomplete";s:9:"className";s:12:"autocomplete";s:4:"name";s:26:"autocomplete-1489575670176";}i:1;O:8:"stdClass":4:{s:4:"type";s:8:"checkbox";s:5:"label";s:8:"Checkbox";s:9:"className";s:8:"checkbox";s:4:"name";s:22:"checkbox-1489575671044";}i:2;O:8:"stdClass":5:{s:4:"type";s:14:"checkbox-group";s:5:"label";s:14:"Checkbox Group";s:9:"className";s:14:"checkbox-group";s:4:"name";s:28:"checkbox-group-1489575671511";s:6:"values";a:3:{i:0;O:8:"stdClass":3:{s:5:"label";s:8:"Option 1";s:5:"value";s:8:"option-1";s:8:"selected";b:1;}i:1;O:8:"stdClass":2:{s:5:"label";s:8:"Option 2";s:5:"value";s:8:"option-2";}i:2;O:8:"stdClass":2:{s:5:"label";s:8:"Option 3";s:5:"value";s:8:"option-3";}}}i:3;O:8:"stdClass":4:{s:4:"type";s:4:"date";s:5:"label";s:10:"Date Field";s:9:"className";s:8:"calendar";s:4:"name";s:18:"date-1489575672165";}i:4;O:8:"stdClass":4:{s:4:"type";s:4:"file";s:5:"label";s:11:"File Upload";s:9:"className";s:12:"form-control";s:4:"name";s:18:"file-1489575672648";}i:5;O:8:"stdClass":4:{s:4:"type";s:6:"header";s:7:"subtype";s:2:"h1";s:5:"label";s:6:"Header";s:9:"className";s:6:"header";}i:6;O:8:"stdClass":4:{s:4:"type";s:6:"hidden";s:5:"label";s:12:"Hidden Input";s:9:"className";s:12:"hidden-input";s:4:"name";s:20:"hidden-1489575673726";}i:7;O:8:"stdClass":4:{s:4:"type";s:9:"paragraph";s:7:"subtype";s:1:"p";s:5:"label";s:9:"Paragraph";s:9:"className";s:9:"paragraph";}i:8;O:8:"stdClass":5:{s:4:"type";s:11:"radio-group";s:5:"label";s:11:"Radio Group";s:9:"className";s:11:"radio-group";s:4:"name";s:25:"radio-group-1489575674647";s:6:"values";a:3:{i:0;O:8:"stdClass":3:{s:5:"label";s:8:"Option 1";s:5:"value";s:8:"option-1";s:8:"selected";b:1;}i:1;O:8:"stdClass":2:{s:5:"label";s:8:"Option 2";s:5:"value";s:8:"option-2";}i:2;O:8:"stdClass":2:{s:5:"label";s:8:"Option 3";s:5:"value";s:8:"option-3";}}}i:9;O:8:"stdClass":4:{s:4:"type";s:6:"number";s:5:"label";s:6:"Number";s:9:"className";s:12:"form-control";s:4:"name";s:20:"number-1489575675342";}i:10;O:8:"stdClass":5:{s:4:"type";s:6:"select";s:5:"label";s:6:"Select";s:9:"className";s:12:"form-control";s:4:"name";s:20:"select-1489575675954";s:6:"values";a:3:{i:0;O:8:"stdClass":3:{s:5:"label";s:8:"Option 1";s:5:"value";s:8:"option-1";s:8:"selected";b:1;}i:1;O:8:"stdClass":2:{s:5:"label";s:8:"Option 2";s:5:"value";s:8:"option-2";}i:2;O:8:"stdClass":2:{s:5:"label";s:8:"Option 3";s:5:"value";s:8:"option-3";}}}i:11;O:8:"stdClass":5:{s:4:"type";s:4:"text";s:5:"label";s:10:"Text Field";s:7:"subtype";s:4:"text";s:9:"className";s:12:"form-control";s:4:"name";s:18:"text-1489575676599";}i:12;O:8:"stdClass":4:{s:4:"type";s:8:"textarea";s:5:"label";s:9:"Text Area";s:9:"className";s:12:"form-control";s:4:"name";s:22:"textarea-1489575677145";}}', 0),
(58, 47, 'form3', '2017-03-17', 'a:4:{i:0;O:8:"stdClass":4:{s:4:"type";s:12:"autocomplete";s:5:"label";s:12:"Autocomplete";s:9:"className";s:12:"autocomplete";s:4:"name";s:26:"autocomplete-1489739123261";}i:1;O:8:"stdClass":4:{s:4:"type";s:12:"autocomplete";s:5:"label";s:12:"Autocomplete";s:9:"className";s:12:"autocomplete";s:4:"name";s:26:"autocomplete-1489739127214";}i:2;O:8:"stdClass":4:{s:4:"type";s:12:"autocomplete";s:5:"label";s:12:"Autocomplete";s:9:"className";s:12:"autocomplete";s:4:"name";s:26:"autocomplete-1489739125252";}i:3;O:8:"stdClass":4:{s:4:"type";s:12:"autocomplete";s:5:"label";s:12:"Autocomplete";s:9:"className";s:12:"autocomplete";s:4:"name";s:26:"autocomplete-1489739129175";}}', 0),
(59, 47, 'Form 01', '2017-03-17', 'a:4:{i:0;O:8:"stdClass":6:{s:4:"type";s:4:"text";s:8:"required";b:1;s:5:"label";s:13:"Guardian Name";s:7:"subtype";s:4:"text";s:9:"className";s:12:"form-control";s:4:"name";s:18:"text-1489740088703";}i:1;O:8:"stdClass":6:{s:4:"type";s:4:"text";s:8:"required";b:1;s:5:"label";s:13:"Student Name:";s:7:"subtype";s:4:"text";s:9:"className";s:12:"form-control";s:4:"name";s:18:"text-1489740119750";}i:2;O:8:"stdClass":5:{s:4:"type";s:6:"number";s:8:"required";b:1;s:5:"label";s:10:"Student Id";s:9:"className";s:12:"form-control";s:4:"name";s:20:"number-1489740159004";}i:3;O:8:"stdClass":5:{s:4:"type";s:11:"radio-group";s:5:"label";s:63:"Are you willing  to send your child to the trip? Please select:";s:9:"className";s:11:"radio-group";s:4:"name";s:25:"radio-group-1489740204325";s:6:"values";a:2:{i:0;O:8:"stdClass":3:{s:5:"label";s:3:"Yes";s:5:"value";s:8:"option-1";s:8:"selected";b:1;}i:1;O:8:"stdClass":2:{s:5:"label";s:2:"No";s:5:"value";s:8:"option-2";}}}}', 0),
(60, 47, 'Form 02', '2017-03-17', 'a:4:{i:0;O:8:"stdClass":5:{s:4:"type";s:12:"autocomplete";s:8:"required";b:1;s:5:"label";s:13:"Guardian Name";s:9:"className";s:12:"autocomplete";s:4:"name";s:26:"autocomplete-1489740539822";}i:1;O:8:"stdClass":6:{s:4:"type";s:4:"text";s:8:"required";b:1;s:5:"label";s:13:"Student name:";s:7:"subtype";s:4:"text";s:9:"className";s:12:"form-control";s:4:"name";s:18:"text-1489740564979";}i:2;O:8:"stdClass":5:{s:4:"type";s:6:"number";s:8:"required";b:1;s:5:"label";s:10:"Student Id";s:9:"className";s:12:"form-control";s:4:"name";s:20:"number-1489740582665";}i:3;O:8:"stdClass":6:{s:4:"type";s:11:"radio-group";s:8:"required";b:1;s:5:"label";s:63:"Are you willing to send your child to the event? Please select ";s:9:"className";s:11:"radio-group";s:4:"name";s:25:"radio-group-1489740598972";s:6:"values";a:2:{i:0;O:8:"stdClass":3:{s:5:"label";s:3:"Yes";s:5:"value";s:8:"option-1";s:8:"selected";b:1;}i:1;O:8:"stdClass":2:{s:5:"label";s:2:"No";s:5:"value";s:8:"option-2";}}}}', 0),
(61, 47, 'Login Form', '2017-03-27', 'a:3:{i:0;O:8:"stdClass":5:{s:4:"type";s:4:"text";s:5:"label";s:10:"Text Field";s:7:"subtype";s:4:"text";s:9:"className";s:12:"form-control";s:4:"name";s:18:"text-1490596381912";}i:1;O:8:"stdClass":5:{s:4:"type";s:6:"select";s:5:"label";s:6:"Select";s:9:"className";s:12:"form-control";s:4:"name";s:20:"select-1490596384295";s:6:"values";a:3:{i:0;O:8:"stdClass":3:{s:5:"label";s:8:"Option 1";s:5:"value";s:8:"option-1";s:8:"selected";b:1;}i:1;O:8:"stdClass":2:{s:5:"label";s:8:"Option 2";s:5:"value";s:8:"option-2";}i:2;O:8:"stdClass":2:{s:5:"label";s:8:"Option 3";s:5:"value";s:8:"option-3";}}}i:2;O:8:"stdClass":6:{s:4:"type";s:6:"button";s:5:"label";s:6:"Button";s:7:"subtype";s:6:"button";s:9:"className";s:28:"button-input btn-default btn";s:4:"name";s:20:"button-1490596386566";s:5:"style";s:7:"default";}}', 1),
(62, 47, 'test', '2017-03-27', 'a:5:{i:0;O:8:"stdClass":4:{s:4:"type";s:12:"autocomplete";s:5:"label";s:12:"Autocomplete";s:9:"className";s:12:"autocomplete";s:4:"name";s:26:"autocomplete-1490596657262";}i:1;O:8:"stdClass":4:{s:4:"type";s:4:"date";s:5:"label";s:10:"Date Field";s:9:"className";s:8:"calendar";s:4:"name";s:18:"date-1490596657912";}i:2;O:8:"stdClass":4:{s:4:"type";s:6:"header";s:7:"subtype";s:2:"h1";s:5:"label";s:6:"Header";s:9:"className";s:6:"header";}i:3;O:8:"stdClass":4:{s:4:"type";s:6:"number";s:5:"label";s:6:"Number";s:9:"className";s:12:"form-control";s:4:"name";s:20:"number-1490596659597";}i:4;O:8:"stdClass":5:{s:4:"type";s:4:"text";s:5:"label";s:10:"Text Field";s:7:"subtype";s:4:"text";s:9:"className";s:12:"form-control";s:4:"name";s:18:"text-1490596660543";}}', 0);

-- --------------------------------------------------------

--
-- Table structure for table `form_data`
--

CREATE TABLE IF NOT EXISTS `form_data` (
  `form_data_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` text NOT NULL,
  `data_key` text NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`form_data_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `form_data`
--

INSERT INTO `form_data` (`form_data_id`, `user_id`, `data_key`, `value`) VALUES
(1, '80', 'autocomplete', ''),
(2, '80', 'autocomplete', ''),
(3, '80', 'autocomplete', ''),
(4, '80', 'autocomplete', ''),
(5, '80', 'autocomplete', ''),
(6, '80', 'autocomplete', '');

-- --------------------------------------------------------

--
-- Table structure for table `invitaion`
--

CREATE TABLE IF NOT EXISTS `invitaion` (
  `invitaion_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(11) NOT NULL,
  `form_id` varchar(11) NOT NULL,
  `event_id` varchar(11) NOT NULL,
  `status` varchar(11) NOT NULL,
  `response` varchar(5000) NOT NULL,
  PRIMARY KEY (`invitaion_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=123 ;

--
-- Dumping data for table `invitaion`
--

INSERT INTO `invitaion` (`invitaion_id`, `user_id`, `form_id`, `event_id`, `status`, `response`) VALUES
(110, '80', '55', '94', '1', ''),
(111, '80', '56', '96', '2', ''),
(112, '80', '56', '97', '1', ''),
(113, '80', '55', '97', '1', ''),
(114, '80', '57', '98', '1', ''),
(115, '80', '58', '99', '1', ''),
(116, '80', '55', '99', '1', ''),
(117, '80', '59', '100', '1', ''),
(118, '80', '60', '100', '1', ''),
(119, '80', '55', '95', '1', ''),
(120, '80', '56', '95', '1', ''),
(121, '80', '57', '101', '0', ''),
(122, '80', '58', '101', '0', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` text NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` text NOT NULL,
  `school` text NOT NULL,
  `profile_pic` varchar(500) NOT NULL,
  `is_admin` int(11) NOT NULL,
  `status` text NOT NULL,
  `temp_link` text NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=81 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `email`, `password`, `school`, `profile_pic`, `is_admin`, `status`, `temp_link`, `created_by`) VALUES
(1, 'sdfsusmanfasdfasdf', 'usmanmq0@gmail.com', '$2a$12$y2CRrf8Psw2/MGs4iqgoPOmiKUqOHdti6RgLVBouhtkBCpIfviKcG', 'schoolsdfsdf', '9e03b045c56e33a0050ef01d515ecf4e6667b3f3.png', 1, 'active', '', 0),
(2, 'fatima', 'fatimayousaf115@gmail.com', '$2a$12$PIukIR5jpBdbMz4FfvXND.ito/hp/HBD7CvfrjM82ABh/9Cm3M5wS', 'DPS School', '0dc75e8e2aa9cc78824eabf19c63066fc6042245.jpg', 1, 'active', '1b04605732c8ed9d94e047d7ff19e5d4', 0),
(9, 'fatima', 'fatimayousaf113@gmail.com', '$2a$12$tD1kNFLL0SJWEJG8RRHOgOO0CEfcfWt6h4jzL1NewudGUf0eSuL26', 'DPS', 'default.png', 1, 'active', '', 0),
(10, 'Leon Frazier', 'leon@wrayzier.com', '$2a$12$Qg1WWo9W8nVG4/n1E1J...tMj4fJUDvhDaink1.EFimpVktJsBtHW', '457', 'default.png', 1, 'active', '', 0),
(11, 'Eric Haselhorst', 'rockinhfarmtoys@gmail.com', '$2a$12$cnTuoMwGXTIiem6Tkntpk.M1aZzutLOH/30Q4v0v2HDMGOsm4fokW', 'shs', 'default.png', 1, 'active', '', 0),
(12, 'TEst', 'ashtex4ga@gmail.com', '$2a$12$c/GwShF1PGfCLOtWV6pxgeziM8ekDT3O215GEdj/MdicoJEmFQjHK', 'Test School', 'default.png', 1, 'active', '', 0),
(13, 'fatima', 'fatimayousaf116@yahoo.com', '$2a$12$hhxVPtxdcUdmCxuwgR3rJO2x2pxXSyuY1TFqSwsOD9bGaMsm5UnWO', 'DPS', 'default.png', 1, 'active', '', 0),
(14, 'fatima', 'fatimayousaf118@gmail.com', '$2a$12$JtintYYEp0IqlHYGuFdT1.YAs1C8UragWogu2MtA5GChy.TU0kToq', 'dps', 'default.png', 1, 'active', '', 0),
(15, 'Fatima', 'fatimayousaf115@gmail.com', '$2a$12$ZIiOwvOYhDu1j0kKRyzA8.ZCApKYABWmFgneVufVQl6TS.HFiU0jK', 'DPSS', '594356e880f5e85158133e26cea40c9b5129625e.png', 1, 'active', '1b04605732c8ed9d94e047d7ff19e5d4', 0),
(16, 'fatima', 'fatimayousaf118@yahoo.com', '$2a$12$G.txQN7qarrwYS7V2m1XheACrB6t3gj99u/8cSCvdmXD7J5akUQvy', 'dps', 'default.png', 1, 'active', '', 0),
(47, 'sahir toseef Ali', 'sahirtoseef@gmail.com', '$2a$12$z0oE5gL5F6xel1tb3MHroeopDyb35STZ3PUdQYGBvktzropOrDf6e', 'Ashtex', 'a3eb215feb9b747fe15147cc41c06c81b1b99a4d.png', 1, 'active', '', 0),
(80, 'random', 'usman4qcs@gmail.com', '$2a$12$z0oE5gL5F6xel1tb3MHroeopDyb35STZ3PUdQYGBvktzropOrDf6e', 'dummy', 'default.png', 0, 'active', '', 47);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
