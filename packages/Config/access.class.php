<?php
namespace  Config;

use Config\Central;
use Config\Constants;
use Plusql;

class Access implements \rocketsled\Runnable
{
    //constants - defines action associated with this class
    const LOGIN_ATTEMPT = 'login';
    const LOGIN_STATUS = 'status';
    const SIGNUP_ATTEMPT = 'signup';
    const LOGOUT_ATTEMPT = 'logout';
    //constants - for hashing
    const HASH_CYCLES = 1000;
    const SALT_RANDOM = 'e31f453ab964ec17e1e68faacbb64f05bccceb179858b4c482c1b182ff1e440e';
    //login messages
    const SUCCESS_LOGIN = 'You have logged in successfully';
    const PASS_MISMATCH = 'The password did not match';
    const USER_MISSING = 'The user name does not exists';
    const USER_EXISTS = 'The user name already exists';
    const USER_NOT_LOGGED_IN = 'You are not logged in';
    //----private varibales
    private $central;
    private $esc;
    private $profile = "access";
    private $action = null;
    private $callback = null;

    public function __construct()
    {
        @session_start();
        if(!isset($_SESSION[Constants::LOGGED_USER]))
        {
            $_SESSION[Constants::LOGGED_USER] = array('pass' => false);
        }
        $this->central = Central::instance();
        $this->central->set_alias_connection($this->profile);
    }

    public function logout_user()
    {
        unset($_SESSION[Constants::LOGGED_USER]);
        $_SESSION[Constants::LOGGED_STATUS] = self::USER_NOT_LOGGED_IN;
        if(!isset($_SESSION[Constants::LOGGED_USER]))
        {
            $_SESSION[Constants::LOGGED_USER] = null;
        }
        return true;
    }
    //function which validates the login attempt
    public function signin_user($name, $password)
    {
        $username = $this->esc($name);
        $password = $this->esc($password);
        try
        {
            $usr = PluSQL::from($this->profile)->users->select('*')->where("email = '$username'")->run()->users;
            $salt = substr($usr->hash, 0, 64);
            $hash = $salt.$password;
            // Hash the password as we did before
            for($i = 0; $i < self::HASH_CYCLES; $i++)
            {
                $hash = hash('sha256', $hash);
            }
            $hash = $salt.$hash;
            if($hash == $usr->hash)
            {
                $_SESSION[Constants::LOGGED_USER] = $usr;
                $_SESSION[Constants::LOGGED_STATUS] = self::SUCCESS_LOGIN;
                return true;
            }
            else
            {
                $_SESSION[Constants::LOGGED_USER] = null;
                $_SESSION[Constants::LOGGED_STATUS] = self::PASS_MISMATCH;
                return false;
            }
        }
        catch(\EmptySetException $e)
        {
            //the name was not found
            $_SESSION[Constants::LOGGED_USER] = null;
            $_SESSION[Constants::LOGGED_STATUS] = self::USER_MISSING;
            return false;
        }
    }
    //function which sign ups the new users
    public function signup_user($name, $password, $rpassword, $post)
    {
        $username = $this->esc($name);
        $password = $this->esc($password);
        $rpassword = $this->esc($rpassword);
        //check if the user is duplicate;
        if(strcmp($rpassword,$password)!=0)
        {
            $_SESSION[Constants::LOGGED_USER] = null;
            $_SESSION[Constants::LOGGED_STATUS] = self::PASS_MISMATCH;
            return false;
        }
        try
        {
            $usr_email = PluSQL::from($this->profile)->users->select('*')->where("email = '$username'")->run()->users->email;
            $_SESSION[Constants::LOGGED_USER] = null;
            $_SESSION[Constants::LOGGED_STATUS] = self::USER_EXISTS;
            return false;
        }
        catch(\EmptySetException $e)
        {
            //ok good, unique bastard
            $salt = hash('sha256', uniqid(mt_rand(), true).self::SALT_RANDOM.strtolower($username));
            $hash = $salt.$password;
            for($i = 0; $i < self::HASH_CYCLES; $i++)
            {
                $hash = hash('sha256', $hash);
            }
            $hash = $salt.$hash;
            $post['hash'] = $hash;
            PluSQL::into($this->profile)->users($post)->insert();
            return $this->signin_user($name, $password);
        }
    }
    //-----------------
    public static function login_status()
    {
        return $_SESSION[Constants::LOGGED_USER];
    }
    public static function login_message()
    {
        return $_SESSION[Constants::LOGGED_STATUS];
    }
    //-------------------
    public static function user_details($email)
    {
        try
        {
            $profile = 'accessstatic';
            $central = Central::instance();
            $central->set_alias_connection($profile);
            return PluSQL::from($profile)->users->select("*")->where("email = '$email'")->limit('0,1')->run()->users;
        }
        catch(\EmptySetException $e)
        {
            return null;
        }
    }
    //escape function
    public function __call($closure, $args)
    {
        $f = PluSQL::escape($this->profile);
        return $f($args[0]);
    }

    public function run()
    {
        try
        {
            //$this->update_main_contents();
        }
        catch(Exception $e)
        {
            echo $e->getMessage();
        }
    }

    //update main contents
    private function update_main_contents()
    {
        try
        {//used mainly to handle the NON-AJAX calls from the index_template.
            $this->action = $this->central->getargs('action', $_REQUEST, $this->corrupt);
            $this->callback = $this->central->getargs('callback', $_REQUEST, $this->corrupt);
            if(!$this->corrupt)
            {
                switch($this->action)
                {
                    case self::LOGOUT_ATTEMPT:
                        unset($_SESSION[Constants::LOGIN_FLAG]);
                        if(!isset($_SESSION[Constants::LOGIN_FLAG]))
                        {
                            $_SESSION[Constants::LOGIN_FLAG] = array('pass' => false);
                        }
                        header('Location: '.$this->callback);
                        break;
                    case self::LOGIN_ATTEMPT:
                        $this->corrupt = false;
                        $name = $this->central->getargs('inputEmail', $_REQUEST, $this->corrupt);
                        $password = $this->central->getargs('inputPassword', $_REQUEST, $this->corrupt);
                        if(!$this->corrupt)
                            $this->validate_login_attempt($name, $password, true);
                        else
                            throw new Exception("CSRF attack");
                        break;
                    case self::LOGIN_STATUS:
                        $this->login_status(true);
                        break;
                    case self::SIGNUP_ATTEMPT:
                        //----getting the additional parammeters and creating array for insert
                        $detail = array();
                        $detail['first_name'] = $this->central->getargs('first_name', $_REQUEST, $this->corrupt);
                        $detail['last_name'] = $this->central->getargs('last_name', $_REQUEST, $this->corrupt);
                        $detail['last_name'] = $this->central->getargs('last_name', $_REQUEST, $this->corrupt);
                        $detail['address'] = $this->central->getargs('address', $_REQUEST, $this->corrupt);
                        $detail['shipping_address'] = $this->central->getargs('shipping_address', $_REQUEST, $this->corrupt);
                        $detail['phone'] = $this->central->getargs('phone', $_REQUEST, $this->corrupt);
                        $detail['postcode'] = $this->central->getargs('postcode', $_REQUEST, $this->corrupt);
                        foreach($detail as $k => $d)
                        {
                            if(empty($d))
                                unset($detail[$k]);
                        }
                        $this->corrupt = false;
                        $detail['email'] = $name = $this->central->getargs('email', $_REQUEST, $this->corrupt);
                        $password = $this->central->getargs('signup_password', $_REQUEST, $this->corrupt);
                        $re_password = $this->central->getargs('signup_repassword', $_REQUEST, $this->corrupt);
                        if(!$this->corrupt && strcmp($password, $re_password) == 0)
                            $this->sign_up_new_user($name, $password, $detail, true);
                        else
                            throw new Exception("CSRF attack");
                        break;
                    default:
                        //this is some bullshit - raise the expcetion
                        throw new Exception("CSRF attack");
                        break;
                }
            }
        }
        catch(Exception $e)
        {
            echo $e->getMessage();
            throw $e;
        }
    }

}

?>