<?php

/**
 * @Package  Config\\ Central
 * @author Bugs - Ashtex
 * The central class for the package. Holds all the common utilities 
 */

namespace Config;

use mysqli;
use Plusql;
use Args;
use Fragmentify;
use Config\Constants;

class Central
{

    //constansts
    //pagination support - consts for ids and classes which the template must follow in order for pagination to work

    const ACTIVE_NEXT_ID = "active_page_next";
    const DISABLED_NEXT_ID = "dis_page_next";
    const ACTIVE_PREV_ID = "active_page_prev";
    const DISABLED_PREV_ID = "dis_page_prev";
    const PAGE_LINK_ID = "page";
    const ACTIVE_LINK_CLASS = "active_link";
    const HIDDEN_PAGE_NUM = "page_number";

    //private variables
    private $dbconfig;
    private $profile = 'central';
    private $file_path = '';
    private $server_path = '';
    private static $instance;

    // singleton - private constructor
    private function __construct()
    {
	try
	{
	    $this->dbconfig = Dbconfig::get_dbconfig();
	    $this->set_alias_connection( $this->profile );
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    //instance method
    public static function instance()
    {
	if ( is_null( Central::$instance ) )
	{
	    Central::$instance = new Central();
	}
	return Central::$instance;
    }

    //xml operations
    public static function xmlDataAttributeAsString( $xml, $name )
    {
	if ( !is_object( $xml[ $name ] ) )
	    $ret = $xml[ $name ];
	else
	    $ret = '';

	return ( string ) $ret;
    }

    //args custom function
    public function getargs( $par, $arr, &$corrupt )
    {
	$param = Args::get( $par, $arr );
	$corrupt = false;
	if ( ($param === null) || ($param == false) )
	{
	    $param = 0;
	    $corrupt = true;
	}
	return $param;
    }

    public function Notfound( $template )
    {
	try
	{
	    if ( preg_match( '/Event/', $_SERVER[ 'REQUEST_URI' ] ) )
		$back = '?r=Events';
	    else if ( preg_match( '/Form/', $_SERVER[ 'REQUEST_URI' ] ) )
		$back = '?r=Forms';
	    else
		$back = '?r=Dashboard';

	    $notfound = "<div class='not-found'>
                                <div class='col-md-8'>
                                    <div class='hero-unit center'>
                                        <h1>Page Not Found <small><font face='Tahoma' color='red'>Error 404</font></small></h1>
                                        <p>The page you requested could not be found, either contact your webmaster or try again. 
                                        Use your browsers <b>Back</b> button to navigate to the page you have prevously come from</p>
                                        <p><b>Or you could just press this neat little button:</b></p>
                                        <a href='$back' class='btn btn-large btn-info'><i class='icon-home icon-white'></i> Go Back</a>
                                    </div>
                                </div>
                            </div>";
	    $template->setValue( '.content-wrapper', $notfound, 1 );
	}
	catch ( Exception $ex )
	{
	    
	}
    }

    public function populate_user_contents( $template, $flag = 0 )
    {
	try
	{
	    if ( isset( $_SESSION[ 'user' ][ 'user_id' ] ) )
	    {
		$user_id = $_SESSION[ 'user' ][ 'user_id' ];
		$user_data = PluSQL::from( $this->profile )->user->select( '*' )->where( "user_id='$user_id'" )->run()->user;
		foreach ( $user_data as $user )
		{
		    $template->setValue( '.usrimg@src', "packages/profilepics/$user->profile_pic" );
		    $template->setValue( '.username', $user->user_name );
		}
		if ( $flag )
		{
		    $template->setValue( '.profile-user-img@src', "packages/profilepics/$user->profile_pic" );
		}
	    }
	    else
	    {
		@header( 'Location:?r=Login' );
	    }
	}
	catch ( Exception $e )
	{
	    
	}
    }

    //args custom function
    public function getargs_tst( $par, $arr, &$corrupt )
    {
	$param = Args::get( $par, $arr );
	$corrupt = false;
	if ( ($param === null) || ($param == false) )
	{
	    $param = 0;
	    $corrupt = false;
	}
	return $param;
    }

    //get the last id from the table
    public static function get_accurate_last_id( $table, $profile )
    {
	try
	{
	    $tid = $table . '_id';
	    $id = PluSQL::from( $profile )->$table->select( $tid )->limit( '0,1' )->orderBy( "$tid DESC" )->run()->$table->$tid;
	    return $id;
	}
	catch ( \EmptySetException $e )
	{
	    return 0;
	}
	catch ( \Exception $e )
	{
	    return 0;
	}
    }

    public static function manage_phone_number( $phone_number )
    {
	try
	{
	    $phone_number = trim( $phone_number );
	    if ( strpos( $phone_number, '(' ) !== false && strpos( $phone_number, ')' ) !== false && strpos( $phone_number, '-' ) !== false )
	    {
		$pos1 = strpos( $phone_number, '(' );
		$pos2 = strpos( $phone_number, ')' );
		$pos3 = strpos( $phone_number, '-' );
		if ( $pos1 == 0 && $pos2 == 4 && $pos3 == 8 )
		{
		    return $phone_number;
		}
	    }
	    else if ( strpos( $phone_number, '-' ) == 3 || strpos( $phone_number, '-' ) == 4 || strlen( $phone_number ) == 10 )
	    {
		$subsrt1 = str_split( $phone_number, 3 );
		$newsubsrt1 = '(' . $subsrt1[ 0 ] . ')';
		$actual_phone_number = $newsubsrt1 . $subsrt1[ 1 ] . '-' . $subsrt1[ 2 ] . $subsrt1[ 3 ];
		return $actual_phone_number;
	    }
	    else
	    {
		return "Invalid phone number";
	    }
	}
	catch ( Exception $ex )
	{
	    
	}
    }

    //to remove the last repeating structure from the loop
    public static function remove_last_repeating_element( $template, $ending_flag, $parent_count = 1, $previous_count = 0, $next_count = 0 )
    {
	$item = $template->query( $ending_flag )->item( 0 );
	$proto = $item;
	for ( $i = 1; $i <= $parent_count; $i ++ )
	{
	    $proto = $proto->parentNode;
	}
	for ( $i = 1; $i <= $previous_count; $i ++ )
	{
	    $item = $item->previousSibling;
	}
	for ( $i = 1; $i <= $next_count; $i ++ )
	{
	    $item = $item->nextSibling;
	}
	$proto->removeChild( $item );
    }

    //to remove the last repeating structure from the loop - Non Static version
    public function remove_repeating_element( $template, $ending_flag, $parent_count = 1, $previous_count = 0, $next_count = 0 )
    {
	$item = $template->query( $ending_flag )->item( 0 );
	$proto = $item->parentNode;
	for ( $i = 1; $i < $parent_count; $i ++ )
	{
	    $proto = $proto->parentNode;
	}
	for ( $i = 1; $i <= $previous_count; $i ++ )
	{
	    $item = $item->previousSibling;
	}
	for ( $i = 1; $i <= $next_count; $i ++ )
	{
	    $item = $item->nextSibling;
	}
	$proto->removeChild( $item );
    }

    //logging method for   project
    public static function log_error( $code, $data )
    {
	$link = Central::instance()->db_connect();
	$dbconfig = Central::instance()->get_dbconfig();
	$temp = ($link->select_db( $dbconfig[ Dbconfig::DB_NAME ] ));
	$temp = $link->query( "insert into errorlog (code, dump) values ( $code , '$data' )" );
	if ( $temp === TRUE )
	{
	    return true;
	}
	else
	{
	    throw new Exception( Constants::DBINSERT_ERROR );
	}
    }

    //----
    public function set_alias_connection( $alias )
    {
	try
	{
	    if ( ($alias === null) || ($alias == false) )
	    {
		throw new Exception( Exception::ALIAS_NOTAVAILABLE );
	    }
	    $this->dbconfig = Dbconfig::get_dbconfig();
	    $details = array
		(
		0 => $this->dbconfig[ Dbconfig::DB_HOST ],
		1 => $this->dbconfig[ Dbconfig::DB_USER ],
		2 => $this->dbconfig[ Dbconfig::DB_PASSWORD ],
		3 => $this->dbconfig[ Dbconfig::DB_NAME ],
	    );
	    Plusql::credentials( $alias, $details );
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    //delete old files from directory
    public static function delete_old_files( $dir )
    {
	if ( $d = @opendir( $dir ) )
	{
	    while ( ($file = readdir( $d )) !== false )
	    {
		if ( !is_file( $dir . '/' . $file ) || $file == 'index.php' )
		    continue;
		// then check to see if this one is too old
		$ftime = filemtime( $dir . '/' . $file );
		Central::pr( 'Found file: ' . $dir . '/' . $file );
		// seems 3 min is enough for any report download, isn't it?
		$temp = time() - $ftime;
		Central::pr( " $temp > 180" );
		if ( time() - $ftime > 180 )
		{
		    Central::pr( 'deleting file : ' . $dir . '/' . $file );
		    unlink( $dir . '/' . $file );
		}
	    }
	    closedir( $d );
	}
    }

    //settings functions
    //logging function
    public static function pr( $var, $empty = 0 )
    {
	return false;
	if ( is_array( $var ) )
	{
	    $var = print_r( $var, true );
	}
	if ( Constants::ON_SCREEN_DEBUG )
	    echo $var . "<br/>";
	$fp = @fopen( PACKAGES_DIR . Constants::LOG_DIR . "paperless.log", 'a' );
	if ( $fp == false )
	{
	    return;
	}
	$script_name = pathinfo( $_SERVER[ 'PHP_SELF' ], PATHINFO_FILENAME );
	$time = @date( '[d/M/Y:H:i:s]' );
	fwrite( $fp, "$time ($script_name) $var" . PHP_EOL );
	fclose( $fp );
    }

    //----
    public function update_dbconfig()
    {
	$this->dbconfig = Dbconfig::get_dbconfig();
    }

    //----
    private function get_dbconfig()
    {
	return $this->dbconfig;
    }

    //convert objects to string
    public static function objectToArray( $data )
    {
	if ( !is_array( $data ) && !is_object( $data ) )
	    return $data;
	$result = array();
	$data = ( array ) $data;
	foreach ( $data as $key => $value )
	{
	    if ( is_object( $value ) )
		$value = ( array ) $value;
	    if ( is_array( $value ) )
		$result[ $key ] = Central::objectToArray( $value );
	    else
		$result[ $key ] = $value;
	}

	return $result;
    }

    //loads any normal template
    public function load_normal( $file_name )
    {
	return $this->load_template( $file_name );
    }

    //loads the empty version of the same template
    public function load_empty( $file_name )
    {
	return $this->load_template( "empty_" . $file_name );
    }

    //get sever path
    public static function get_instant_server_path()
    {
	//configure pre-defined path
	$surl = (isset( $_SERVER[ 'HTTP' ] )) ? 'https://' : 'http://';
	$surl .= isset( $_SERVER[ 'SERVER_NAME' ] ) ? $_SERVER[ 'SERVER_NAME' ] : 'localhost';
	$path = pathinfo( $_SERVER[ 'PHP_SELF' ] );
	$surl .= $path[ 'dirname' ] . '/';
	$service_base = $surl;
	$file_path = Constants::HTML_PATH;
	if ( !is_null( $file_name ) )
	{
	    if ( strpos( $file_name, '/' ) !== null )
	    {
		$temp = explode( '/', $file_name );
		$file_name = $temp[ count( $temp ) - 1 ];
		for ( $i = 0; $i < count( $temp ) - 1; $i ++ )
		{
		    $file_path .= '/' . $temp[ $i ];
		}
	    }
	}
	$server_path = $service_base . PACKAGES_DIR . $file_path;
	return $server_path;
    }

    //--load template function
    private function load_template( $file_name )
    {
	try
	{
	    //configure pre-defined path
//	    if(!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == ""){
//	        $redirect = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
//	        header("HTTP/1.1 301 Moved Permanently");
//	        header("Location: $redirect");
//	    }
	    $surl = (isset( $_SERVER[ 'HTTP' ] )) ? 'https://' : 'http://';
	    $surl .= isset( $_SERVER[ 'SERVER_NAME' ] ) ? $_SERVER[ 'SERVER_NAME' ] : 'localhost';
	    $path = pathinfo( $_SERVER[ 'PHP_SELF' ] );
	    $surl .= $path[ 'dirname' ] . '/';
	    $service_base = $surl;
	    $file_path = Constants::HTML_PATH;
	    if ( strpos( $file_name, '/' ) !== null )
	    {
		$temp = explode( '/', $file_name );
		$file_name = $temp[ count( $temp ) - 1 ];
		for ( $i = 0; $i < count( $temp ) - 1; $i ++ )
		{
		    $file_path .= '/' . $temp[ $i ];
		}
	    }
	    $this->file_path = PACKAGES_DIR . $file_path . '/' . $file_name;
	    $this->server_path = $service_base . PACKAGES_DIR . $file_path;

	    // Load the template and adjust the paths
	    $apppaths = new \DOMTemplateAppPaths( (Constants::ENABLE_FRAGMENTIFY) ? Fragmentify::render( $this->file_path ) : $this->file_path, $this->server_path, Constants::ENABLE_FRAGMENTIFY );
	    $imgpaths = new \DOMTemplateImgPaths( $apppaths->process()->template(), $this->server_path );
	    return $imgpaths->process()->template();
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    public function get_server_path( $flag = false )
    {
	if ( !$flag )
	    return $this->server_path;
	else
	{

	    //configure pre-defined path
	    $surl = (isset( $_SERVER[ 'HTTP' ] )) ? 'https://' : 'http://';
	    $surl .= isset( $_SERVER[ 'SERVER_NAME' ] ) ? $_SERVER[ 'SERVER_NAME' ] : 'localhost';
	    $path = pathinfo( $_SERVER[ 'PHP_SELF' ] );
	    $surl .= $path[ 'dirname' ] . '/';
	    $service_base = $surl;
	    $server_path = $service_base;
	    Central::pr( $server_path );
	    return $server_path;
	}
    }

    public function get_server_url()
    {
	$surl = (isset( $_SERVER[ 'HTTP' ] )) ? 'https://' : 'http://';
	$surl .= isset( $_SERVER[ 'SERVER_NAME' ] ) ? $_SERVER[ 'SERVER_NAME' ] : 'localhost';
	$path = pathinfo( $_SERVER[ 'PHP_SELF' ] );
	$surl .= $path[ 'dirname' ] . '/';
	return $surl;
    }

    //-- the generic render function
    public function render( $template, $output = true )
    {
	try
	{
	    if ( $output )
	    {

		die( $template->html() );
	    }
	}
	catch ( Exception $e )
	{
	    if ( !$output )
	    {
		throw $e;
	    }
	}
    }

    public function flush( $mysql_pass, $verbose = 1 )
    {
	try
	{
	    if ( $this->pheanstalk_installed( $verbose ) )
	    {
		Dbconfig::set_dbpassword( $mysql_pass );
		$this->dbconfig = Dbconfig::get_dbconfig();
		if ( $this->db_exists() )
		{
		    //send the table data to the pipe - re start the pipe
		    $pheanstalk = new \Pheanstalk( Constants::SERVER_IP );
		    $tube = $pheanstalk->useTube( Constants::SEND_PIPE );
		    $link = $this->db_connect();
		    $temp = ($link->select_db( $this->dbconfig[ Dbconfig::DB_NAME ] ));
		    if ( $temp === True )
		    {
			$this->flush_pipe( $link, $tube );
		    }
		    else
		    {
			throw new Exception( Constants::DBSELECT_ERROR );
		    }
		}
	    }
	    else
	    {
		throw new \Exception( "unable to flush the messages to the pipe as the beanstalkd pipe is still not operational... \n" );
	    }
	}
	catch ( \Exception $e )
	{
	    throw $e;
	}
    }

    private function flush_pipe( $link, $tube )
    {
	try
	{
	    $result = $link->query( "select * from pipe_temporary_data" );
	    if ( $result )
	    {
		while ( $row = $result->fetch_assoc() )
		{
		    $data = $row[ 'pipe_data' ];
		    $tube->put( $data );
		}
	    }
	    $link->query( "delete from pipe_temporary_data" );
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    public function install( $mysql_pass, $verbose = 1 )
    {
	try
	{
	    //if($this->pheanstalk_installed($verbose)) {
	    Dbconfig::set_dbpassword( $mysql_pass );
	    $this->dbconfig = Dbconfig::get_dbconfig();
	    if ( $this->db_exists() )
	    {
		if ( $verbose )
		{
		    $msg = Constants::DB_RE_INSTALL_MSG;
		    fwrite( STDOUT, "$msg: " );
		    $res = trim( fgets( STDIN ) );
		    if ( $res == Constants::DB_RE_INSTALL_GOAHEAD )
		    {
			//confirm from user that the database will be removed
			$this->db_cleanup();
			$this->db_install();
		    }
		}
		else
		{
		    $this->db_cleanup();
		    $this->db_install();
		}
	    }
	    else
	    {
		$this->db_install();
	    }
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    public function db_connect()
    {
	$link = null;
	try
	{
	    $link = new mysqli(
		    $this->dbconfig[ Dbconfig::DB_HOST ], $this->dbconfig[ Dbconfig::DB_USER ], $this->dbconfig[ Dbconfig::DB_PASSWORD ]
	    );
	    if ( $link->connect_error )
	    {
		throw new Exception( Constants::DBCONNECTION_NOTAVAILABLE );
	    }
	    return $link;
	}
	catch ( Exception $e )
	{
	    if ( $link instanceof mysqli )
	    {
		$link->close();
	    }
	    throw $e;
	}
    }

    public function db_exists()
    {
	$link = null;
	try
	{
	    $link = $this->db_connect();
	    $temp = ($link->select_db( $this->dbconfig[ Dbconfig::DB_NAME ] ));
	    $link->close();
	    return $temp;
	}
	catch ( Exception $e )
	{
	    if ( $link instanceof mysqli )
	    {
		$link->close();
	    }
	    throw $e;
	}
    }

    public function db_cleanup()
    {
	$link = null;
	try
	{
	    $link = $this->db_connect();
	    $temp = $link->query( 'drop database ' . $this->dbconfig[ Dbconfig::DB_NAME ] );
	    if ( $temp === TRUE )
	    {
		return true;
	    }
	    else
	    {
		throw new Exception( Constants::DBDELETE_ERROR );
	    }
	}
	catch ( Exception $e )
	{
	    if ( $link instanceof mysqli )
	    {
		$link->close();
	    }
	    throw $e;
	}
    }

    public function db_install()
    {
	$link = null;
	try
	{
	    $link = $this->db_connect();
	    $temp = $link->query( 'create database ' . $this->dbconfig[ Dbconfig::DB_NAME ] );
	    if ( $temp === TRUE )
	    {
		$temp = ($link->select_db( $this->dbconfig[ Dbconfig::DB_NAME ] ));
		if ( $temp === True )
		{
		    $this->upload_dump( $link );
		}
		else
		{
		    throw new Exception( Constants::DBSELECT_ERROR );
		}
	    }
	    else
	    {
		throw new Exception( Constants::DBCREATE_ERROR );
	    }
	}
	catch ( Exception $e )
	{
	    if ( $link instanceof mysqli )
	    {
		$link->close();
	    }
	    throw $e;
	}
    }

    private function upload_dump( $link )
    {
	try
	{
	    $templine = '';
	    $path = PACKAGES_DIR . '/' . Constants::PACKAGE_NAME . '/' . $this->dbconfig[ Dbconfig::DB_DUMP ];
	    $lines = file( $path, FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES );
	    if ( !$lines )
	    {
		throw new Exception( Constants::DUMP_FILEOPEN_ERROR );
	    }
	    foreach ( $lines as $line )
	    {
		if ( substr( $line, 0, 2 ) == '--' || $line == '' )
		{
		    continue;
		}
		$templine .= $line;
		if ( substr( trim( $line ), -1, 1 ) == ';' )
		{
		    $temp = $link->query( $templine );
		    if ( !$temp )
		    {
			throw new Exception( Constants::DUMP_CORRUPTED );
		    }
		    $templine = '';
		}
	    }
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    public static function array_copy( $arr )
    {

	$newArray = array();

	foreach ( $arr as $key => $value )
	{
	    if ( is_array( $value ) )
	    {

		$newArray[ $key ] = self::array_copy( $value );
	    }
	    else if ( is_object( $value ) )
	    {

		$newArray[ $key ] = clone $value;
	    }
	    else
	    {

		$newArray[ $key ] = $value;
	    }
	}

	return $newArray;
    }

    //pagination support offered by central
    public function update_pagination( $total_pages, $template, $page_number = 1 )
    { //atleast we will have one page
	try
	{
	    //we have the total number of pages - first adjust the pagaintion html
	    //take care of crazy user
	    if ( $page_number < 1 )
	    {
		$page_number = 1;
	    }
	    if ( $page_number > $total_pages )
	    {
		$page_number = $total_pages;
	    }
	    $curr_page = $page_number;
	    //adjust the previous button
	    if ( $page_number == 1 )
	    { //disable the previous button
		$template->remove( "#" . self::ACTIVE_PREV_ID );
	    }
	    else
	    { //enable the previous button
		$template->remove( "#" . self::DISABLED_PREV_ID );
	    }
	    //adjust the next button
	    if ( $page_number == $total_pages )
	    {
		//disable the next button
		$template->remove( "#" . self::ACTIVE_NEXT_ID );
	    }
	    else
	    { //enable the next button
		$template->remove( "#" . self::DISABLED_NEXT_ID );
	    }
	    //adjust the page links
	    $proto = $template->query( "#" . self::PAGE_LINK_ID )->item( 0 );
	    $parent = $proto->parentNode;
	    $item = $proto->cloneNode( true );
	    for ( $i = 1; $i <= $total_pages; $i ++ )
	    {
		$item->textContent = $i;
		$item->nodeValue = $i;
		if ( $i != $page_number )
		{
		    $item->attributes->getNamedItem( 'class' )->nodeValue = "";
		}
		else
		{
		    $item->attributes->getNamedItem( 'class' )->nodeValue = self::ACTIVE_LINK_CLASS;
		}
		$parent->insertBefore( $item, $proto );
		$item = $item->cloneNode( true );
	    }
	    $parent->removeChild( $proto );
	    $template->setValue( "#" . self::HIDDEN_PAGE_NUM . "@value", $page_number );
	    return $curr_page;
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    //This function will for repeating elements in div or selection...
    public static function repeat_list( $template, $selname, $option, $id, $name, $selected = null )
    {
	$index = $template->query( $selname )->item( 0 );
	$org = $index->childNodes->item( 1 );
	$opt = $org->cloneNode( true );
	foreach ( $option as $optt )
	{
	    $opt = $opt->cloneNode( true );
	    $opt->nodeValue = $optt[ $name ];
	    $opt->attributes->getNamedItem( 'value' )->nodeValue = $optt[ $id ];
	    $index->appendChild( $opt );
	}
	//$index -> removeChild($org);
    }

    public static function get_request_title( $string = '', $flag = 0 )
    {
	if ( $flag )
	    $title = substr( $string, 0, 12 ) . '...';
	else
	    $title = substr( $string, 0, 14 ) . '...';
	return $title;
    }

    public static function set_request_description( $string = '' )
    {
	if ( strlen( $string ) > 90 )
	{
	    $desc = substr( $string, 0, 90 ) . '...';
	    return $desc;
	}
	else
	    return $string;
    }

    public static function repeat_select_options( $template, $select_id, $options, $sel_val = '' )
    {
	$temp = $template->query( $select_id )->item( 0 );
	$org = $temp->childNodes->item( 1 );
	$par = $temp->parentNode;
	$proto = $org->cloneNode( true );
	foreach ( $options as $value => $text )
	{
	    $opt = $proto->cloneNode( true );
	    $opt->nodeValue = htmlentities( $text );
	    $opt->attributes->getNamedItem( 'value' )->nodeValue = $value;
	    if ( !empty( $sel_val ) )
	    {
		if ( strcmp( trim( $sel_val ), trim( $value ) ) == 0 )
		{
		    $opt->setAttribute( 'selected', 'selected' );
		}
	    }
	    $temp->appendChild( $opt );
	}
	$ff = $temp->childNodes->item( 1 );
	$temp->removeChild( $ff );
    }

    //this function will give us the date with dummy response
    public static function set_date_formate( $date )
    {
	date_default_timezone_set( date_default_timezone_get() );
	$date = explode( '(', $date );
	$date = $date[ 1 ];
	$date = explode( ')', $date );
	$date = $date[ 0 ];
	$date1 = date( "Y-m-d" );
	$date2 = date( 'Y-m-d', substr( $date, 0, -3 ) );
	$diff = abs( strtotime( $date2 ) - strtotime( $date1 ) );
	$days = floor( ($diff) / (60 * 60 * 24) );
	if ( $days >= 7 )
	{
	    $days = $days / 7;
	    if ( $days > 4 )
	    {
		$days = $days / 4;
		if ( $days >= 12 )
		{
		    $days = $days / 12;
		    return intval( $days ) . "yr";
		}
		else
		    return intval( $days ) . "mo";
	    }
	    else
	    {
		return intval( $days ) . "wk";
	    }
	}
	else
	    return intval( $days ) . "d";
    }

    public static function base64url_encode( $data )
    {
	return rtrim( strtr( base64_encode( $data ), '+/', '-_' ), '=' );
    }

    public static function base64url_decode( $data )
    {
	return base64_decode( str_pad( strtr( $data, '-_', '+/' ), strlen( $data ) % 4, '=', STR_PAD_RIGHT ) );
    }

    public static function check_service_subscription( $service_name, $school_id, $profile )
    {
	$return[ 'check' ] = 0;
	try
	{
	    $orders = Plusql::from( $profile )->orders->select( '*' )->where( 'school_id=' . $school_id . " AND user_id={$_SESSION[ 'user' ][ 'user_id' ]} AND status != 'expired' AND ipn = 1 AND pdt = 1" )->run()->orders;
	    foreach ( $orders as $order )
	    {
		try
		{
		    $orders = Plusql::from( $profile )->orders_details->select( '*' )->where( "orders_id={$order->orders_id} AND is_deleted <> 1 AND label = '{$service_name}'" )->run()->orders_details;
		    $return[ 'status' ] = $order->status;
		    $return[ 'date' ] = $order->date;
		    $return[ 'check' ] = 1;
		}
		catch ( \Exception $ex )
		{
		    continue;
		}
	    }
	}
	catch ( \Exception $ex )
	{
	    $return[ 'check' ] = 0;
	}
	return $return;
    }

    public function create_password_randomly()
    {
	$hash = md5( uniqid() . time() );
	return substr( $hash, rand( 0, 4 ), rand( 5, 9 ) );
    }

    public function check_user_login_status( $profile )
    {
	try
	{
	    if ( isset( $_SESSION[ 'user' ][ 'user_id' ] ) )
	    {
		try
		{
		    $user = \Plusql::from( $profile )->user->select( '*' )->where( 'user_id = ' . $_SESSION[ 'user' ][ 'user_id' ] . " AND status = 'active'" )->run()->user;
		    return true;
		}
		catch ( \EmptySetException $ex )
		{
		    return false;
		}
	    }
	}
	catch ( Exception $ex )
	{
	    return false;
	}
    }

    public static function check_existance( $profile, $table, $where, $select = "*", $limit = "0, 1" )
    {
	try
	{
	    $result = Plusql::from( $profile )->$table->select( $select )->where( $where )->limit( $limit )->run()->$table;
	}
	catch ( \EmptySetException $ex )
	{
	    $result = false;
	}
	catch ( \Exception $ex )
	{
	    $result = false;
	}

	return $result;
    }

    public function send_email( $data )
    {
	try
	{
	    $transport = \Swift_SmtpTransport::newInstance( Constants::EMAIL_SMTP, Constants::EMAIL_SMTP_PORT, 'ssl' );
	    $transport->setUsername( Constants::FROM_EMAIL );
	    $transport->setPassword( Constants::FROM_EMAIL_PASS );
//            $transport->setPort( Constants::EMAIL_SMTP_PORT )->setEncryption( 'ssl' );
	    $swift_instance = \Swift_Mailer::newInstance( $transport );
	    $message = new \Swift_Message( $data[ "subject" ] );
	    $message->setFrom( array( Constants::FROM_EMAIL => 'Paperless' ) );
	    $message->setBody( $data[ 'html_body' ], 'text/html' );
	    $message->addPart( $data[ 'text_body' ], 'text/plain' );
	    $message->setTo( $data[ 'email' ] );
	    $failed = array();
	    $result = $swift_instance->send( $message, $failed );
	    return $result;
	}
	catch ( Exception $e )
	{
	    return false;
	}
    }

    public function send_email_to_file( $data )
    {
	try
	{

	    $message = new \Swift_Message( $data[ "subject" ] );
	    $message->setFrom( array( Constants::FROM_EMAIL => 'Paperless' ) );
	    $message->setBody( $data[ 'html_body' ], 'text/html' );
	    $message->addPart( $data[ 'text_body' ], 'text/plain' );
	    $message->setTo( $data[ 'email' ] );
	    $file_name = date( "Ymdhisa" ) . '.txt';
	    file_put_contents( 'packages/email/' . $file_name, $message );
	    return true;
	}
	catch ( Exception $e )
	{
	    return false;
	}
    }

    public static function generate_random_string()
    {
//        return substr( str_shuffle( abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ), 0, 1 ) . substr( str_shuffle( 0123456789 ), 0, 1 );
	return rand( 0000, 9999 );
    }

}

?>
