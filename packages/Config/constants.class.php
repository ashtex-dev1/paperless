<?php

/**
 * @Package  Config\\ Constants
 * @author Bugs - Ashtex
 * The constant define class for the package
 */

namespace Config;

class Constants {

    const ON_SCREEN_DEBUG = 0;
    //constansts for install utility
    const PACKAGE_NAME = 'Config/dumps';
    const ARG_NAME = 'mysql_root';
    const DB_RE_INSTALL_MSG = 'The database already exists. Please type "yes" to confirm that you want to delete and install it again';
    const DB_RE_INSTALL_GOAHEAD = 'yes';
    const HTML_PATH = '/Views';
    const EXCEL_PATH = '/files/';
    const PHASE1_HTML_PATH = 'phase1';
    const ARCHIVE = 'archive';
    //pagination for the entire package
    const PAGINATION_COUNT = 20;
    //logging directory
    const LOG_DIR = "/Logs/"; //The directory will not be created automatically. Please create it manually
    //constants for global settings incase they are missing from the database --- safe fall back
    const GLOBAL_SETTING_ID = 1;
    //to enable the fragmentation
    const ENABLE_FRAGMENTIFY = 1;
    //-----------------------------------------------------------
    const UNKNOWN_ERROR = "The error source cannot be identified";
    const DBHOST_NOTAVAILABLE = "The database host is not defined";
    const DBNAME_NOTAVAILABLE = "The database name is not defined";
    const DBUSER_NOTAVAILABLE = "The database user is not defined";
    const DBPASSWORD_NOTAVAILABLE = "The database password is not defined";
    const DBDUMP_NOTAVAILABLE = "The database dump file is not available";
    const DB_USERPASSWORD_NOTAVAILABLE = "The provided password is either empty or null";
    const DBCONNECTION_NOTAVAILABLE = "Connection to the Database failed";
    const DBDELETE_ERROR = "Unable to delete the specified database";
    const DBCREATE_ERROR = "Unable to create the specified database";
    const DBSELECT_ERROR = "Unable to select the specified database";
    const DUMP_FILEOPEN_ERROR = "Unable to open the sql dump file";
    const DUMP_CORRUPTED = "The dump file is corrupted. Unable to load to load the dump";
    const ARG_NOTAVAILABLE = "The argument passed is rejected. Please provide argument as 'mysql_root=MYROOTPASS'";
    const ALIAS_NOTAVAILABLE = "Alias cannot be null or empty";
    const DBINSERT_ERROR = "Unable to insert data into the database";
    //----------------------------
    const SMS_SERIVCE_PRICE = 0.5;
    const MARKETING_REPORT_PRICE = 0.5;
    const FROM_EMAIL = 'test@rockinhfarmtoys.com';
    const FROM_EMAIL_PASS = 'A3iqFGcFWS-7B*';
    const EMAIL_SMTP = 'box349.bluehost.com';
    const EMAIL_SMTP_PORT = 465;
    const EMAIL_SUBJECT = 'Welcome to Paperless';
    const SIGNUP_EMAIL_BODY = 'Hey %username%,\n Congratulations you are successfully registed to Paperless.\n To access your account please use the following details:\n\n Username : %user_name%\n Password: %password%\n\nClick here to login now %link%.';
    // ---------------------------------
    const STATUS_ALL = 'all';
    const STATUS_GENERAL = 'general';
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';
    const STATUS_DELETED = 'deleted';
    const STATUS_NOTCONFIRMED = '0';
    const STATUS_CONFIRMED = '1';
    const BULK_USERS = 'bulk_users';
    const CUSTOM_USERS = 'custom_user';
    const INDIVIDUAL_USER = 'individual_user';

}

?>
