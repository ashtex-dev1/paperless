-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 14, 2015 at 01:03 PM
-- Server version: 5.5.43-0ubuntu0.14.04.1-log
-- PHP Version: 5.5.9-1ubuntu4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sheldon`
--
CREATE DATABASE IF NOT EXISTS `sheldon` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `sheldon`;

-- --------------------------------------------------------

--
-- Table structure for table `burial_choice`
--

DROP TABLE IF EXISTS `burial_choice`;
CREATE TABLE IF NOT EXISTS `burial_choice` (
  `burial_choice_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `emblamed` tinyint(1) DEFAULT NULL,
  `open_casket` tinyint(1) DEFAULT NULL,
  `purchased_plot` tinyint(1) DEFAULT NULL,
  `purchased_plotDetails` longtext,
  `date_created` datetime DEFAULT NULL,
  `created_user` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`burial_choice_id`),
  KEY `burial_choice_id` (`burial_choice_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=138 ;

-- --------------------------------------------------------

--
-- Table structure for table `burial_options`
--

DROP TABLE IF EXISTS `burial_options`;
CREATE TABLE IF NOT EXISTS `burial_options` (
  `burial_options_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) DEFAULT NULL,
  `selection` tinyint(1) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_user` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`burial_options_id`),
  KEY `burial_options_id` (`burial_options_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
CREATE TABLE IF NOT EXISTS `city` (
  `city_id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `city` varchar(128) NOT NULL,
  `state` enum('AB','AK','AL','AR','AZ','BC','CA','CO','CT','DE','FL','GA','HI','IA','ID','IL','IN','KS','KY','LA','MA','MB','MD','ME','MI','MN','MO','MS','MT','NB','NC','ND','NE','NH','NJ','NL','NM','NS','NT','NU','NV','NY','OH','OK','ON','OR','PA','PE','QC','RI','SC','SD','SK','TN','TX','UT','VA','VT','WA','WI','WV','WY','YT') NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8017 ;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
CREATE TABLE IF NOT EXISTS `country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) NOT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=240 ;

-- --------------------------------------------------------

--
-- Table structure for table `cremation_options`
--

DROP TABLE IF EXISTS `cremation_options`;
CREATE TABLE IF NOT EXISTS `cremation_options` (
  `cremation_options_id` int(11) NOT NULL AUTO_INCREMENT,
  `desposal_type` varchar(50) DEFAULT NULL,
  `cremation_notes` longtext,
  `date_created` datetime DEFAULT NULL,
  `created_user` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`cremation_options_id`),
  KEY `cremation_options_id` (`cremation_options_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `death`
--

DROP TABLE IF EXISTS `death`;
CREATE TABLE IF NOT EXISTS `death` (
  `death_id` int(10) NOT NULL AUTO_INCREMENT,
  `member_id` int(10) NOT NULL,
  `created_date` date NOT NULL,
  `user_created` text CHARACTER SET latin1 NOT NULL,
  `user_modified` text CHARACTER SET latin1 NOT NULL,
  `status` text COLLATE utf8_unicode_ci NOT NULL,
  `modified_date` date NOT NULL,
  PRIMARY KEY (`death_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `directors`
--

DROP TABLE IF EXISTS `directors`;
CREATE TABLE IF NOT EXISTS `directors` (
  `director_id` int(11) NOT NULL AUTO_INCREMENT,
  `position` varchar(50) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `province` varchar(50) DEFAULT NULL,
  `postal_code` varchar(50) DEFAULT NULL,
  `phone_no_1` varchar(50) DEFAULT NULL,
  `phone_no_2` varchar(50) DEFAULT NULL,
  `elected_date` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_user` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`director_id`),
  KEY `director_id` (`director_id`),
  KEY `postal_code` (`postal_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `excludes`
--

DROP TABLE IF EXISTS `excludes`;
CREATE TABLE IF NOT EXISTS `excludes` (
  `excludes_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_user` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`excludes_id`),
  KEY `excludes_id` (`excludes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `executor`
--

DROP TABLE IF EXISTS `executor`;
CREATE TABLE IF NOT EXISTS `executor` (
  `executor_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `province` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `postal_code` varchar(8) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `relationship` varchar(50) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_user` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`executor_id`),
  KEY `executor_id` (`executor_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=950 ;

-- --------------------------------------------------------

--
-- Table structure for table `funeral_home`
--

DROP TABLE IF EXISTS `funeral_home`;
CREATE TABLE IF NOT EXISTS `funeral_home` (
  `funeral_home_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `province` varchar(50) DEFAULT '0',
  `country` varchar(50) DEFAULT '0',
  `phone` varchar(15) DEFAULT NULL,
  `postal_code` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_user` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`funeral_home_id`),
  KEY `funeral_home_id` (`funeral_home_id`),
  KEY `postal_code` (`postal_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=54 ;

-- --------------------------------------------------------

--
-- Table structure for table `hospital`
--

DROP TABLE IF EXISTS `hospital`;
CREATE TABLE IF NOT EXISTS `hospital` (
  `hospital_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `date_created` datetime DEFAULT '0000-00-00 00:00:00',
  `created_user` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`hospital_id`),
  KEY `hospital_id` (`hospital_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `includes`
--

DROP TABLE IF EXISTS `includes`;
CREATE TABLE IF NOT EXISTS `includes` (
  `includes_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_user` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`includes_id`),
  KEY `includes_id` (`includes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `instruction`
--

DROP TABLE IF EXISTS `instruction`;
CREATE TABLE IF NOT EXISTS `instruction` (
  `instruction_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_memorial_office_address_id` int(11) NOT NULL DEFAULT '0',
  `notice_in_paper` tinyint(1) DEFAULT NULL,
  `gifts_not_flowers` tinyint(1) DEFAULT NULL,
  `gifts_to` varchar(50) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_user` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`instruction_id`),
  KEY `instruction_id` (`instruction_id`),
  KEY `member_memorial_office_address_id` (`member_memorial_office_address_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=535 ;

-- --------------------------------------------------------

--
-- Table structure for table `marital_status`
--

DROP TABLE IF EXISTS `marital_status`;
CREATE TABLE IF NOT EXISTS `marital_status` (
  `marital_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) DEFAULT NULL,
  `date_created` datetime DEFAULT '0000-00-00 00:00:00',
  `created_user` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`marital_status_id`),
  KEY `marital_status_id` (`marital_status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `medical`
--

DROP TABLE IF EXISTS `medical`;
CREATE TABLE IF NOT EXISTS `medical` (
  `medical_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT '0',
  `research` tinyint(1) DEFAULT NULL,
  `hospital_id` int(11) DEFAULT '0',
  `consent_organ_donation` tinyint(1) DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_user` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_user` varchar(50) DEFAULT NULL,
  `statusStatus` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`medical_id`),
  UNIQUE KEY `member_id` (`member_id`),
  KEY `medical_id` (`medical_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
CREATE TABLE IF NOT EXISTS `member` (
  `member_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `membership_no` varchar(50) NOT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `street_address` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `province` varchar(50) DEFAULT '0',
  `postal_code` varchar(50) DEFAULT NULL,
  `phone_number` varchar(50) DEFAULT NULL,
  `membership_date` datetime DEFAULT NULL,
  `funeral_home_id` int(11) DEFAULT '0',
  `minister_church` varchar(50) DEFAULT NULL,
  `veteran` tinyint(1) DEFAULT NULL,
  `body_treatment` varchar(50) DEFAULT NULL,
  `plan_id` int(11) DEFAULT NULL,
  `research` tinyint(1) DEFAULT NULL,
  `hospital_id` int(11) DEFAULT NULL,
  `consent_organ_donation` tinyint(1) DEFAULT NULL,
  `medical_notes` longtext,
  `status` varchar(50) DEFAULT 'active',
  `date_inactive` datetime DEFAULT NULL,
  `deceased_status` tinyint(1) DEFAULT NULL,
  `transfer_out_status` tinyint(1) DEFAULT NULL,
  `transfer_in_status` tinyint(1) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `occupation` varchar(50) DEFAULT NULL,
  `birth_city` varchar(50) DEFAULT NULL,
  `birth_province` varchar(50) DEFAULT NULL,
  `birth_country` varchar(50) DEFAULT NULL,
  `social_insurance_number` double DEFAULT NULL,
  `drivers_license` int(11) DEFAULT NULL,
  `birth_date` datetime DEFAULT NULL,
  `marital_status_id` int(11) DEFAULT NULL,
  `spouse_middle_name` varchar(50) DEFAULT NULL,
  `spouses_name` varchar(50) DEFAULT NULL,
  `notes` longtext,
  `date_created` datetime DEFAULT NULL,
  `created_user` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_user` varchar(50) DEFAULT NULL,
  `amount_details` varchar(500) NOT NULL,
  `clergy_church` varchar(100) NOT NULL,
  PRIMARY KEY (`member_id`),
  UNIQUE KEY `membership_no` (`membership_no`),
  KEY `funeral_home_id` (`funeral_home_id`),
  KEY `member_id` (`member_id`),
  KEY `plan_choice_id` (`plan_id`),
  KEY `marital_status_id` (`marital_status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3939 ;

-- --------------------------------------------------------

--
-- Table structure for table `member_burial`
--

DROP TABLE IF EXISTS `member_burial`;
CREATE TABLE IF NOT EXISTS `member_burial` (
  `member_burial_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL DEFAULT '0',
  `burial_options_id` int(11) NOT NULL DEFAULT '0',
  `burial_plot_cemetry` varchar(50) DEFAULT NULL,
  `burial_plot_town` varchar(50) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_user` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`member_id`,`burial_options_id`),
  KEY `burial_options_id` (`burial_options_id`),
  KEY `member_burial_id` (`member_burial_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `member_cremation`
--

DROP TABLE IF EXISTS `member_cremation`;
CREATE TABLE IF NOT EXISTS `member_cremation` (
  `member_cremation_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `cremation_options_id` int(11) DEFAULT NULL,
  `cremation_notes` longtext,
  `date_created` datetime DEFAULT NULL,
  `created_user` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`member_cremation_id`),
  UNIQUE KEY `member_cremation_id` (`member_cremation_id`),
  UNIQUE KEY `member_id` (`member_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=409 ;

-- --------------------------------------------------------

--
-- Table structure for table `member_deceased`
--

DROP TABLE IF EXISTS `member_deceased`;
CREATE TABLE IF NOT EXISTS `member_deceased` (
  `member_deceased_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL DEFAULT '0',
  `deceased_date` datetime NOT NULL,
  `records_fee_received` tinyint(1) DEFAULT NULL,
  `records_fee_expected` tinyint(1) DEFAULT '1',
  `reason_fee_unexpected` varchar(50) DEFAULT NULL,
  `funeral_home_id` int(11) DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `created_user` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`member_id`,`deceased_date`),
  UNIQUE KEY `member_deceased_id` (`member_deceased_id`),
  UNIQUE KEY `member_id` (`member_id`),
  KEY `deceased_date` (`deceased_date`),
  KEY `funeral_home_id` (`funeral_home_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1187 ;

-- --------------------------------------------------------

--
-- Table structure for table `member_relative`
--

DROP TABLE IF EXISTS `member_relative`;
CREATE TABLE IF NOT EXISTS `member_relative` (
  `member_relative` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `relation` varchar(255) NOT NULL,
  `address` varchar(500) NOT NULL,
  `phone_no` varchar(50) NOT NULL,
  PRIMARY KEY (`member_relative`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=72 ;

-- --------------------------------------------------------

--
-- Table structure for table `member_transfers_in`
--

DROP TABLE IF EXISTS `member_transfers_in`;
CREATE TABLE IF NOT EXISTS `member_transfers_in` (
  `member_transfers_in_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL DEFAULT '0',
  `transfer_society_id` int(11) DEFAULT '0',
  `transfer_date` datetime DEFAULT NULL,
  `transfer_in` tinyint(1) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_user` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`member_transfers_in_id`),
  KEY `member_id` (`member_id`),
  KEY `member_transfers_in_id` (`member_transfers_in_id`),
  KEY `transfer_society_id` (`transfer_society_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

-- --------------------------------------------------------

--
-- Table structure for table `member_transfers_out`
--

DROP TABLE IF EXISTS `member_transfers_out`;
CREATE TABLE IF NOT EXISTS `member_transfers_out` (
  `member_transfers_out_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT '0',
  `transfer_society_id` int(11) DEFAULT '0',
  `transfer_date` datetime NOT NULL,
  `transfer_out` tinyint(1) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_user` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`member_transfers_out_id`),
  UNIQUE KEY `member_transfers_out_id` (`member_transfers_out_id`),
  UNIQUE KEY `member_id` (`member_id`),
  KEY `transfer_society_id` (`transfer_society_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=107 ;

-- --------------------------------------------------------

--
-- Table structure for table `memorial_office_address`
--

DROP TABLE IF EXISTS `memorial_office_address`;
CREATE TABLE IF NOT EXISTS `memorial_office_address` (
  `memorial_office_address_id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `province` varchar(50) DEFAULT NULL,
  `postal_code` varchar(50) DEFAULT NULL,
  `phone_no_1` varchar(50) DEFAULT NULL,
  `phone_no_2` varchar(50) DEFAULT NULL,
  `office_start_year` int(11) DEFAULT '0',
  `office_end_year` int(11) DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `created_user` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`memorial_office_address_id`),
  KEY `postal_code` (`postal_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `Option`
--

DROP TABLE IF EXISTS `Option`;
CREATE TABLE IF NOT EXISTS `Option` (
  `option_id` int(11) NOT NULL AUTO_INCREMENT,
  `cost` int(11) DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `description` longtext,
  `date_created` datetime DEFAULT NULL,
  `created_user` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`option_id`),
  KEY `option_id` (`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `parent`
--

DROP TABLE IF EXISTS `parent`;
CREATE TABLE IF NOT EXISTS `parent` (
  `parent_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT '0',
  `type` varchar(1) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `birth_city` varchar(50) DEFAULT NULL,
  `birth_town` varchar(50) DEFAULT NULL,
  `birth_province` varchar(50) DEFAULT NULL,
  `birth_country` varchar(50) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_user` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`parent_id`),
  KEY `member_id` (`member_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1155 ;

-- --------------------------------------------------------

--
-- Table structure for table `plan`
--

DROP TABLE IF EXISTS `plan`;
CREATE TABLE IF NOT EXISTS `plan` (
  `plan_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `cost` int(11) DEFAULT '0',
  `description` longtext,
  `extra_description` longtext,
  `status` varchar(50) CHARACTER SET utf8mb4 DEFAULT 'active',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_user` varchar(50) DEFAULT NULL,
  `type` varchar(50) NOT NULL DEFAULT '-',
  PRIMARY KEY (`plan_id`),
  KEY `plan_id` (`plan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

-- --------------------------------------------------------

--
-- Table structure for table `plan_exclude`
--

DROP TABLE IF EXISTS `plan_exclude`;
CREATE TABLE IF NOT EXISTS `plan_exclude` (
  `plan_exclude_id` int(11) NOT NULL DEFAULT '0',
  `exclude_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`plan_exclude_id`,`exclude_id`),
  KEY `exclude_id` (`exclude_id`),
  KEY `plan_exclude_id` (`plan_exclude_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `plan_include`
--

DROP TABLE IF EXISTS `plan_include`;
CREATE TABLE IF NOT EXISTS `plan_include` (
  `plan_include_id` int(11) NOT NULL DEFAULT '0',
  `includes_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`plan_include_id`,`includes_id`),
  KEY `includes_id` (`includes_id`),
  KEY `plan_include_id` (`plan_include_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `plan_options`
--

DROP TABLE IF EXISTS `plan_options`;
CREATE TABLE IF NOT EXISTS `plan_options` (
  `plan_options_id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_id` int(11) DEFAULT '0',
  `option_id` int(11) DEFAULT '0',
  `date_created` datetime DEFAULT '0000-00-00 00:00:00',
  `created_user` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`plan_options_id`),
  KEY `option_id` (`option_id`),
  KEY `plan_id` (`plan_id`),
  KEY `plan_options_id` (`plan_options_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `price`
--

DROP TABLE IF EXISTS `price`;
CREATE TABLE IF NOT EXISTS `price` (
  `price_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `plan_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `cost` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` date NOT NULL,
  `created_user` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `modified_date` date NOT NULL,
  PRIMARY KEY (`price_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
CREATE TABLE IF NOT EXISTS `transaction` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `funeral_home_id` int(11) DEFAULT '0',
  `member_id` int(11) DEFAULT '0',
  `records_fee` tinyint(1) DEFAULT NULL,
  `date_records_fee_received` datetime DEFAULT NULL,
  `funeral_home_receipt_issued` varchar(50) DEFAULT NULL,
  `date_funeral_home_receipt_issued` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_user` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`transaction_id`),
  KEY `funeral_home_id` (`funeral_home_id`),
  KEY `member_id` (`member_id`),
  KEY `transaction_id` (`transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transfer_society`
--

DROP TABLE IF EXISTS `transfer_society`;
CREATE TABLE IF NOT EXISTS `transfer_society` (
  `transfer_society_id` int(11) NOT NULL AUTO_INCREMENT,
  `society_name` varchar(50) DEFAULT NULL,
  `society_address` varchar(50) DEFAULT NULL,
  `society_city` varchar(50) DEFAULT NULL,
  `society_province` varchar(50) DEFAULT NULL,
  `society_postal_code` varchar(50) DEFAULT NULL,
  `society_country` varchar(50) DEFAULT NULL,
  `society_phone` varchar(50) DEFAULT NULL,
  `status` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `date_created` datetime DEFAULT '0000-00-00 00:00:00',
  `created_user` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`transfer_society_id`),
  KEY `transfer_society_id` (`transfer_society_id`),
  KEY `society_postal_code` (`society_postal_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` varchar(50) NOT NULL,
  `role` varchar(50) NOT NULL,
  `temp_link` varchar(100) NOT NULL,
  `confirmation` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
