<?php

use \Config\Constants;
use \Config\Central;

class EventsAction implements RocketSled\Runnable {

    const SEND_SAVE_EVENT = 1;    
    
//--private members
    private $profile = 'db';
    private $central;
    private $duplicateUser = false;
    public function __construct() {
        try {
            @session_start();
            $this->central = Central::instance();
            $this->central->set_alias_connection($this->profile);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function run() {
        try {
            $this->update_main_contents();
        } catch (Exception $ex) {
            
        }
    }

    private function update_main_contents() {
        try {           
            $corrupt = false;
            $action = $this->central->getargs('action', $_GET, $corrupt);
            if ($action) {
                switch (!$corrupt) {
                    case self::SEND_SAVE_EVENT:
                        $this->AddNewEvent();
                        break;
                    default:
                        $return = array('success' => 0, 'error' => 1, 'message' => 'Error');
                        break;
                }
            } else if (isset($_POST['new_event'])) {
                $this->AddNewEvent();
            } else if (!empty($_POST['update_event'])) {
                $this->UpdateAndNewEvent();
            } else if (!empty($_POST['delete_event'])) {
                $this->DeleteEvent();
            } else if (!empty($_POST['SendEvent'])) {                
                $this->SendEvent();
            }
            else if(!empty($_POST['AddUser']))
            {
                $user = $_POST['AddUser'];
                if($this->AddUsers($user))
                {
                    header('Location:?r=Users');
                }
                else if(!$this->AddUsers($user) && $this->duplicateUser)
                {
                    header('Location:?r=Users&error=1');
                }
                else
                {
                    header('Location:?r=Users&error=2');
                }
            }
            else if (!empty($_POST['SaveAndSend'])) {
                $inserted = $this->UpdateAndNewEvent(1);
                if ($inserted) {
                    $_POST['hidden_id'] = $this->central->get_accurate_last_id('event', $this->profile);
                    $this->SendEvent(1);
                }
            }
        } catch (Exception $ex) {
            
        }
    }

    private function DeleteEvent() {
        try {
            $corrupt = false;
            $event_id = $this->central->getargs('delete_event', $_POST, $corrupt);
            $event_id = Central::base64url_decode($event_id);
            if (!$corrupt) {
                Plusql::on($this->profile)->event(array('deleted' => 1))->where("event_id='$event_id'")->update();
            }
        } catch (Exception $ex) {
            $this->Response(1);
        }
        $this->Response();
    }

    private function SendEvent($flag = 0) {
        try {
            $corrupt = false;
            $is_success = false;
            $users = $this->central->getargs('SendEvent', $_POST, $corrupt);
            if ($flag) {
                $users = $this->central->getargs('SaveAndSend', $_POST, $corrupt);
                $users = explode(',', $users);
            }
            if (!$corrupt) {
                foreach ($users as $user) {
                    if ($user) {
                        $success = $this->AddUsers($user);
                        if ($success) {
                            $is_success = true;
                        } else {
                            $this->DeleteLastUser($this->central->get_accurate_last_id('user', $this->profile));
                            $_SESSION['event']['MailEror'] = 1;
                            header('Location:?r=Events');
                        }
                    }
                }
                if ($is_success) {
                    $updated = $this->Published();
                    if ($updated) {
                        $_SESSION['event']['EventSent'] = 1;
                    }
                } else {
                    $_SESSION['event']['EventSentEror'] = 1;
                }
            } else {
                $_SESSION['event']['EventSentEror'] = 1;
            }
            header('Location:?r=Events');
        } catch (Exception $ex) {
            
        }
    }

    private function DeleteLastUser($user_id) {
        try {
            $sql = "DELETE FROM user WHERE user_id='$user_id'";
            Plusql::against($this->profile)->run($sql);
            return true;
        } catch (Exception $ex) {
            
        }
    }

    private function AddUsers($user) {
        try {
            $mailsent = FALSE;
            $already = $this->IsAlready($user);
            if (!$already) {
                $password = $this->AddNewUser($user);
                if ($password) {
                    $password = $password['password'];                    
                    $mailsent = $this->SendMail($user, $password);
                } else {
                    return FALSE;
                }
            }
            else if($already && !empty($_POST['AddUser']))
            {
                $this->duplicateUser = true;
                return false;
            }
            else {
                $mailsent = $this->SendMail($user);
            }
            

            if ($mailsent)
                return TRUE;
            else if (!$mailsent)
            {                
                return FALSE;
            }                
        } catch (Exception $ex) {
            return FALSE;
        }
    }

    private function SendMail($user, $password = 0) {
        try {
            $email_body = '';
            if ($password) {
                $email_body = $this->GetEmailBody($user, $password);
            } else {
                $email_body = $this->GetEmailBody($user);
            }

            $email_data = array('email' => $user,
                'subject' => 'Invitation',
                'text_body' => $email_body,
                'html_body' => str_replace('\n', '<br />', $email_body)
            );
            if ($this->central->send_email($email_data)) {            
          //  if ($this->central->send_email_to_file($email_data)) {
                return TRUE;
            } else {
                return TRUE;
            }
        } catch (Exception $ex) {
            return FALSE;
        }
    }

    private function Published() {
        try {
            $corrupt = false;
            $event_id = $this->central->getargs('hidden_id', $_POST, $corrupt);
            $form_id = $this->central->getargs('send_form_id', $_POST, $corrupt);
            $event_id = Central::base64url_decode($event_id);
            $send_event_emails = $this->central->getargs('SendEvent', $_POST, $corrupt);
            $user_id = $_SESSION['user']['user_id'];
            $forms = $_POST['send_form_id'];
            $array = explode(",", $forms);

            foreach ($send_event_emails as $send_event_email) {
                $users = PluSQL::from($this->profile)->user->select('*')->where("email='$send_event_email'")->run()->user;
                foreach ($users as $user) {
                    $random_user_id = $user->user_id;
                    foreach ($array as $form) {
                        $invitaion = array(
                            'user_id' => $random_user_id,
                            'form_id' => $form,
                            'event_id' => $event_id,
                            'status' => '0'
                        );

                        Plusql::into($this->profile)->invitaion($invitaion)->insert();
                    }
                }
            }
            if (!$corrupt) {
                Plusql::on($this->profile)->event_form_data(array('published' => 1, 'modified_at' => date('Y-m-d H:i:sa')))->where("event_id='$event_id'")->update();
                Plusql::on($this->profile)->event(array('published' => 1, 'modified_at' => date('Y-m-d H:i:sa')))->where("event_id='$event_id'")->update();
                return TRUE;
            }
        } catch (Exception $ex) {
            return FALSE;
        }
    }

    private function GetEmailBody($user, $password = 0) {
        if ($password) {
            $email_body = "<p style='font:normal 18px Arial,serif'>"
                    . "Evetn Invitation:</p>
                           Dear Parents: <br />
                           we have organized a small event, and want a confirmation that will you send your child in that Event . Following link is of Android Base application.<br />
                           Please Download the application, install and login in to your account, and send us your confirmation.<br/>
                           Application link :-  <a href='http://app.paperless.com/'> Paperless </a> <br/> <br/>
                           <strong> Login Credentials:- </strong> <br />
                           <span> Email :- <span>$user <br />
                           <span> Password :- </span> $password <br /> <br />
                           Sincerely, \n Paperless</p>";
        } else {
            $email_body = "<p style='font:normal 18px Arial,serif'>"
                    . "Evetn Invitation:</p>
                        Dear Parents: <br />
                        we have organized a small event, and want a confirmation that will you send your child in that Event . Please Confirm us from Andriod base application by filling up form.<br/>
                        Sincerely, \n Paperless </p>";
        }
        return $email_body;
    }

    private function AddNewUser($user) {
        try {
            $bcrypt = new \Config\Bcrypt();
            $password = $this->central->create_password_randomly();
            $newuser = array('user_name' => 'random', 'email' => $user, 'password' => $bcrypt->hash($password),
                'school' => 'dummy', 'is_admin' => 0, 'status' => 'active', 'created_by' => $_SESSION['user']['user_id'], 'profile_pic' => 'default.png');
            Plusql::into($this->profile)->user($newuser)->insert();
            return array('password' => $password);
        } catch (Exception $ex) {
            return false;
        }
    }

    private function IsAlready($user) {
        try {
            PluSQL::from($this->profile)->user->select('*')->where("email='$user'")->run()->user;
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    private function Response($exception = 0) {
        try {
            $corrupt = false;
            $ajax = $this->central->getargs('ajax', $_POST, $corrupt);
            if (!$corrupt) {
                if ($exception) {
                    die(json_encode(array('success' => 0, 'error' => 1)));
                } else {
                    die(json_encode(array('success' => 1, 'error' => 0)));
                }
            } else {
                @header('Location:?r=Events');
            }
        } catch (Exception $ex) {
            
        }
    }

    private function UpdateAndNewEvent($havetosend = 0) {
        try {
            $corrupt = false;
            if (isset($_SESSION['user']['user_id'])) {
                $update = $this->central->getargs('update_event', $_POST, $corrupt);
                if ($this->central->getargs('hidden_id', $_POST, $corrupt) !== '' && !$update) {

                    try {

                        $corrupt = false;
                        $eventdata['modified_at'] = date('Y-m-d H:i:sa');
                        $event_id = $this->central->getargs('hidden_id', $_POST, $corrupt);
                        $form_id = $this->central->getargs('form_ids', $_POST, $corrupt);
                        $user_id = $_SESSION['user']['user_id'];

                        if (!$corrupt) {
                            $send_event_emails = $this->central->getargs('SaveAndSend', $_POST, $corrupt);
                            $emails = explode(",", $send_event_emails);
                            $user_id = $_SESSION['user']['user_id'];
                            $forms = $_POST['form_ids'];
                            $array = explode(",", $forms);

                            foreach ($emails as $email) {
                                $users = PluSQL::from($this->profile)->user->select('*')->where("email='$email'")->run()->user;
                                foreach ($users as $user) {
                                    $random_user_id = $user->user_id;
                                    foreach ($array as $form) {
                                        $invitaion = array(
                                            'user_id' => $random_user_id,
                                            'form_id' => $form,
                                            'event_id' => $event_id,
                                            'status' => '0'
                                        );

                                        Plusql::into($this->profile)->invitaion($invitaion)->insert();
                                        Plusql::on($this->profile)->event_form_data(array('published' => '1'))->where("event_id='$event_id'")->update();
                                        Plusql::on($this->profile)->event(array('published' => '1'))->where("event_id='$event_id'")->update();
                                        $_SESSION['event']['updated'] = 1;
                                        $url = '?r=Events';
                                        @header("Location:$url");
                                    }
                                }
                            }
                            $_SESSION['event']['updated'] = 1;
                            $url = '?r=Events';
                            @header("Location:$url");
                        }
                    } catch (Exception $ex) {
                        
                    }
                }
                $corrupt = false;
                $user_id = $_SESSION['user']['user_id'];
                $name = $this->central->getargs('event_name', $_POST, $corrupt);
                $type = $this->central->getargs('event_type', $_POST, $corrupt);
                $start_date = $this->central->getargs('start_date', $_POST, $corrupt);
                $start_date = date("Y-m-d", strtotime($start_date));
                $end_date = $this->central->getargs('end_date', $_POST, $corrupt);
                $end_date = date("Y-m-d", strtotime($end_date));
                $start_time = $this->central->getargs('start_time', $_POST, $corrupt);
                $end_time = $this->central->getargs('end_time', $_POST, $corrupt);
                $venue = $this->central->getargs('event_venue', $_POST, $corrupt);
                $des = $this->central->getargs('event_des', $_POST, $corrupt);
                $form_ides = $this->central->getargs('form_ids', $_POST, $corrupt);
                $form_ids = explode(",", $form_ides);
                foreach ($form_ids as $form_id) {
                    $eventdata = array(
                        'user_id' => $user_id, 'name' => $name, 'type' => $type, 'start_date' => $start_date, 'end_date' => $end_date,
                        'start_time' => $start_time, 'end_time' => $end_time, 'venue' => $venue, 'description' => $des,
                        'published' => 0, 'form_id' => $form_id
                    );

                    if (isset($_FILES['event_image'])) {
                        $image_extension = strtolower(end(explode(".", $_FILES["event_image"]["name"])));
                        if ($image_extension == "jpg" || $image_extension == "png" || $image_extension == "jpeg") {
                            $image_name = sha1(time()) . "." . $image_extension;
                            $target_file = 'packages/eventimg/' . $image_name;                            
                            move_uploaded_file($_FILES["event_image"]["tmp_name"], $target_file);
                            $eventdata['image'] = $image_name;
                        } else {
                            $_SESSION['EventPicEror'] = 1;
                        }
                    }
                    try {
                        
                        $update = $this->central->getargs('update_event', $_POST, $corrupt);
                    } catch (Exception $ex) {
                        $update = false;
                    }
                    if ($update) {
                        try {

                            $corrupt = false;
                            $eventdata['modified_at'] = date('Y-m-d H:i:sa');
                            $event_id = $this->central->getargs('hidden_id', $_POST, $corrupt);
                            if (!$corrupt) {                                
                                Plusql::on($this->profile)->event($eventdata)->where("event_id='$event_id'")->update();

                                $_SESSION['event']['updated'] = 1;
                                $url = '?r=Events&EditEvent=MQ&' . Central::base64url_encode('event_id') . '=' . Central::base64url_encode($event_id);                                
                                @header("Location:$url");
                            }
                        } catch (Exception $ex) {
                            echo $ex->getMessage();
                        }
                    }
                }
            } else {
                @header('Location:?r = Login');
            }
        } catch (Exception $ex) {
            
        }
    }

    function AddNewEvent() {
        if (isset($_SESSION['user']['user_id'])) {
            $corrupt = false;
            $user_id = $_SESSION['user']['user_id'];
            $name = $this->central->getargs('event_name', $_POST, $corrupt);
            $type = $this->central->getargs('event_type', $_POST, $corrupt);
            $start_date = $this->central->getargs('start_date', $_POST, $corrupt);
            $start_date = date("Y-m-d", strtotime($start_date));
            $end_date = $this->central->getargs('end_date', $_POST, $corrupt);
            $end_date = date("Y-m-d", strtotime($end_date));
            $start_time = $this->central->getargs('start_time', $_POST, $corrupt);
            $end_time = $this->central->getargs('end_time', $_POST, $corrupt);
            $venue = $this->central->getargs('event_venue', $_POST, $corrupt);
            $des = $this->central->getargs('event_des', $_POST, $corrupt);
            $form_id = $this->central->getargs('form_ids', $_POST, $corrupt);

            $eventdata = array(
                'user_id' => $user_id, 'name' => $name, 'type' => $type, 'start_date' => $start_date, 'end_date' => $end_date,
                'start_time' => $start_time, 'end_time' => $end_time, 'venue' => $venue, 'description' => $des,
                'published' => 0, 'form_id' => $form_id
            );
            if (isset($_FILES['event_image'])) {
                $image_extension = strtolower(end(explode(".", $_FILES["event_image"]["name"])));
                if ($image_extension == "jpg" || $image_extension == "png" || $image_extension == "jpeg") {
                    $image_name = sha1(time()) . "." . $image_extension;
                    $target_file = 'packages/eventimg/' . $image_name;
                    move_uploaded_file($_FILES["event_image"]["tmp_name"], $target_file);
                    $eventdata['image'] = $image_name;
                } else {
                    $_SESSION['EventPicEror'] = 1;
                }
            }
            if (!$corrupt) {
                $eventdata['created_at'] = date('Y-m-d H:i:sa');
                if ($_POST['new_event']) {
                    $datas = Plusql::into($this->profile)->event($eventdata)->insert();
                    $this->AddNewEvents();
                    $url = '?r=Events';
                    @header("Location:$url");
                } else {

                    $datas = Plusql::into($this->profile)->event($eventdata)->insert();
                    $this->AddNewEvents();
                    $url = '?r=Events';
                    @header("Location:$url");
                }
            }
        }
    }

    function AddNewEvents() {
        if (isset($_SESSION['user']['user_id'])) {
            $corrupt = false;
            $user_id = $_SESSION['user']['user_id'];
            $name = $this->central->getargs('event_name', $_POST, $corrupt);
            $type = $this->central->getargs('event_type', $_POST, $corrupt);
            $start_date = $this->central->getargs('start_date', $_POST, $corrupt);
            $start_date = date("Y-m-d", strtotime($start_date));
            $end_date = $this->central->getargs('end_date', $_POST, $corrupt);
            $end_date = date("Y-m-d", strtotime($end_date));
            $start_time = $this->central->getargs('start_time', $_POST, $corrupt);
            $end_time = $this->central->getargs('end_time', $_POST, $corrupt);
            $venue = $this->central->getargs('event_venue', $_POST, $corrupt);
            $des = $this->central->getargs('event_des', $_POST, $corrupt);
            $form_ides = $this->central->getargs('form_ids', $_POST, $corrupt);
            $form_ids = explode(",", $form_ides);
            $event_ides = PluSQL::from($this->profile)->event->select('*')->run()->event;
            $last_event_id = $this->central->get_accurate_last_id('event', $this->profile);
            foreach ($form_ids as $form_id) {

                $eventdata = array(
                    'user_id' => $user_id, 'name' => $name, 'type' => $type, 'start_date' => $start_date, 'end_date' => $end_date,
                    'start_time' => $start_time, 'end_time' => $end_time, 'venue' => $venue, 'description' => $des,
                    'published' => 0, 'form_id' => $form_id, 'event_id' => $last_event_id
                );
                if (isset($_FILES['event_image'])) {
                    $image_extension = strtolower(end(explode(".", $_FILES["event_image"]["name"])));
                    if ($image_extension == "jpg" || $image_extension == "png" || $image_extension == "jpeg") {
                        $image_name = sha1(time()) . "." . $image_extension;
                        $target_file = 'packages/eventimg/' . $image_name;
                        move_uploaded_file($_FILES["event_image"]["tmp_name"], $target_file);
                        $eventdata['image'] = $image_name;
                    } else {
                        $_SESSION['EventPicEror'] = 1;
                    }
                }
                if (!$corrupt) {
                    $eventdata['created_at'] = date('Y-m-d H:i:sa');
                    if ($_POST['new_event']) {
                        $datas = Plusql::into($this->profile)->event_form_data($eventdata)->insert();

                        $event_ides = PluSQL::from($this->profile)->event_form_data->select('*')->where("form_id = $form_id AND published <> 1")->run()->event_form_data;
                        foreach ($event_ides as $event_ids) {
                            $event_id = $event_ids->event_id;
                        }
                        $url = '?r=Events';
                        @header("Location:$url");
                    } else {

                        $datas = Plusql::into($this->profile)->event_form_data($eventdata)->insert();
                        $event_ides = PluSQL::from($this->profile)->event_form_data->select('*')->where("form_id = $form_id AND published <> 1")->run()->event_form_data;
                        foreach ($event_ides as $event_ids) {
                            $event_id = $event_ids->event_id;
                        }
//			$url = '?r=Events';
//			@header( "Location:$url" );
                    }

                    $return = array('success' => 1, 'error' => 0, 'data' => $event_id, 'message' => "Inserted");
                } else {
                    $return = array('success' => 0, 'error' => 1, 'message' => "Insertion-Err");
                }
            }
        }
        die(json_encode($return));
    }

}
