<?php

use \Config\Constants;
use \Config\Central;

class FormAction implements RocketSled\Runnable
{

    //--private members
    private $profile = 'db';
    private $central;

    public function __construct()
    {
	try
	{
	    @session_start();
	    $this->central = Central::instance();
	    $this->central->set_alias_connection( $this->profile );
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    public function run()
    {
	try
	{
	    $this->update_main_contents();
	}
	catch ( Exception $ex )
	{
	    
	}
    }

    private function update_main_contents()
    {
	try
	{
	    if ( isset( $_POST[ 'formfields' ] ) )
	    {
		$this->CreateForm();
	    }
	    else if ( isset( $_POST[ 'delete_from' ] ) )
	    {
		$this->DeleteForm();
	    }
	    else if ( isset( $_POST[ 'get_form' ] ) )
	    {
		$this->GetForm();
	    }
	}
	catch ( Exception $ex )
	{
	    
	}
    }

    private function GetForm()
    {
	try
	{
	    $corrupt = false;
	    $user_id = $_SESSION[ 'user' ][ 'user_id' ];
	    $form_id = $this->central->getargs( 'get_form', $_POST, $corrupt );
	    if ( !$corrupt )
	    {
		$formdata = PluSQL::from( $this->profile )->form->select( '*' )->where( "user_id='$user_id' AND form_id='$form_id'" )->limit( '0,1' )->run()->form;
		$response = array( 'form_data' => unserialize( $formdata->form_fields ), 'success' => 1 );
	    }
	    else
	    {
		$response = array( 'form_data' => '', 'success' => 0 );
	    }
	}
	catch ( Exception $ex )
	{
	    $response = array( 'form_data' => '', 'success' => 0 );
	}
	die( json_encode( $response ) );
    }

    private function DeleteForm()
    {
	try
	{
	    $corrupt = false;
	    $user_id = $_SESSION[ 'user' ][ 'user_id' ];
	    $form_id = $this->central->getargs( 'delete_from', $_POST, $corrupt );
	    if ( !$corrupt )
	    {
		PluSQL::on( $this->profile )->form( array( 'deleted' => 1 ) )->where( "user_id='$user_id' AND form_id=$form_id" )->update();
		$response = array( 'success' => 1 );
	    }
	    else
		$response = array( 'success' => 0 );
	}
	catch ( Exception $ex )
	{
	    $response = array( 'success' => 0 );
	}
	die( json_encode( $response ) );
    }

    private function CreateForm()
    {
	try
	{
	    $corrupt = false;

	    $formfields = $this->central->getargs( 'formfields', $_POST, $corrupt );
	    $name = $this->central->getargs( 'name', $_POST, $corrupt );
	    $created_at = date( 'Y-m-d' );
	    $user_id = $_SESSION[ 'user' ][ 'user_id' ];
	    if ( !$corrupt )
	    {
		$form = array( 'user_id' => $user_id, 'name' => $name, 'created_at' => $created_at, 'form_fields' => serialize( json_decode( $formfields ) ) );
		PluSQL::into( $this->profile )->form( $form )->insert();
		$_SESSION[ 'formcreated' ] = 1;
		header( 'Location:?r=Forms' );
	    }
	}
	catch ( Exception $ex )
	{
	    
	}
    }

}
