<?php

use \Config\Constants;
use \Config\Central;

class Users extends Config\RSBase {

//--private members
    private $file_name = "users.html";

//--constructor
    public function __construct() {
        try {
            parent::__construct();
            $this->template = $this->central->load_normal($this->file_name);
            $this->central->populate_user_contents($this->template);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function update_main_contents() {
        try {
            $this->template->setValue('#home@href', '?r=Dashboard');
            $this->template->query('#manage_users')->item(0)->setAttribute('class', 'treeview active');
            $this->PopulateUsers();
        } catch (Exception $ex) {
            
        }
    }

    private function PopulateUsers() {
        try {
            $cnt = 0;
            $item = $this->template->repeat('.rep_users');
            $users = PluSQL::from($this->profile)->user->select('*')->where("status='active' AND is_admin <> '1' AND created_by ={$_SESSION['user']['user_id']}")->run()->user;
            foreach ($users as $user) {
                $cnt++;
                $item->setValue('#name', $user->user_name);
                $item->setValue('#email', $user->email);
                $item->setValue('#status', ucfirst($user->status));
                $item->setValue('#cnt', $cnt);
                $item->next();
            }
            Central::remove_last_repeating_element($this->template, '#stop_users', 1, 2, 0);
            $this->template->remove('#stop_users');
        } catch (Exception $ex) {
            $this->template->setValue('.rep_users', "<td></td><td>No matching records found</td><td></td><td></td>", 1);
            $this->template->remove('#stop_users');
        }
    }

}
