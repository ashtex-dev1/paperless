<?php

use \Config\Constants;
use \Config\Central;

class Forms extends Config\RSBase {

    public function __construct() {
        try {
            parent::__construct();
            if (isset($_GET['NewForm'])) {
                $this->LoadTemplate('new_form');
                $this->LoadNewForm();
            } else {
                $this->LoadTemplate('forms');
                $this->LoadForm();
            }
        } catch (Exception $e) {
            
        }
    }

    public function update_main_contents() {
        try {
            
        } catch (Exception $ex) {
            
        }
    }

    private function LoadForm() {
        try {
            $this->MarkActive('forms');
            $this->ShowFormsPageMessages();
            $this->ShowFormsPageOthers();
            $this->PopulateForms();
        } catch (Exception $ex) {
            
        }
    }

    private function LoadNewForm() {
        try {
            $this->MarkActive('new_form');
            $this->ShowNewFormsOthers();
        } catch (Exception $ex) {
            
        }
    }

    private function ShowNewFormsOthers() {
        try {
            $this->template->setValue('#home@href', '?r=Dashboard');
            $this->template->setValue('#create_form@action', '?r=FormAction');
        } catch (Exception $ex) {
            
        }
    }

    private function ShowFormsPageOthers() {
        try {
            $this->template->setValue('#home@href', '?r=Dashboard');
        } catch (Exception $ex) {
            
        }
    }

    private function MarkActive($id) {
        try {
            $this->template->query('#form_menu')->item(0)->setAttribute('class', 'treeview active');
            $this->template->query("#form_menu/ul/li#$id")->item(0)->setAttribute('class', 'active');
        } catch (Exception $ex) {
            
        }
    }

    private function LoadTemplate($file) {
        try {
            $this->template = $this->central->load_normal("$file.html");
            $this->central->populate_user_contents($this->template);
        } catch (Exception $ex) {
            
        }
    }

    private function PopulateForms() {
        try {
            $cnt = 0;
            $user_id = $_SESSION['user']['user_id'];
            $forms = PluSQL::from($this->profile)->form->select('*')->where("user_id = '$user_id' AND deleted <> 1")->orderBy('name ASC')->run()->form;
            $item = $this->template->repeat('.rep_forms');
            foreach ($forms as $form) {
                $cnt++;
                $item->setValue('#cnt', $cnt);
                $item->setValue('#hidden_id@value', $form->form_id);
                $item->setValue('#name', $form->name);
                $item->setValue('#created_at', $form->created_at);
                $item->next();
            }
            Central::remove_last_repeating_element($this->template, '#stop_forms', 1, 2, 0);
            $this->template->remove('#stop_forms');
        } catch (Exception $ex) {
            $this->template->setValue('.rep_forms', "<td></td><td>No matching records found</td><td></td><td></td><td></td>", 1);
            $this->template->remove('#stop_forms');
        }
    }

    private function ShowFormsPageMessages() {
        try {
            if (isset($_SESSION['formcreated'])) {
                $this->template->setValue('.frmscs@style', 'display:block');
                $this->template->setValue('#frmmsg', 'New form has been successfully created.');
                unset($_SESSION['formcreated']);
            }
        } catch (Exception $ex) {
            
        }
    }

}

?>