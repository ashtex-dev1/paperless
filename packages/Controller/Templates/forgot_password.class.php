<?php

@session_start();

use \Config\Constants;
use \Config\Central;

class ForgotPassword implements RocketSled\Runnable {

    //--private members
    private $file_name = "forgot_pas.html";
    private $profile = "forgot_pas";
    private $template;
    private $central;

    //--constructor
    public function __construct() {
        try {
            $this->central = Central::instance();
            $this->central->set_alias_connection($this->profile);
            unset($_SESSION['reset_pass_div']);
            try {
                if (isset($_GET[Central::base64url_encode('temp_link')])) {
                    $istmplink = $this->IsThere();
                    if ($istmplink) {
                        $_SESSION['reset_pass_div'] = 1;
                        $this->file_name = "forgot_pas.html";
                    } else {
                        $_SESSION['token_expire'] = 1;
                        unset($_SESSION['reset_pass_div']);
                    }
                }
            } catch (Exception $ex) {
                $_SESSION['token_expire'] = 1;
                unset($_SESSION['reset_pass_div']);
            }

            $this->template = $this->central->load_normal($this->file_name);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function run() {
        try {
            $this->update_main_contents();
            $this->central->render($this->template);
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function update_main_contents() {
        try {
            $this->template->setValue('#forgot_pas_form@action', '?r=AuthAction');
            if (isset($_SESSION['Smthng'])) {
                $this->ShowSmthngEror();
            } else if (isset($_SESSION['ForgotPassEmail'])) {
                $this->ShowForgotPassEror();
            } else if (isset($_SESSION['email_success'])) {
                $this->ShowEmailedScs();
            } else if (isset($_SESSION['reset_pass_div'])) {
                $this->ShowResetPassFields();
                if (isset($_SESSION['UpdatePasSmthng'])) {
                    $this->ShowUpdatePasSmthngEror();
                } else if (isset($_SESSION['UpdateAgainPass'])) {
                    $this->ShowUpdateAgainPassEror();
                }
            } else if (isset($_SESSION['token_expire'])) {
                $this->ShowTokenExpireEror();
            }
        } catch (Exception $ex) {
            
        }
    }

    private function ShowSmthngEror() {
        try {
            $this->template->setValue('.alert-success@style', 'display:none');
            $this->template->setValue('.alert-danger@style', 'display:block');
            $this->template->setValue('#forgot_eror', '<i class="fa fa-warning"> </i> Something went wrong, Try again later', 1);
            unset($_SESSION['Smthng']);
            unset($_SESSION['reset_pass_div']);
        } catch (Exception $ex) {
            
        }
    }

    private function ShowForgotPassEror() {
        try {
            $this->template->setValue('.alert-success@style', 'display:none');
            $this->template->setValue('.alert-danger@style', 'display:block');
            $this->template->setValue('#forgot_eror', '<i class="fa fa-warning"> </i> Entered email does not exist in our record.', 1);
            $this->template->setValue('#email@value', $_SESSION['ForgotPassEmailAdress']);
            unset($_SESSION['ForgotPassEmail']);
            unset($_SESSION['ForgotPassEmailAdress']);
            unset($_SESSION['reset_pass_div']);
        } catch (Exception $ex) {
            
        }
    }

    private function ShowEmailedScs() {
        try {
            $this->template->setValue('.alert-success@style', 'display:block');
            $this->template->setValue('.alert-danger@style', 'display:none');
            unset($_SESSION['email_success']);
            unset($_SESSION['reset_pass_div']);
        } catch (Exception $ex) {
            
        }
    }

    private function ShowTokenExpireEror() {
        try {
            $this->template->setValue('.alert-success@style', 'display:none');
            $this->template->setValue('.alert-danger@style', 'display:block');
            $this->template->setValue('#forgot_eror', '<i class="fa fa-warning"> </i> Sorry! Your token has been expired, So please enter your email again for reset password.', 1);
            unset($_SESSION['token_expire']);
        } catch (Exception $ex) {
            
        }
    }

    private function ShowUpdatePasSmthngEror() {
        try {
            $this->template->setValue('.alert-success@style', 'display:none');
            $this->template->setValue('.alert-danger@style', 'display:block');
            $this->template->setValue('#forgot_eror', '<i class="fa fa-warning"> </i> Something went wrong, Try again later.', 1);
            unset($_SESSION['UpdatePasSmthng']);
        } catch (Exception $ex) {
            
        }
    }

    private function IsThere() {
        try {
            if (isset($_GET[Central::base64url_encode('temp_link')])) {
                $tmplink = $_GET[Central::base64url_encode('temp_link')];
                $user = Plusql::from($this->profile)->user->select("*")->where("temp_link = '{$tmplink}'")->limit('0,1')->run()->user;
                if ($user) {
                    return TRUE;
                }
            } else {
                return FALSE;
            }
        } catch (Exception $ex) {
            return FALSE;
        }
    }

    private function ShowUpdateAgainPassEror() {
        try {
            $this->template->setValue('.alert-success@style', 'display:none');
            $this->template->setValue('.alert-danger@style', 'display:block');
            $this->template->setValue('#forgot_eror', '<i class="fa fa-warning"> </i> Password does not match.', 1);
            unset($_SESSION['UpdateAgainPass']);
        } catch (Exception $ex) {
            
        }
    }

    private function ShowResetPassFields() {
        try {
            $this->template->setValue('#forgot_pass_div@style', 'display:block');
            $this->template->setValue('#forgot_pass_email_div@style', 'display:none');
            $this->template->setValue('#tmplink@value', $_GET[Central::base64url_encode('temp_link')]);
            $this->template->setValue('#forgot_pas_field_form@action', '?r=AuthAction');
        } catch (Exception $ex) {
            
        }
    }

}

?>