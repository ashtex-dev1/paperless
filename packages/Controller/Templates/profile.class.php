<?php

use \Config\Constants;
use \Config\Central;

class Profile extends Config\RSBase {

    //--private members
    private $file_name = "profile.html";

    //--constructor
    public function __construct() {
        try {
            parent::__construct();
            $this->template = $this->central->load_normal($this->file_name);
            $this->central->populate_user_contents($this->template);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function update_main_contents() {
        try {
            $this->template->setValue('#home@href', '?r=Dashboard');
            if (isset($_POST)) {
                $this->HandlePost();
            }
            $this->ProfilePic();
            $this->PopulateUserData();
            $this->ShowMessage();
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    private function ProfilePic() {
        try {
            $this->central->populate_user_contents($this->template, 1);
        } catch (Exception $ex) {
            
        }
    }

    private function PopulateUserData() {
        try {
            $user_id = $_SESSION['user']['user_id'];
            $users = PluSQL::from($this->profile)->user->select('*')->where("user_id='$user_id'")->run()->user;
            foreach ($users as $user) {
                $this->template->setValue('#name@value', $user->user_name);
                $this->template->setValue('#email@value', $user->email);
                $this->template->setValue('#schol@value', $user->school);
            }
        } catch (Exception $ex) {
            
        }
    }

    private function HandlePost() {
        try {
            $bcrypt = new \Config\Bcrypt();
            if (isset($_POST['update_profile_pic'])) {
                $this->UpdateProfilePic();
            } else if (isset($_POST['update_profile'])) {
                $this->UpdateProfile($bcrypt);
            }
        } catch (Exception $ex) {
            
        }
    }

    private function ShowMessage() {
        try {
            if (isset($_SESSION['ProfilePicUpdate'])) {
                $this->ProfilePicUpdated();
            } else if (isset($_SESSION['ProfilePicUpdateror'])) {
                $this->ProfilePicUpdatedEror();
            } else if (isset($_SESSION['ProfileUpdated'])) {
                $this->ProfileUpdated();
            } else if (isset($_SESSION['Smthng'])) {
                $this->SmthngHappened();
            }
        } catch (Exception $ex) {
            
        }
    }

    private function UpdateProfile($bcrypt) {
        try {
            $user_id = $_SESSION['user']['user_id'];
            $user_name = $this->central->getargs('name', $_POST, $corrupt);
            $email = $this->central->getargs('email', $_POST, $corrupt);
            $schol = $this->central->getargs('schol', $_POST, $corrupt);
            $corrupt = false;
            $password = $this->central->getargs('pas', $_POST, $corrupt);
            if ($corrupt) {
                $profile_data = array('user_name' => $user_name, 'email' => $email, 'school' => $schol);
            } else if (!$corrupt) {
                $password = $bcrypt->hash($password);
                $profile_data = array('user_name' => $user_name, 'email' => $email, 'school' => $schol, 'password' => $password);
                $_SESSION['ProfileUpdatedPass'] = 1;
            }
            Plusql::on($this->profile)->user($profile_data)->where("user_id='$user_id'")->update();
            $_SESSION['ProfileUpdated'] = 1;
        } catch (Exception $ex) {
            $_SESSION['Smthng'] = 1;
        }
    }

    private function UpdateProfilePic() {
        try {
            $user_id = $_SESSION['user']['user_id'];
            if (isset($_FILES['profile_pic']['tmp_name'])) {
                $image_extension = strtolower(end(explode(".", $_FILES["profile_pic"]["name"])));
                if ($image_extension == "jpg" || $image_extension == "png" || $image_extension == "jpeg") {
                    $image_name = sha1(time()) . "." . $image_extension;
                    $target_file = 'packages/profilepics/' . $image_name;
                    move_uploaded_file($_FILES["profile_pic"]["tmp_name"], $target_file);
                    $profilepic = $image_name;
                    Plusql::on($this->profile)->user(array('profile_pic' => $profilepic))->where("user_id='$user_id'")->update();
                    $_SESSION['ProfilePicUpdate'] = 1;
                } else {
                    $_SESSION['ProfilePicUpdateror'] = 1;
                }
            }
        } catch (Exception $ex) {
            $_SESSION['ProfilePicUpdateror'] = 1;
        }
    }

    private function ProfilePicUpdated() {
        try {
            $this->template->setValue('.pic_updated@style', 'display:block');
            unset($_SESSION['ProfilePicUpdate']);
        } catch (Exception $ex) {
            
        }
    }

    private function ProfilePicUpdatedEror() {
        try {
            $this->template->setValue('.pic_eror@style', 'display:block');
            unset($_SESSION['ProfilePicUpdateror']);
        } catch (Exception $ex) {
            
        }
    }

    private function ProfileUpdated() {
        try {
            $this->template->setValue('.profile_updated@style', 'display:block');
            $this->template->setValue('#profile_updated', 'Your profile has successfully updated.');
            unset($_SESSION['ProfileUpdated']);
            if (isset($_SESSION['ProfileUpdatedPass'])) {
                session_destroy();
            }
        } catch (Exception $ex) {
            
        }
    }

    private function SmthngHappened() {
        try {
            $this->template->setValue('.alert-danger', 'Something went wrong, Try again later.');
            unset($_SESSION['Smthng']);
        } catch (Exception $ex) {
            
        }
    }

}

?>