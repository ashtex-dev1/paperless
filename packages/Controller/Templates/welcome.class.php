<?php

@session_start();

use \Config\Constants;
use \Config\Central;

class Welcome implements RocketSled\Runnable
{

    //--private members
    private $file_name = "welcome.html";
    private $profile = "welcome";
    private $template;
    private $central;

    //--constructor
    public function __construct()
    {
	try
	{
	    $this->central = Central::instance();
	    $this->central->set_alias_connection( $this->profile );
	    $this->template = $this->central->load_normal( $this->file_name );
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    public function run()
    {
	try
	{
	    if ( !$this->central->check_user_login_status( $this->profile ) )
	    {
		$this->render();
	    }
	    else
	    {
		@header( 'location: ?r=Dashboard' );
	    }
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

    private function render()
    {
	try
	{
	    $this->central->render( $this->template );
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

    public function update_main_contents()
    {
	try
	{
	    
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

}
