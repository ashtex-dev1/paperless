<?php

@session_start();

use \Config\Constants;
use \Config\Central;

class Signup implements RocketSled\Runnable {

    //--private members
    private $file_name = "register.html";
    private $profile = "register";
    private $template;
    private $central;

    //--constructor
    public function __construct() {
        try {
            $this->central = Central::instance();
            $this->central->set_alias_connection($this->profile);
            $this->template = $this->central->load_normal($this->file_name);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function run() {
        try {
            $this->template->setValue('#already_resitered@href', '?r=Login');
            $this->template->setValue('#register_form@action', '?r=AuthAction');
            if (isset($_SESSION['DuplicateInRegister'])) {
                $this->setOtherValues();
                $this->template->setValue('.email_eror@style', 'display:block');
                $this->template->setValue('.email_eror', '<i class="fa fa-warning"> </i> Sorry! email already exist.', 1);
                unset($_SESSION['DuplicateInRegister']);
            } else if (isset($_SESSION['ProfilePicEror'])) {
                $this->setOtherValues();
                $this->template->setValue('.alert-danger@style', 'display:block');
                unset($_SESSION['ProfilePicEror']);
            }
            $this->central->render($this->template);
        } catch (Exception $ex) {
            
        }
    }

    private function setOtherValues() {
        try {
            $this->template->setValue('#email@value', $_SESSION['EmailInRegister']);
            $this->template->setValue('#name@value', $_SESSION['NameInRegister']);
            $this->template->setValue('#pas@value', $_SESSION['PassInRegister']);
            $this->template->setValue('#again_pas@value', $_SESSION['PassInRegister']);
            $this->template->setValue('#schol@value', $_SESSION['SchoolInRegister']);
            unset($_SESSION['EmailInRegister']);
            unset($_SESSION['NameInRegister']);
            unset($_SESSION['PassInRegister']);
            unset($_SESSION['SchoolInRegister']);
        } catch (Exception $ex) {
            
        }
    }

    public function update_main_contents() {
        try {
            
        } catch (Exception $ex) {
            
        }
    }

}

?>