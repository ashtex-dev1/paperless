$(document).ready(function ()
{    
    var url = window.location.href;     
    if ( url.substr(url.indexOf("&") + 1) === "error=1" ) 
    {       
	$('#duplicateEmailError').css('display','block');
    } 
    else
    {        
        $('duplicateEmailError').css('display','none');
    }
    if ( url.substr(url.indexOf("&") + 1) === "error=2" ) 
    {       
	$('#UnknownError').css('display','block');
    } 
    else
    {        
        $('UnknownError').css('display','none');
    }
    
    $('#validate_users').on('click', function ()
    {       
        var is_ok = false;
        var emails = '';                 
        if ($('#users').val() && isEmail($('#users').val().replace(/<\/?span[^>]*>/g, "")))
        {
            emails += $('#users').val().replace(/<\/?span[^>]*>/g, "") + ',';
            $('.allusers_eror').hide();
            is_ok = true;
        } else {
            $('.allusers_eror').show();
        }

        if (is_ok)
            $('#new_user').submit();
    });
});


function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}


