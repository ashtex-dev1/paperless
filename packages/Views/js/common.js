jQuery(document).ready(function () {
    $(".submenu > a").click(function (e) {
        e.preventDefault();
        var $li = $(this).parent("li");
        var $ul = $(this).next("ul");

        if ($li.hasClass("open")) {
            $ul.slideUp(350);
            $li.removeClass("open");
        } else {
            $(".nav > li > ul").slideUp(350);
            $(".nav > li").removeClass("open");
            $ul.slideDown(350);
            $li.addClass("open");
        }
    });
});

// function to validate form
function check_error(id, par)
{
    var all_ok = true;
    var node = (typeof par !== "undefined") ? $('#' + id, par) : $('#' + id);
    var error_node = (typeof par !== "undefined") ? $('.' + id + '_eror', par) : $('.' + id + '_eror');

    if ((node.val() === '' || node.val() === 0 || node.val() === '0' || (node.attr("type") === "radio" && !$('.' + id).is(":checked"))) && !node.is(':disabled') && node.is(':visible')) {
        node.addClass('error');
        error_node.show();
        all_ok = false;
    } else {
        node.removeClass('error');
        error_node.hide();
    }

    return all_ok;
}

// function to check numeric values
function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

// function for validating email
function validateEmail(id) {
    var email = $('#' + id).val();
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!re.test(email)) {
        $('#' + id).addClass('error');
        $('.' + id + '_eror').show();
    } else {
        $('#' + id).removeClass('error');
        $('.' + id + '_eror').hide();
    }
    return re.test(email);
}

// function to enable or disable nodes
function enable_disable_nodes(par, child, action) {
    $(par).find(child).each(function () {
        $(this).prop("disabled", action);
    });
}

// function to compare strings
function strcmp(str1, str2)
{
    return ((str1 == str2) ? 0 : ((str1 > str2) ? 1 : -1));
}

// function to serialze form
$.fn.serializeObject = function ()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

// function to get URL prams
function GetURLParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
    }
    return '';
}

// function for getting ordinals
function getGetOrdinal(n) {
    var s = ["th", "st", "nd", "rd"],
            v = n % 100;
    return n + (s[(v - 20) % 10] || s[v] || s[0]);
}

// function for submit form with ajax
function send_ajax(data, url, flag)
{
    var response = $.parseJSON($.ajax({
        url: url,
        type: "POST",
        data: data,
        dataType: "json",
        async: false
    }).responseText);
    if (typeof flag !== 'undefined' && flag)
    {
        if (response.success)
        {
            if (strcmp(response.message, '') !== 0)
            {
                $('#message #msg').html(response.message);
                $('#message').modal('show');
            }
            setTimeout(function () {
                $('#message').modal('hide');
            }, 1500);
        } else
        {
            if (strcmp(response.message, '') !== 0)
            {
                $('#message #msg').html(response.message);
                $('#message').modal('show');
            }
        }
    }
    return response;
}

// function for submiting form data object with ajax
function send_ajax_with_additional_prams(data, action)
{
    var response = $.parseJSON($.ajax({
        url: action,
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        data: data,
        dataType: "json",
        async: false
    }).responseText);
    return response;
}
function notify(intitle, intext, insticker, inhistory, intype, inopacity) {
    var classs = '';
    switch (intype) {
        case 'warning':
            classs = 'picon icon16 entypo-icon-warning white';
            break;
        case 'info':
            classs = 'picon icon16 brocco-icon-info white';
            break;
        case 'success':
            classs = 'picon icon16 iconic-icon-check-alt white';
            break;
        case 'error':
            classs = 'picon icon24 typ-icon-cancel white';
            break;
        default:
            classs = 'picon icon16 entypo-icon-info white';
            break;
    }
    $.pnotify({
        type: intype,
        title: intitle,
        text: intext,
        icon: classs,
        opacity: inopacity,
        sticker: insticker,
        history: inhistory
    });
}