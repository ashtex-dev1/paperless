$( document ).ready( function ( $ ) {
    var url = window.location.href;
    if ( url.indexOf( 'NewForm=MQ' ) !== -1 ) {
	NewFormjs( );
    } else if ( url.indexOf( '?r=Forms' ) !== -1 ) {
	Formsjs( );
    }

    function Formsjs( ) {
	$( 'body' ).delegate( '.delete_from, .get_form', 'click', function ( ) {
	    var cls = $( this ).attr( 'class' );
	    var tr = $( this ).parent( ).parent( );
	    var form_id = $( this ).parent( ).parent( ).find( '#hidden_id' ).val( );
	    if ( form_id && ( cls.indexOf( 'delete_from' ) !== -1 ) ) {
		DeleteForm( form_id, tr );
	    } else if ( form_id && ( cls.indexOf( 'get_form' ) !== -1 ) ) {
		GetForm( form_id );
	    }
	} );
    }

    function GetForm( form_id ) {
	var result = send_ajax( { ajax: 1, get_form: form_id }, '?r=FormAction', false );
	$( '.form-view' ).formRender( {
	    dataType: 'json',
	    formData: JSON.stringify( result.form_data )
	} );
	$( '#view_form' ).modal( 'show' );
    }

    function DeleteForm( form_id, tr ) {
	var result = send_ajax( { ajax: 1, delete_from: form_id }, '?r=FormAction', false );
	if ( result ) {
	    $( tr ).fadeOut( 1000 );
	}
    }

    function NewFormjs( ) {
	var buildWrap = document.querySelector( '.build-wrap' ),
		renderWrap = document.querySelector( '.render-wrap' ),
		editBtn = document.getElementById( 'edit-form' ),
		formData = window.sessionStorage.getItem( 'formData' ),
		editing = true,
		fbOptions = {
		    dataType: 'json'
		};
	if ( formData ) {
	    fbOptions.formData = JSON.parse( formData );
	}

	var toggleEdit = function ( ) {
	    document.body.classList.toggle( 'form-rendered', editing );
	    editing = !editing;
	};
	$( ".access-wrap" ).remove();
	var formBuilder = $( buildWrap ).formBuilder( fbOptions ).data( 'formBuilder' );
	$( '.form-builder-save' ).click( function ( ) {
	    all_ok = true;
	    var names = [ ];
	    $( 'input[name="name"]', $( 'ul.frmb.ui-sortable' ) ).each( function () {
		if ( $( this ).val( ) == '' || $( this ).val( ) == '0' || $( this ).val( ) == 'undefined' ) {
		    $( this ).closest( 'li.form-field' ).addClass( "has-error" );
		    all_ok = false;
		} else {
		    if ( $.inArray( $( this ).val( ), names ) < 0 ) {
			names.push( $( this ).val( ) );
			$( this ).closest( 'li.form-field' ).removeClass( "has-error" );
		    } else {
			$( this ).closest( 'li.form-field' ).addClass( "has-error" );
			all_ok = false;
		    }
		}
	    } );

	    if ( all_ok ) {
		toggleEdit( );
		$( renderWrap ).formRender( {
		    dataType: 'json',
		    formData: formBuilder.formData
		} );
		$( ".fld-name" ).closest( 'li.form-field' ).removeClass( "has-error" );
		$( '#formfields' ).val( formBuilder.formData );
		console.log( formBuilder.formData );
		window.sessionStorage.setItem( 'formData', JSON.stringify( formBuilder.formData ) );
	    }
	} );
	$( '.cb-wrap li.icon-file-input,.cb-wrap li.icon-hidden-input,.cb-wrap li.icon-autocomplete,.cb-wrap li.icon-button-input' ).remove( );
	editBtn.onclick = function ( ) {
	    toggleEdit( );
	};
	$( '#save-form' ).click( function ( ) {
	    $( '#formsmodal' ).modal( 'show' );
	} );
	$( '#create_form' ).submit( function ( ) {
	    var all_ok = true;
	    if ( !check_error( 'name' ) )
		all_ok = false;
	    if ( all_ok ) {
		window.sessionStorage.removeItem( 'formData' );
		return true;
	    }
	} );
    }
}
);
