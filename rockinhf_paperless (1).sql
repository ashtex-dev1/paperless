-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 18, 2017 at 08:00 AM
-- Server version: 5.6.32-78.1-log
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rockinhf_paperless`
--

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `event_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `form_id` varchar(11) NOT NULL,
  `name` text NOT NULL,
  `type` text NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `image` varchar(300) NOT NULL,
  `venue` text NOT NULL,
  `description` text NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `published` int(11) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`event_id`, `user_id`, `form_id`, `name`, `type`, `start_date`, `end_date`, `start_time`, `end_time`, `image`, `venue`, `description`, `created_at`, `modified_at`, `published`, `deleted`) VALUES
(1, 80, '3,2', 'Sports Gala', 'sport', '2017-09-01', '2017-09-30', '05:30:00', '05:30:00', '', 'the_palace_of_auburn_hills', 'Just a dummy entry', '2017-09-14 06:45:17', '2017-09-14 06:45:32', 1, 0),
(2, 80, '4', 'Event01', 'exhibition', '2017-09-19', '2017-09-20', '03:45:00', '05:30:00', '8a738f7bb1f9cf8532fce9e07e66af558b1fe902.jpg', 'scottrade_center', 'test ', '2017-09-15 03:54:45', '2017-09-15 03:57:04', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `event_form_data`
--

CREATE TABLE IF NOT EXISTS `event_form_data` (
  `event_form_data_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `type` text NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `image` varchar(500) NOT NULL,
  `venue` text NOT NULL,
  `description` text NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `published` int(50) NOT NULL,
  `deleted` int(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event_form_data`
--

INSERT INTO `event_form_data` (`event_form_data_id`, `user_id`, `event_id`, `form_id`, `name`, `type`, `start_date`, `end_date`, `start_time`, `end_time`, `image`, `venue`, `description`, `created_at`, `modified_at`, `published`, `deleted`) VALUES
(1, 80, 1, 3, 'Sports Gala', 'sport', '2017-09-01', '2017-09-30', '05:30:00', '05:30:00', '', 'the_palace_of_auburn_hills', 'Just a dummy entry', '2017-09-14 06:45:17', '2017-09-14 06:45:32', 1, 0),
(2, 80, 1, 2, 'Sports Gala', 'sport', '2017-09-01', '2017-09-30', '05:30:00', '05:30:00', '', 'the_palace_of_auburn_hills', 'Just a dummy entry', '2017-09-14 06:45:17', '2017-09-14 06:45:32', 1, 0),
(3, 80, 2, 4, 'Event01', 'exhibition', '2017-09-19', '2017-09-20', '03:45:00', '05:30:00', '8a738f7bb1f9cf8532fce9e07e66af558b1fe902.jpg', 'scottrade_center', 'test ', '2017-09-15 03:54:45', '2017-09-15 03:57:04', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `form`
--

CREATE TABLE IF NOT EXISTS `form` (
  `form_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `created_at` date NOT NULL,
  `form_fields` text NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `form`
--

INSERT INTO `form` (`form_id`, `user_id`, `name`, `created_at`, `form_fields`, `deleted`) VALUES
(1, 80, 'Dummy', '2017-09-13', 'a:2:{i:0;O:8:"stdClass":4:{s:4:"type";s:12:"autocomplete";s:5:"label";s:12:"Autocomplete";s:9:"className";s:12:"autocomplete";s:4:"name";s:10:"firstfield";}i:1;O:8:"stdClass":4:{s:4:"type";s:12:"autocomplete";s:5:"label";s:12:"Autocomplete";s:9:"className";s:12:"autocomplete";s:4:"name";s:11:"secondfield";}}', 1),
(2, 80, 'Dummy', '2017-09-13', 'a:8:{i:0;O:8:"stdClass":3:{s:4:"type";s:12:"autocomplete";s:5:"label";s:12:"Autocomplete";s:9:"className";s:12:"autocomplete";}i:1;O:8:"stdClass":3:{s:4:"type";s:8:"checkbox";s:5:"label";s:8:"Checkbox";s:9:"className";s:8:"checkbox";}i:2;O:8:"stdClass":4:{s:4:"type";s:14:"checkbox-group";s:5:"label";s:14:"Checkbox Group";s:9:"className";s:14:"checkbox-group";s:6:"values";a:3:{i:0;O:8:"stdClass":3:{s:5:"label";s:8:"Option 1";s:5:"value";s:8:"option-1";s:8:"selected";b:1;}i:1;O:8:"stdClass":2:{s:5:"label";s:8:"Option 2";s:5:"value";s:8:"option-2";}i:2;O:8:"stdClass":2:{s:5:"label";s:8:"Option 3";s:5:"value";s:8:"option-3";}}}i:3;O:8:"stdClass":3:{s:4:"type";s:4:"date";s:5:"label";s:10:"Date Field";s:9:"className";s:8:"calendar";}i:4;O:8:"stdClass":4:{s:4:"type";s:6:"select";s:5:"label";s:6:"Select";s:9:"className";s:12:"form-control";s:6:"values";a:3:{i:0;O:8:"stdClass":3:{s:5:"label";s:8:"Option 1";s:5:"value";s:8:"option-1";s:8:"selected";b:1;}i:1;O:8:"stdClass":2:{s:5:"label";s:8:"Option 2";s:5:"value";s:8:"option-2";}i:2;O:8:"stdClass":2:{s:5:"label";s:8:"Option 3";s:5:"value";s:8:"option-3";}}}i:5;O:8:"stdClass":4:{s:4:"type";s:4:"text";s:5:"label";s:10:"Text Field";s:7:"subtype";s:4:"text";s:9:"className";s:12:"form-control";}i:6;O:8:"stdClass":3:{s:4:"type";s:8:"textarea";s:5:"label";s:9:"Text Area";s:9:"className";s:12:"form-control";}i:7;O:8:"stdClass":4:{s:4:"type";s:11:"radio-group";s:5:"label";s:11:"Radio Group";s:9:"className";s:11:"radio-group";s:6:"values";a:3:{i:0;O:8:"stdClass":3:{s:5:"label";s:8:"Option 1";s:5:"value";s:8:"option-1";s:8:"selected";b:1;}i:1;O:8:"stdClass":2:{s:5:"label";s:8:"Option 2";s:5:"value";s:8:"option-2";}i:2;O:8:"stdClass":2:{s:5:"label";s:8:"Option 3";s:5:"value";s:8:"option-3";}}}}', 0),
(3, 80, 'Dummy Form', '2017-09-14', 'a:10:{i:0;O:8:"stdClass":3:{s:4:"type";s:8:"checkbox";s:5:"label";s:8:"Checkbox";s:9:"className";s:8:"checkbox";}i:1;O:8:"stdClass":4:{s:4:"type";s:14:"checkbox-group";s:5:"label";s:14:"Checkbox Group";s:9:"className";s:14:"checkbox-group";s:6:"values";a:3:{i:0;O:8:"stdClass":3:{s:5:"label";s:8:"Option 1";s:5:"value";s:8:"option-1";s:8:"selected";b:1;}i:1;O:8:"stdClass":2:{s:5:"label";s:8:"Option 2";s:5:"value";s:8:"option-2";}i:2;O:8:"stdClass":2:{s:5:"label";s:8:"Option 3";s:5:"value";s:8:"option-3";}}}i:2;O:8:"stdClass":3:{s:4:"type";s:4:"date";s:5:"label";s:10:"Date Field";s:9:"className";s:8:"calendar";}i:3;O:8:"stdClass":4:{s:4:"type";s:6:"header";s:7:"subtype";s:2:"h1";s:5:"label";s:7:"Heading";s:9:"className";s:6:"header";}i:4;O:8:"stdClass":4:{s:4:"type";s:9:"paragraph";s:7:"subtype";s:1:"p";s:5:"label";s:28:"Yuhoooo whats going on HERE ";s:9:"className";s:9:"paragraph";}i:5;O:8:"stdClass":3:{s:4:"type";s:6:"number";s:5:"label";s:6:"Number";s:9:"className";s:12:"form-control";}i:6;O:8:"stdClass":4:{s:4:"type";s:11:"radio-group";s:5:"label";s:11:"Radio Group";s:9:"className";s:11:"radio-group";s:6:"values";a:3:{i:0;O:8:"stdClass":3:{s:5:"label";s:8:"Option 1";s:5:"value";s:8:"option-1";s:8:"selected";b:1;}i:1;O:8:"stdClass":2:{s:5:"label";s:8:"Option 2";s:5:"value";s:8:"option-2";}i:2;O:8:"stdClass":2:{s:5:"label";s:8:"Option 3";s:5:"value";s:8:"option-3";}}}i:7;O:8:"stdClass":4:{s:4:"type";s:6:"select";s:5:"label";s:6:"Select";s:9:"className";s:12:"form-control";s:6:"values";a:3:{i:0;O:8:"stdClass":3:{s:5:"label";s:8:"Option 1";s:5:"value";s:8:"option-1";s:8:"selected";b:1;}i:1;O:8:"stdClass":2:{s:5:"label";s:8:"Option 2";s:5:"value";s:8:"option-2";}i:2;O:8:"stdClass":2:{s:5:"label";s:8:"Option 3";s:5:"value";s:8:"option-3";}}}i:8;O:8:"stdClass":4:{s:4:"type";s:4:"text";s:5:"label";s:10:"Text Field";s:7:"subtype";s:4:"text";s:9:"className";s:12:"form-control";}i:9;O:8:"stdClass":3:{s:4:"type";s:8:"textarea";s:5:"label";s:9:"Text Area";s:9:"className";s:12:"form-control";}}', 0),
(4, 80, 'Form01', '2017-09-15', 'a:5:{i:0;O:8:"stdClass":4:{s:4:"type";s:4:"text";s:5:"label";s:10:"First Name";s:7:"subtype";s:4:"text";s:9:"className";s:12:"form-control";}i:1;O:8:"stdClass":4:{s:4:"type";s:4:"text";s:5:"label";s:9:"Last Name";s:7:"subtype";s:4:"text";s:9:"className";s:12:"form-control";}i:2;O:8:"stdClass":3:{s:4:"type";s:6:"number";s:5:"label";s:12:"Phone Number";s:9:"className";s:12:"form-control";}i:3;O:8:"stdClass":4:{s:4:"type";s:11:"radio-group";s:5:"label";s:51:"Please confirm your availability for the exhibition";s:9:"className";s:11:"radio-group";s:6:"values";a:2:{i:0;O:8:"stdClass":3:{s:5:"label";s:9:"Available";s:5:"value";s:8:"option-1";s:8:"selected";b:1;}i:1;O:8:"stdClass":2:{s:5:"label";s:13:"Not Available";s:5:"value";s:8:"option-3";}}}i:4;O:8:"stdClass":3:{s:4:"type";s:4:"date";s:5:"label";s:5:"Date ";s:9:"className";s:8:"calendar";}}', 0);

-- --------------------------------------------------------

--
-- Table structure for table `form_data`
--

CREATE TABLE IF NOT EXISTS `form_data` (
  `form_data_id` int(11) NOT NULL,
  `user_id` text NOT NULL,
  `data_key` text NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `form_data`
--

INSERT INTO `form_data` (`form_data_id`, `user_id`, `data_key`, `value`) VALUES
(1, '47', 'firstfield', 'A'),
(2, '47', 'firstfield', 'A'),
(3, '47', 'secondfield', 'C'),
(4, '47', 'secondfield', 'C');

-- --------------------------------------------------------

--
-- Table structure for table `invitaion`
--

CREATE TABLE IF NOT EXISTS `invitaion` (
  `invitaion_id` int(11) NOT NULL,
  `user_id` varchar(11) NOT NULL,
  `form_id` varchar(11) NOT NULL,
  `event_id` varchar(11) NOT NULL,
  `status` varchar(11) NOT NULL,
  `response` varchar(5000) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invitaion`
--

INSERT INTO `invitaion` (`invitaion_id`, `user_id`, `form_id`, `event_id`, `status`, `response`) VALUES
(1, '47', '3', '1', '1', ''),
(2, '47', '2', '1', '0', ''),
(3, '80', '4', '2', '1', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL,
  `user_name` text NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` text NOT NULL,
  `school` text NOT NULL,
  `profile_pic` varchar(500) NOT NULL,
  `is_admin` int(11) NOT NULL,
  `status` text NOT NULL,
  `contact` varchar(20) NOT NULL,
  `emergency_contact` varchar(20) NOT NULL,
  `address` varchar(100) NOT NULL,
  `temp_link` text NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `email`, `password`, `school`, `profile_pic`, `is_admin`, `status`, `contact`, `emergency_contact`, `address`, `temp_link`, `created_by`) VALUES
(1, 'sdfsusmanfasdfasdf', 'usmanmq0@gmail.com', '$2a$12$y2CRrf8Psw2/MGs4iqgoPOmiKUqOHdti6RgLVBouhtkBCpIfviKcG', 'schoolsdfsdf', '9e03b045c56e33a0050ef01d515ecf4e6667b3f3.png', 1, 'active', '12345677890', '123445567', 'testing', '', 0),
(2, 'fatima', 'fatimayousaf115@gmail.com', '$2a$12$PIukIR5jpBdbMz4FfvXND.ito/hp/HBD7CvfrjM82ABh/9Cm3M5wS', 'DPS School', '0dc75e8e2aa9cc78824eabf19c63066fc6042245.jpg', 1, 'active', '', '', '', '1b04605732c8ed9d94e047d7ff19e5d4', 0),
(9, 'fatima', 'fatimayousaf113@gmail.com', '$2a$12$tD1kNFLL0SJWEJG8RRHOgOO0CEfcfWt6h4jzL1NewudGUf0eSuL26', 'DPS', 'default.png', 1, 'active', '', '', '', '', 0),
(10, 'Leon Frazier', 'leon@wrayzier.com', '$2a$12$Qg1WWo9W8nVG4/n1E1J...tMj4fJUDvhDaink1.EFimpVktJsBtHW', '457', 'default.png', 1, 'active', '', '', '', '', 0),
(11, 'Eric Haselhorst', 'rockinhfarmtoys@gmail.com', '$2a$12$cnTuoMwGXTIiem6Tkntpk.M1aZzutLOH/30Q4v0v2HDMGOsm4fokW', 'shs', 'default.png', 1, 'active', '', '', '', '', 0),
(12, 'TEst', 'ashtex4ga@gmail.com', '$2a$12$c/GwShF1PGfCLOtWV6pxgeziM8ekDT3O215GEdj/MdicoJEmFQjHK', 'Test School', 'default.png', 1, 'active', '', '', '', '', 0),
(13, 'fatima', 'fatimayousaf116@yahoo.com', '$2a$12$hhxVPtxdcUdmCxuwgR3rJO2x2pxXSyuY1TFqSwsOD9bGaMsm5UnWO', 'DPS', 'default.png', 1, 'active', '', '', '', '', 0),
(14, 'fatima', 'fatimayousaf118@gmail.com', '$2a$12$JtintYYEp0IqlHYGuFdT1.YAs1C8UragWogu2MtA5GChy.TU0kToq', 'dps', 'default.png', 1, 'active', '', '', '', '', 0),
(15, 'Fatima', 'fatimayousaf115@gmail.com', '$2a$12$ZIiOwvOYhDu1j0kKRyzA8.ZCApKYABWmFgneVufVQl6TS.HFiU0jK', 'DPSS', '594356e880f5e85158133e26cea40c9b5129625e.png', 1, 'active', '', '', '', '1b04605732c8ed9d94e047d7ff19e5d4', 0),
(16, 'fatima', 'fatimayousaf118@yahoo.com', '$2a$12$G.txQN7qarrwYS7V2m1XheACrB6t3gj99u/8cSCvdmXD7J5akUQvy', 'dps', 'default.png', 1, 'active', '', '', '', '', 0),
(47, 'sahir', 'sahirtoseef@gmail.com', '$2a$12$SpbneeNj/hS7PcA3qEmLPesw4ijUscqRE5pyy2D73JL1eFZSMCx0O', 'Ashtex', 'a3eb215feb9b747fe15147cc41c06c81b1b99a4d.png', 1, 'active', '03217877888', '03331234567', 'Lahore', '', 0),
(80, 'Usman', 'usman4qcs@gmail.com', '$2a$12$z0oE5gL5F6xel1tb3MHroeopDyb35STZ3PUdQYGBvktzropOrDf6e', 'dummy', 'default.png', 1, 'active', '03001234567', '02021234567', 'lahore', '', 47),
(81, 'random', 'husnainshabbir0@gmail.com', '$2a$12$2WLzsNsLJdrWPqpYKaikieZNkTvki13EtFIqmj69aAR4S65x7sZhC', 'dummy', 'default.png', 0, 'active', '', '', '', '', 47),
(84, 'random', 'newtestuser9@hotmail.com', '$2a$12$1EOXOuSVxnTIu0srQxXi3ecba.jcG7E7fKtcwoUCVr0dodb673ScO', 'dummy', 'default.png', 0, 'active', '', '', '', '', 47),
(85, 'random', 'testuser3@hotmail.com', '$2a$12$ujfJUIFChd0Mkjc8mOFcwONVh1WG4N4vdA6ZupvIBs/u2XNwjhbHi', 'dummy', 'default.png', 0, 'active', '', '', '', '', 47),
(86, 'random', 'newtestuser92@hotmail.com', '$2a$12$V9kI..VBXnv4YMn71JN6E.BwTvjM9ruiJhrApeWPm3fJsVoZqRUDG', 'dummy', 'default.png', 0, 'active', '', '', '', '', 47),
(87, 'random', 'newtestuser29@hotmail.com', '$2a$12$76/KCeDPWtiUKPFCObDFju8EwlSVlyQywbDJYE/6ziuJIQ33xTv6G', 'dummy', 'default.png', 0, 'active', '', '', '', '', 47),
(88, 'random', 'newtestuser90@hotmail.com', '$2a$12$XMMtddtFNKVl629q54t2DeCaOclLJGVP5O/bjCS/c9jpWuIkaMDeu', 'dummy', 'default.png', 0, 'active', '', '', '', '', 47),
(89, 'random', 'ayeshaaslam32@gmail.com', '$2a$12$QNWogRLjQHVFaLloLWJaF.nm8gQfv3lamfYEJ0yGqD.bvaD7NXoBG', 'dummy', 'default.png', 0, 'active', '', '', '', '', 0),
(90, 'ayesha', 'aisha32aslam@gmail.com', '$2a$12$uw5dbRubyVoiaCLHsSmdUetlxolj3A/5XSd3qk3t.oWqGwr2L8ZIO', 'abc', 'default.png', 1, 'active', '', '', '', '', 0),
(91, 'usman', 'testingashtex2@gmail.com', '$2a$12$GquTiKspmHKGwk5C4nLH7eW6ORWpaVRzH9C23WpVZaIy/xYqU/ooq', 'test', 'default.png', 1, 'active', '', '', '', '', 0),
(92, 'random', 'testingashtex1@gmail.com', '$2a$12$AjDUCv1HUuABlR02gALn4ugNM1XdRoTKuUyJjk43ejquPYT6Sj7am', 'dummy', 'default.png', 0, 'active', '', '', '', '', 80),
(116, 'Test name', 'testtinngs@gmail.com', '$2a$12$xMjNIJG9TgfmhNrkawWg.eY9VXf.uEtBg/Jq25b3R/0Hw7BKhzmpG', 'dummy', 'default.png', 0, 'active', '123456789', '123123123', 'new Address', '', 47),
(117, 'Test', 'testingashtex4@gmail.com', '$2a$12$LwDCpyiiJW/poX6txhFmtOyIiQgFbGhDu929tacQGFUoF6FIMg6M.', 'dummy', 'default.png', 0, 'active', '123456789', '4567890321', 'Busbf8dnixd ei28cidndie99', '', 80),
(118, 'Eric Haselhorst', 'echasel95@gmail.com', '$2a$12$oGMX0xxyUzq9iKLMWaFY6uc6kK8/.A2QQ04D37Zcc0VYltJLOFH6m', 'Sacred Heart Dodge City', 'default.png', 1, 'active', '', '', '', '', 0),
(119, 'Test account', 'testingashtex3@gmail.com', '$2a$12$/TtMotxVKlacpwwA.F7JwOnd91gtp2mdmHbzoahAb3YwrfpmLdjZK', 'dummy', 'default.png', 0, 'active', '1234567890', '1234567890', 'Test address', '', 80),
(120, 'random', 'hashbrownie02@gmail.com', '$2a$12$iUADTkzsVuEUFuhenx7qOuP6KiGOJvThW6iNjQ08w5v2Qu.dtaoTe', 'dummy', 'default.png', 0, 'active', '', '', '', '', 118);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `event_form_data`
--
ALTER TABLE `event_form_data`
  ADD PRIMARY KEY (`event_form_data_id`);

--
-- Indexes for table `form`
--
ALTER TABLE `form`
  ADD PRIMARY KEY (`form_id`);

--
-- Indexes for table `form_data`
--
ALTER TABLE `form_data`
  ADD PRIMARY KEY (`form_data_id`);

--
-- Indexes for table `invitaion`
--
ALTER TABLE `invitaion`
  ADD PRIMARY KEY (`invitaion_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `event_form_data`
--
ALTER TABLE `event_form_data`
  MODIFY `event_form_data_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `form`
--
ALTER TABLE `form`
  MODIFY `form_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `form_data`
--
ALTER TABLE `form_data`
  MODIFY `form_data_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `invitaion`
--
ALTER TABLE `invitaion`
  MODIFY `invitaion_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=121;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
