/*
 * App initialization
 */
var myApp = new Framework7( {
    swipeout: false,
    swipeBackPage: false,
} );
var $$ = Dom7;
var mainView = myApp.addView( '.view-main' );
var URL = '';
var APP_NAME = 'Paperless';

/*
document.addEventListener( "deviceready", function () {

    if ( typeof screen.lockOrientation == 'fuction' )
	screen.lockOrientation( 'portrait' );

    if ( device.platform.toLowerCase() == 'ios' )
	StatusBar.hide();

    // Slider initialization
    var mySwiper = myApp.swiper( '.swiper-container', {
	loop: true,
	autoplay: 5000,
	preloadImages: false,
	lazyLoading: true,
	pagination: '.swiper-pagination',
    } );
    // load front page
    var value = window.localStorage.getItem( "user_id" );
    if ( value ) {
	var options = {
	    url: 'pages/home.html',
	    animatePages: false
	};
	mainView.router.load( options );
    } else {
	var options = {
	    url: ( get_user_id() == 0 ) ? 'pages/login.html' : 'pages/home.html',
	    animatePages: false
	};
	mainView.router.load( options );
    }

    // Page change behaviour 
    $$( document ).on( 'page:beforeinit', function ( e ) {
	var page = e.detail.page;
	URL = page.url;
	sessionStorage.setItem( "url", URL );
	loadjscssfile( 'assets/js/pages/' + page.name + '.js', 'js' );
	load_fragment();
    } ).on( 'page:init', function ( e ) {
	var page = e.detail.page;
	myApp.hideIndicator();
	bind_clicks();
    } ).on( 'page:back', function ( e ) {
	var page = e.detail.page;
	loadjscssfile( 'assets/js/pages/' + page.name + '.js', 'js' );
	if ( page.name.indexOf( 'smart-select' ) > -1 ) {
	    $$( 'a.smart-select.update-alert div.item-after' ).html( "<span class='badge'>" + ( $$( 'input[type="checkbox"]:checked' ).length ? $$( 'input[type="checkbox"]:checked' ).length : 0 ) + "</span>" );
	}
    } );
}, false );*/

/*
 *
 */
//var server = 'http://45.55.183.220/paperless';
//var server = 'http://www.rockinhfarmtoys.com/paperless/';
var server = 'http://paperless.me/';
var value = window.localStorage.getItem( "user_id" );
var end_points = {
    LOGIN: server + '/?r=AppApi&action=1',
    UPDATE_USER: server + '/?r=AppApi&action=15&user_id=' + value,
    ACCEPT_EVENTS: server + '/?r=AppApi&action=2&user_id=' + value,
    REJECT_EVENTS: server + '/?r=AppApi&action=3&user_id=' + value,
    PENDNG_EVENTS: server + '/?r=AppApi&action=4&user_id=' + value,
    ALL_EVENTS: server + '/?r=AppApi&action=5&user_id=' + value,
    FORM: server + '/?r=AppApi&action=6&user_id=' + value,
    FORM_DATA: server + '/?r=AppApi&action=7&user_id=' + value,
    FORM_INSERTION: server + '/?r=AppApi&action=7&user_id=' + value,
    FORM_DETAILS: server + '/?r=AppApi&action=8&user_id=' + value,
    FORM_DATA_SEND: server + '/?r=AppApi&action=9&user_id=' + value,
    ACCEPT_EVENTS_DETAILS: server + '/?r=AppApi&action=10&user_id=' + value,
    ACCEPT_FORM_DETAILS: server + '/?r=AppApi&action=11&user_id=' + value,
    REJECT_FORM_DETAILS: server + '/?r=AppApi&action=12&user_id=' + value,
    ALL_FORM_DETAILS: server + '/?r=AppApi&action=13&user_id=' + value,
    GET_USER: server + '/?r=AppApi&action=14&user_id=' + value,
    // UPDATE_USER: server + '/?r=AppApi&action=15&user_id=' + value,
    ALL_EVENTS_WITHOUT_USER_ID: server + '/?r=AppApi&action=16&user_id=' + value,
    EVENT_DETAIL: server + '/?r=AppApi&action=17&user_id=' + value
};

function send_request( action, request_type, data, callback ) {
    $.ajax( {
	url: action,
	type: request_type,
	data: data,
	crossDomain: true,
	cache: false,
	dataType: "json",
	success: function ( data ) {
	    callback( data );
	}
    } );
}

/*
 * Email Validation
 */
function email_validation() {
    var email_reg_ex = /^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*(\.[a-zA-Z0-9_-]+)+$/;

    if ( !$( "#email" ).val().length || !email_reg_ex.test( $( "#email" ).val() ) ) {
	$( ".email_error" ).show();
	all_ok = false;
    } else {
	$( ".email_error" ).hide();
    }
}

/*
 * All common function below
 */

function bind_clicks() {
    $$( '.open-indicator' ).on( 'click', function () {
	myApp.showIndicator();
    } );
}

function get_user_id() {
    try {
	var id = 0;
	if ( localStorage.getItem( 'appUserId' ) )
	    id = localStorage.getItem( 'appUserId' );
	else
	    localStorage.setItem( 'appUserId', '1' );
    } catch ( e ) {
	myApp.alert( JSON.stringify( e ) );
    }

    return 0; // id;
}

function loadjscssfile( filename, filetype ) {
    if ( filetype == "js" ) { //if filename is a external JavaScript file 
	var fileref = document.createElement( 'script' );
	fileref.setAttribute( "type", "text/javascript" );
	fileref.setAttribute( "src", filename );
    } else if ( filetype == "css" ) { //if filename is an external CSS file 
	var fileref = document.createElement( "link" );
	fileref.setAttribute( "rel", "stylesheet" );
	fileref.setAttribute( "type", "text/css" );
	fileref.setAttribute( "href", filename );
    }
    if ( typeof fileref != "undefined" && fileref !== "" && fileref !== null && fileref !== "null" )
	document.getElementsByTagName( "head" )[0].appendChild( fileref )
}

function load_fragment() {
    var z, i, elmnt, file, xhttp;
    z = document.getElementsByTagName( "div" );
    for ( i = 0; i < z.length; i++ ) {
	elmnt = z[i];
	file = elmnt.getAttribute( "data-load-fragment" );
	if ( file ) {
	    try {
		xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function () {
		    elmnt.innerHTML = this.responseText;
		    elmnt.removeAttribute( "data-load-fragment" );
		    load_fragment();
		}
		xhttp.open( "GET", file, true );
		xhttp.send();
		return;
	    } catch ( e ) {
		// myApp.alert( JSON.stringify( e ) );
	    }
	}
    }
}

function $_GET( param ) {
    var vars = { };
    URL.replace( location.hash, '' ).replace(
	    /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
	    function ( m, key, value ) { // callback
		vars[key] = value !== undefined ? value : '';
	    }
    );

    if ( param ) {
	return vars[param] ? vars[param] : null;
    }
    return vars;
}

$.fn.serializeObject = function () {
    var o = { };
    var a = this.serializeArray();
    $.each( a, function () {
	if ( o[this.name] ) {
	    if ( !o[this.name].push ) {
		o[this.name] = [ o[this.name] ];
	    }
	    o[this.name].push( this.value || '' );
	} else {
	    o[this.name] = this.value || '';
	}
    } );
    return o;
};
// document.addEventListener('deviceready', function () {
//     if (typeof cordova.plugins.notification.badge.set() == 'fuction')
//     cordova.plugins.notification.badge.set(10);
// }, false);