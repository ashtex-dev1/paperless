myApp.showIndicator();
// window.localStorage.clear();
$(document).ready(function () {    
    var mySwiper = myApp.swiper('.swiper-container', {
        loop: true,
        autoplay: 5000,
        pagination: '.swiper-pagination',
        paginationHide: false,
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
    });
    send_request(end_points.PENDNG_EVENTS, 'GET', {},
        function (data) {
            if (data.success) {
                var response = data;
                var pending_count = response.data.length;
                $(".pending_number").html(pending_count);
                myApp.hideIndicator();
            } else {
                $(".pending_number").html("0");
                myApp.hideIndicator();
            }
        });
    send_request(end_points.ACCEPT_EVENTS, 'GET', {},
        function (data) {
            if (data.success) {
                var response = data;
                var accept_count = response.data.length;
                $(".accept_number").html(accept_count);
                myApp.hideIndicator();

            } else {
                $(".accept_number").html("0");
                myApp.hideIndicator();
            }
        });
    send_request(end_points.REJECT_EVENTS, 'GET', {},
        function (data) {
            if (data.success) {
                var response = data;
                var reject_count = response.data.length;
                $(".reject_number").html(reject_count);
                myApp.hideIndicator();

            } else {
                $(".reject_number").html("0");
                myApp.hideIndicator();
            }
        });
    send_request(end_points.ALL_EVENTS, 'GET', {},
        function (data) {
            if (data.success) {
                var response = data;
                var all_count = response.data.length;
                $(".all_number").html(all_count);
                myApp.hideIndicator();
            } else {
                $(".all_number").html("0");
                myApp.hideIndicator();
            }
        });
});