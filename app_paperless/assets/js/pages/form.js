myApp.showIndicator();
$( document ).ready( function () {
    send_request( end_points.FORM, 'POST', { form_id: $_GET( 'form_id' ), event_id: $_GET( 'id' ) },
    function ( response ) {
	if ( response.success ) {
	    // console.log( response );
	    $( 'div[data-page="form"] .card-header' ).html( response.title );
	    $( '.form-view' ).formRender( {
		dataType: 'json',
		formData: JSON.stringify( response.form_data )
	    } );
	    setTimeout( function () {
		$( 'input, select, textarea', $( '.form-view' ) ).each( function () {
		    var par = $( this ).parent();
		    if (
			    $( '.page.page-on-left' ).attr( 'data-page' ) == 'rejectrequestdetails' ||
			    $( '.page.page-on-left' ).attr( 'data-page' ) == 'pendingrequestdetails' ||
			    $( '.page.page-on-left' ).attr( 'data-page' ) == 'allrequestsdetails'
			    ) {
			$( this ).attr( "disabled", true );
		    }

		    if ( $( this ).hasAttr( 'required' ) ) {
			if (
				$( this ).closest( '.radio-group' ).length ||
				$( this ).closest( '.checkbox-group' ).length
				) {
			    if ( $( 'label:first', par.parent() ).text().indexOf( '*' ) < 0 )
				$( 'label:first', par.parent() ).text( $( 'label:first', par.parent() ).text() + "*" );
			} else
			    $( 'label', par ).text( $( 'label', par ).text() + "*" );
		    }

		    for ( var i = 0; i < response.data.length; i++ ) {
			var user_data = response.data[i];
			if ( $( this ).attr( 'name' ) == user_data.data_key ) {
			    $( this ).val( user_data.value );
			    if (
				    $( this ).attr( 'type' ) == "radio" ||
				    $( this ).attr( 'type' ) == "checkbox"
				    ) {
				$( this ).attr( 'checked', true );
			    }
			}
		    }
		} );
		myApp.hideIndicator();
	    }, 500 );
	}
	else {
	    myApp.hideIndicator();
	}
	switch ( $( '.page.page-on-left' ).attr( 'data-page' ) ) {
	    case 'rejectrequestdetails':
		$( ".form-submit-btn", $( '.form-detail-view' ) ).remove();
		$( ".pending_request_back" ).attr( 'href', 'pages/rejectrequestdetails.html?id=' + $_GET( 'id' ) );
		break;
	    case 'pendingrequestdetails':
		$( ".form-submit-btn", $( '.form-detail-view' ) ).remove();
		$( ".pending_request_back" ).attr( 'href', 'pages/pendingrequestdetails.html?id=' + $_GET( 'id' ) );
		break;
	    case 'allrequestsdetails':
		$( ".form-submit-btn", $( '.form-detail-view' ) ).remove();
		$( ".pending_request_back" ).attr( 'href', 'pages/allrequestsdetails.html?id=' + $_GET( 'id' ) );
		break;
	    case 'acceptrequestdetails':
		$( ".form-submit-btn", $( '.form-detail-view' ) ).fadeIn( 200 );
		$( ".pending_request_back" ).attr( 'href', 'pages/acceptrequestdetails.html?id=' + $_GET( 'id' ) );
		break;
	    default:
		$( ".pending_request_back" ).attr( 'href', 'pages/home.html' );
		break;
	}
    } );

    $( document ).off( 'click', '#submit' ).on( 'click', '#submit', function ( event ) {
	var all_ok = true;
	$( 'input[required], select[required], textarea[required]' +
		'input[required="true"], select[required="true"], textarea[required="true"]' +
		'input[required="required"], select[required="required"], textarea[required="required"]'
		, $( '.form-view' ) ).each( function () {
	    /*
	     switch ( $( this ).attr( 'type' ) ) {
	     case 'radio':
	     case 'checkbox':
	     if (  )
	     break;
	     default:
	     break;
	     }
	     */
	    if ( typeof $( this ).val() === "undefined" || $( this ).val() === '' || $( this ).val() === 0 || $( this ).val() === '0' || $( this ).val() === null || !$( this ).val().length ) {
		all_ok = false;
	    }
	} );

	if ( !all_ok ) {
	    myApp.alert( "Please fill in the mandetory fields...", APP_NAME );
	} else {
	    var data = $( '#user-form' ).serializeObject();
	    var user_id = window.localStorage.getItem( "user_id" );
	    myApp.showIndicator();
	    send_request( end_points.FORM_INSERTION, 'POST', {
		form_id: $_GET( 'form_id' ),
		event_id: $_GET( 'id' ),
		user_id: user_id,
		form_data: data,
	    }, function ( data ) {
		myApp.hideIndicator();
            mainView.router.loadPage( "pages/acceptrequest.html" );
		if ( data.success ) {
		    mainView.router.loadPage( "pages/acceptrequest.html" );
		} else {
		    // myApp.alert( "Something went wrong...", APP_NAME );
		}
	    } );
	}
    } );
    // myApp.showIndicator();
} );