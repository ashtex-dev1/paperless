$( document ).ready( function () {
    myApp.showIndicator();
    //send_request(end_points.ALL_EVENTS_WITHOUT_USER_ID, 'GET', {},
    send_request( end_points.ALL_EVENTS, 'GET', { },
	    function ( data ) {
		if ( data.success ) {
		    var response = data;
		    if ( response.success == "1" ) {
			for ( var i = 0; i < response.data.length; i++ ) {
			    var event = response.data[i];
			    var clone = $( ".all_name_li:first" ).clone();
			    if ( event.status == "0" ) {
				$( ".item-after", clone ).html( "<span class='badge bg-grey'>Pending</span>" );
			    } else if ( event.status == "1" ) {
				$( ".item-after", clone ).html( "<span class='badge bg-green'>Accepted</span>" );
			    } else if ( event.status == "2" ) {
				$( ".item-after", clone ).html( "<span class='badge bg-red'>Rejected</span>" );
			    } else if ( event.status == "3" ) {
				$( ".item-after", clone ).html( "<span class='badge bg-blue'>Attempted</span>" );
			    }
			    clone.find( "a.item-link" ).attr( 'href', 'pages/allrequestsdetails.html?id=' + event.event_id );
			    clone.find( ".name" ).html( event.name );
			    clone.show().appendTo( $( '.main_ul' ) );
			    myApp.hideIndicator();
			}
		    } else {
			myApp.hideIndicator();
			$( "#msg" ).show();
		    }
		} else {
		    myApp.hideIndicator();
		    $( "#msg" ).show();
		}
	    } );
} );