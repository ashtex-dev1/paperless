myApp.showIndicator();
$( document ).ready( function () {
    var user_id = window.localStorage.getItem( "user_id" );
    send_request( end_points.GET_USER, 'POST', { user_id: user_id },
    function ( data ) {
	if ( data ) {
	    var response = data.data;
	    $.each( response, function ( key, user_data ) {
		if ( user_data.user_id ) {
		    $( '#name' ).val( user_data.user_name );
		    $( '#email' ).val( user_data.email );
		    $( '#contact' ).val( user_data.contact );
		    $( '#emergencyContact' ).val( user_data.emergency_contact );
		    $( '#address' ).val( user_data.address );
		} else
		    $( ".email_data_error" ).show();
	    } );
	    myApp.hideIndicator();
	} else {
	    $( ".email_data_error" ).show();
	}
    } );

    $( document ).off( 'click', '#submit' ).on( 'click', '#submit', function ( event ) {
	var all_ok = true;
	var input = $( ".form-control" ).val();
	var autocomplete = $( ".autocomplete" ).val();
	var checkbox = $( ".checkbox" ).is( ":checked" );
	var checkbox_group = $( ".checkbox-group" ).is( ":checked" );
	var calendar = $( ".calendar" ).val();
	var radio_group = $( ".radio-group" ).val();
	if ( $( '#name' ).val() === "" )
	{
	    $( '.input-error-name' ).css( 'display', 'block' );
	    return false;
	} else
	{
	    if ( !ValidateName( $( '#name' ).val() ) )
	    {
		$( '.input-error-name' ).css( 'display', 'block' );
		return false;
	    } else {
		$( '.input-error-name' ).css( 'display', 'none' );
	    }
	}
	if ( $( '#email' ).val() === "" )
	{
	    $( '.input-error-email' ).css( 'display', 'block' );
	    return false;
	} else
	{
	    if ( !ValidateEmail( $( '#email' ).val() ) )
	    {
		$( '.input-error-email' ).css( 'display', 'block' );
		return false;
	    } else {
		$( '.input-error-email' ).css( 'display', 'none' );
	    }
	}
	if ( $( '#contact' ).val() === "" )
	{
	    $( '.input-error-contact' ).css( 'display', 'block' );
	    return false;
	} else
	{
	    if ( !ValidateContact( $( '#contact' ).val() ) )
	    {
		$( '.input-error-contact' ).css( 'display', 'block' );
		return false;
	    } else {
		$( '.input-error-contact' ).css( 'display', 'none' );
	    }
	}
	if ( $( '#emergencyContact' ).val() === "" )
	{
	    $( '.input-error-emergency-contact' ).css( 'display', 'block' );
	    return false;
	} else
	{
	    if ( !ValidateContact( $( '#emergencyContact' ).val() ) )
	    {
		$( '.input-error-emergency-contact' ).css( 'display', 'block' );
		return false;
	    } else {
		$( '.input-error-emergency-contact' ).css( 'display', 'none' );
	    }
	}
	if ( $( '#address' ).val() === "" )
	{
	    $( '.input-error-address' ).css( 'display', 'block' );
	    return false;
	} else
	{
	    $( '.input-error-address' ).css( 'display', 'none' );
	}

	if ( all_ok ) {
	    var data = $( '#user-form' ).serializeObject();
	    var value = window.localStorage.getItem( "user_id" );
	    send_request( end_points.UPDATE_USER, 'POST', {
		user_id: value, status: "1", form_data: data
	    }, function ( data ) {
		myApp.alert( "Updated...", APP_NAME );
		// mainView.router.loadPage( "pages/home.html" );
	    } );
	}
    } );

    $( "#logout" ).off( 'click' ).on( 'click', function () {
	localStorage.clear();
	mainView.router.loadPage( "pages/login.html" );
    } );

    $( ".pending_request_back" ).off( 'click' ).on( 'click', function () {
	if ( $( '.page' ).attr( 'data-page' ) == 'acceptrequestdetails' ) {
	    mainView.router.loadPage( 'pages/acceptrequestdetails.html?id=' + $_GET( 'id' ) );
	} else if ( $( '.page' ).attr( 'data-page' ) == 'pendingrequestdetails' ) {
	    mainView.router.loadPage( 'pages/pendingrequestdetails.html?id=' + $_GET( 'id' ) );
	} else if ( $( '.page' ).attr( 'data-page' ) == 'allrequestsdetails' ) {
	    mainView.router.loadPage( 'pages/allrequestsdetails.html?id=' + $_GET( 'id' ) );
	} else {
	    mainView.router.loadPage( 'pages/home.html?id=' + $_GET( 'id' ) );
	}
    } );

    function ValidateEmail( email ) {
	var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	return expr.test( email );
    }

    function ValidateName( name ) {
	var expr = /^[a-zA-Z\s]+$/;
	return expr.test( name );
    }

    function ValidateContact( contact ) {
	var expr = /^[0-9\-\s]+$/i;
	return expr.test( contact );
    }

} );