myApp.showIndicator();
$( document ).ready( function () {
    send_request( end_points.PENDNG_EVENTS, 'POST', { event_id: $_GET( 'id' ) },
    function ( data ) {

	if ( data.success ) {
	    var response = data.data;
	    $.each( response, function ( key, value ) {
		if ( $_GET( 'id' ) == value.event_id ) {
		    $( ".event_name" ).html( value.name );
		    $( ".event_type" ).html( value.type );
		    $( ".start_date" ).html( value.start_date );
		    $( ".end_date" ).html( value.end_date );
		    $( ".start_time" ).html( value.start_time );
		    $( ".end_time" ).html( value.end_time );
		    $( ".venue" ).html( value.venue );
		    $( ".description" ).html( value.description );
		    $( "a.form-request-btn" ).attr( 'href', 'pages/form.html?form_id=' + value.form_id + '&id=' + $_GET( 'id' ) );
		}
		myApp.hideIndicator();
	    } );
	} else {

	}
    } );
    $( '.create-popup' ).off( 'click' ).on( 'click', function () {
	myApp.showIndicator();
	send_request( end_points.FORM_DETAILS, 'POST', { event_id: $_GET( 'id' ) },
	function ( data ) {
	    if ( data.success ) {
		var response = data.data;
        var extra_css = (device.platform.toLowerCase() == 'ios') ? "margin-top: -20px; height: 100%" : "";
		var popupHTML =
			'<div class="popup" style="'+ extra_css +'">' +
			'<div class="content-block">' +
			'<p><a href="#" class="close-popup"><div class="item-media" style="float: left;margin-top: -28px;"><i class="icon f7-icons">close_round_fill</i></div></a></p>' +
			'<div class="list-block">' +
			'<ul>';
		$.each( response, function ( key, form ) {
		    popupHTML += '<li>' +
			    '<div class="item-content form_request">' +
			    '<div class="item-media">' +
			    '<i class="icon f7-icons">person</i></div>' +
			    '<div class="item-inner">' +
			    '<div class="item-title label form_name">' +
			    form.name +
			    '</div>' +
			    '<input type="hidden" id="form_id" value="' + form.form_id + '">' +
			    '<a href="javascript:void(0);" class="button accept_btn button-fill bg-green" style="width: 10px; float: right; margin: 0 !important; font-size: 10px;">Accept</a>' +
			    '<a href="javascript:void(0);" class="button reject_btn button-fill bg-red" style="width: 10px; float: right; margin: 0 !important; font-size: 10px;">Reject</a>' +
			    '</div>' +
			    '</div>' +
			    '</li>';
		} );
		popupHTML += '</ul >' +
			'</div>' +
			'</div>' +
			'</div>';
		myApp.popup( popupHTML );
		myApp.hideIndicator();
		$( document ).off( 'click', '.reject_btn' ).on( 'click', '.reject_btn', function () {
		    myApp.showIndicator();
		    var node = $( this ).parent().parent().parent();
		    var form_id = $( this ).parent().find( "#form_id" ).val();
		    send_request( end_points.FORM_INSERTION, 'POST', { form_id: form_id, event_id: $_GET( 'id' ), status: "2" },
		    function ( data ) {
			myApp.hideIndicator();
			if ( data.success ) {
			    node.fadeOut( 200, function () {
				node.remove();
			    } );
			    
			    myApp.alert( 'Request Rejected', APP_NAME );
			    mainView.router.loadPage( "pages/pendingrequest.html" );
			} else {
			    myApp.alert( 'Something went wrong...', APP_NAME );
			}
		    } );
		} );
		$( document ).off( 'click', '.accept_btn' ).on( 'click', '.accept_btn', function () {
		    myApp.showIndicator();
		    var node = $( this ).parent().parent().parent();
		    var form_id = $( this ).parent().find( "#form_id" ).val();
		    send_request( end_points.FORM_INSERTION, 'POST', { form_id: form_id, event_id: $_GET( 'id' ), status: "1" },
		    function ( data ) {
			myApp.hideIndicator();
			if ( data.success ) {
			    console.log( node.html() );
			    node.fadeOut( 200, function () {
				node.remove();
			    } );

			    myApp.alert( 'Request Accepted', APP_NAME );
			    mainView.router.loadPage( "pages/pendingrequest.html" );
			} else {
			    myApp.alert( 'Something went wrong...', APP_NAME );
			}
		    } );
		} );
	    } else {
		myApp.hideIndicator();
		myApp.alert( 'Something went wrong...', APP_NAME );
	    }
	} );
    } );
} );