$( document ).ready( function () {
    $( '#login_btn' ).off( 'click' ).on( 'click', function () {
	all_ok = true;
	email_validation();

	if ( $( '#password' ).val() == '' ) {
	    all_ok = false;
	    $( '.password_error' ).fadeIn( 200 );
	} else
	    $( '.password_error' ).hide();

	if ( all_ok ) {
	    myApp.showIndicator();
	    send_request( end_points.LOGIN, 'POST', { email: $( '#email' ).val(), password: $( '#password' ).val() },
	    function ( data ) {
		myApp.hideIndicator();
		if ( data.success ) {
		    var response = data.data;
		    $.each( response, function ( key, user_data ) {
			if ( user_data.user_id ) {
			    window.localStorage.setItem( "user_id", user_data.user_id );
			    window.localStorage.setItem( "email", $( '#email' ).val() );
			    mainView.router.loadPage( 'pages/userprofile.html?id=' + user_data.user_id );
			    window.location.reload( true );
			} else
			    $( ".email_data_error" ).show();
		    } );
		} else {
		    $( ".email_data_error" ).show();
		}
	    } );
	}
    } );
} );