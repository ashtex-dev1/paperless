$(document).ready(function () {
    $('#login_btn').on('click', function () {
        email_validation();
        send_request(end_points.LOGIN, 'POST', { email: $('#email').val(), password: $('#password').val() },
            function (data) {
                if (data.success) {
                    var response = data.data;
                    $.each(response, function (key, user_data) {
                        if (user_data.user_id) {
                            window.localStorage.setItem("user_id", user_data.user_id);
                            var value = window.localStorage.getItem("user_id");
                            window.location.reload(true);
                            mainView.router.loadPage('pages/home.html?id=' + user_data.user_id);
                            
                        } else
                            $(".email_data_error").show();
                    });
                } else {
                    $(".email_data_error").show();
                }
            });
    });
});