myApp.showIndicator();
$(document).ready(function () {
    send_request(end_points.FORM, 'POST', { form_id: $_GET('form_id'), event_id: $_GET('id') },
        function (response) {
            if (response.success) {
                $('.form-view').formRender({
                    dataType: 'json',
                    formData: JSON.stringify(response.form_data)
                });
                $('input, select, textarea', $('.form-view')).each(function () {
                    for (var i = 0; i < response.data.length; i++) {
                        var user_data = response.data[i];
                        if ($(this).attr('name') == user_data.data_key)
                            $(this).val(user_data.value);
                    }
                });
                myApp.hideIndicator();
            }
            else {
                myApp.hideIndicator();
            }
        });

    $(document).delegate('#submit', 'click', function (event) {
        var all_ok = true;
        var input = $(".form-control").val();
        var autocomplete = $(".autocomplete").val();
        var autocomplete = $(".autocomplete").val();
        var checkbox = $(".checkbox").is(":checked");
        var checkbox_group = $(".checkbox-group").is(":checked");
        var calendar = $(".calendar").val();
        var radio_group = $(".radio-group").val();
        if (input == '') {
            $('.fb-number-label').append("<p class='input-error'>Fill the field</p>");
            $('.fb-text-label').append("<p class='input-error'>Fill the field</p>");
            $('.fb-textarea-label').append("<p class='input-error'>Fill the field</p>");
            $('.fb-file-label').append("<p class='input-error'>Fill the field</p>");
            return false;
        }else{
            $(".input-error").hide();
        }
        if (autocomplete == '') {
            $('.fb-autocomplete-label').append("<p class='input-error'>Fill the field</p>");
            return false;
        }else{
            $(".input-error").hide();
        }
        if (!checkbox) {
            $('.fb-checkbox').append("<p class='input-error'>Please check the checkbox</p>");
            return false;
        }else{
            $(".input-error").hide();
        }
        if (!checkbox_group) {
            $('.fb-checkbox-group-label').append("<p class='input-error'>Fill the field</p>");
            return false;
        }else{
            $(".input-error").hide();
        }
        if (calendar == '') {
            $('.fb-date-label').append("<p class='input-error'>Fill the field</p>");
            return false;
        }
        if (all_ok) {
            var data = $('#user-form').serializeObject();
            var value = window.localStorage.getItem("user_id");
            send_request(end_points.FORM_INSERTION, 'POST', {
                form_id: $_GET('form_id'), event_id: $_GET('id'), status: "1", form_data: data
            },
                function (data) {
                    mainView.router.loadPage("pages/acceptrequest.html");
                });
            if (data.success) {
                mainView.router.loadPage("pages/acceptrequest.html");
            } else {

            }
        }
    });
    $(".pending_request_back").click(function () {
        if ($('.page').attr('data-page') == 'acceptrequestdetails') {
            mainView.router.loadPage('pages/acceptrequestdetails.html?id=' + $_GET('id'));
        } else if ($('.page').attr('data-page') == 'pendingrequestdetails') {
            mainView.router.loadPage('pages/pendingrequestdetails.html?id=' + $_GET('id'));
        } else if ($('.page').attr('data-page') == 'allrequestsdetails') {
            mainView.router.loadPage('pages/allrequestsdetails.html?id=' + $_GET('id'));
        } else {
            mainView.router.loadPage('pages/rejectrequestdetails.html?id=' + $_GET('id'));
        }
    });

});