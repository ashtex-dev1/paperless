myApp.showIndicator();
$(document).ready(function () {
    send_request(end_points.REJECT_EVENTS, 'GET', {},
        function (data) {

            if (data.success) {
                var response = data;
                if (response.success == "1") {
                    for (var i = 0; i < response.data.length; i++) {
                        var event = response.data[i];
                        var clone = $(".reject_name_li:first").clone();
                        clone.find("a.item-link").attr('href', 'pages/rejectrequestdetails.html?id=' + event.event_id);
                        clone.find(".name").html(event.name);
                        clone.show().appendTo($('.main_ul'));
                    }
                    myApp.hideIndicator();
                } else {
                    myApp.hideIndicator();
                    $("#msg").show();
                }
            } else {
                myApp.hideIndicator();
                $("#msg").show();
            }
        });
});