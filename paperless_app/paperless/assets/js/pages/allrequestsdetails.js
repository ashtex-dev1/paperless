myApp.showIndicator();
$(document).ready(function () {
    send_request(end_points.ALL_EVENTS, 'POST', { event_id: $_GET('id') },
        function (data) {

            if (data.success) {
                var response = data.data;
                $.each(response, function (key, value) {
                    if ($_GET('id') == value.event_id) {
                        $(".event_name").html(value.name);
                        $(".event_type").html(value.type);
                        $(".start_date").html(value.start_date);
                        $(".end_date").html(value.end_date);
                        $(".start_time").html(value.start_time);
                        $(".end_time").html(value.end_time);
                        $(".venue").html(value.venue);
                        $(".description").html(value.description);
                        //$("a.accept-request-btn").attr('href', 'pages/form.html?form_id=' + value.form_id + '&id=' + $_GET('id'));
                    }
                    myApp.hideIndicator();
                });
            } else if (data.error) {

            }
        });
    $('.create-popup').on('click', function () {
        send_request(end_points.ALL_FORM_DETAILS, 'POST', { event_id: $_GET('id') },
            function (data) {
                var response = data.data;
                var popupHTML =
                    '<div class="popup">' +
                    '<div class="content-block">' +
                    '<p><a href="#" class="close-popup"><div class="item-media" style="float: left;margin-top: -28px;"><i class="icon f7-icons">close_round_fill</i></div></a></p>' +
                    '<div class="list-block">' +
                    '<ul>';
                $.each(response, function (key, form) {
                    popupHTML += '<li>' +
                        '<div class="item-content form_request">' +
                        '<div class="item-media">' +
                        '<i class="icon f7-icons">person</i></div>' +
                        '<div class="item-inner">' +
                        '<div class="item-title label form_name">' +
                        form.name +
                        '</div>' +
                        '<a href="pages/form.html?form_id=' + form.form_id + '&id=' + $_GET('id') + '" class="button accept_btn close-popup" style="width: 10px;font-size: 10px;">View</a>' +
                        '</div>' +
                        '</div>' +
                        '</li>';
                });
                popupHTML += '</ul >' +
                    '</div>' +
                    '</div>' +
                    '</div>'
                myApp.popup(popupHTML);
                if (data.success) {
                } else {

                }
            });
    });
});